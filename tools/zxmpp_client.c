#include <stdio.h>
#include <stdlib.h>
#include <zcontext.h>
#include <zconnection.h>
#include <zjid.h>
#include <zobject.h>
#include <zxmpp.h>
#include <string.h>
#include <zstanza_filter.h>

static ZContext *ctx;
static ZConnection *conn;

static void status_callback(ZConnection *connection,int type,void *data) {
	switch(type) {
		case Z_STATUS_WRONG_CREDENTIALS:
		{
			printf("Wrong credentials\n");
		}
		break;
		case Z_STATUS_SESSION_SUCCESS:
		{
			ZStanza *presense_stanza,*child_stanza;
			presense_stanza = z_stanza_new("presence");
			z_stanza_add_attribute(presense_stanza,"type","subscribed");

			child_stanza = z_stanza_new("show");
			z_stanza_append_text(child_stanza,"chat",0);

			z_stanza_add_child(presense_stanza,child_stanza);
			z_object_unref((ZObject*)child_stanza);

			z_connection_send_stanza(connection,presense_stanza);
			z_object_unref((ZObject*)presense_stanza);

			presense_stanza = z_stanza_new("presence");
			child_stanza = z_stanza_new("priority");
			z_stanza_append_text(child_stanza,"5",0);
			z_stanza_add_child(presense_stanza,child_stanza);
			z_object_unref((ZObject*)child_stanza);
			z_connection_send_stanza(connection,presense_stanza);
			z_object_unref((ZObject*)presense_stanza);
		}
		break;
	}
}


static int message_response(ZStanzaFilter *filter,ZStanza *stanza,void *data) {
	ZStanza *child = z_stanza_get_child_by(stanza,Z_STANZA_CHILD_BY_NAME,"body");
	if(child) {
		const char *text = z_stanza_get_text(child);
		printf("Received message [ %s ]\n",text);
		if(strcmp(text,"exit") == 0) {
			z_context_remove_connection(ctx,conn);
		}
	}
	return 0;
}


static void read_write_callback(ZSocket *sock,int type,const char *text,size_t data_size,void *data) {
	char *tmp;

	tmp = malloc(data_size+1);
	memcpy(tmp,text,data_size);
	tmp[data_size] = '\0';
	if(type == Z_SOCKET_READ) {
		printf("Reading [ %s ]\n",tmp);
	}else{
		printf("Writting [ %s ]\n",tmp);
	}
	free(tmp);
}

static void clear_newline(char *str) {
	int i;
	i = strlen(str)-1;
	if( str[ i ] == '\n') {
		str[i] = '\0';
	}
}

int main (int argc, char *argv[])
{
	ZJid *jid = NULL;
	ZSocket *socket;
	ZStanzaFilter *stanza_filter;
	ZStanzaFilterRule *rule;
	char input[128];

	if(z_xmpp_init()) {
		fprintf(stderr,"Error initializing library\n");
		exit(1);
	}
	ctx = z_context_new();
	if(!ctx) {
		fprintf(stderr,"Error creating context\n");
		exit(1);
	}
	printf("Please enter google account. Ex (test@gmail.com): ");
	fgets(input,sizeof(input),stdin);
	clear_newline(input);
	jid = z_jid_new(input);
	if(!jid) {
		fprintf(stderr,"Error creating jid\n");
		exit(1);
	}

        printf("Please enter google account password: ");
        fgets(input,sizeof(input),stdin);
        clear_newline(input);

	conn = z_connection_new(jid,input,0);
	if(!conn) {
		fprintf(stderr,"Error creating connection\n");
		exit(1);
	}
	z_object_unref((ZObject*)jid);

	stanza_filter = z_connection_get_filter(conn);
	z_connection_set_status_callback(conn,status_callback,NULL);
   	rule = z_stanza_filter_add_rule(stanza_filter,message_response,NULL,Z_FILTER_RULE_NAME,"message",Z_FILTER_RULE_TYPE,"chat",Z_FILTER_RULE_DONE);
	if(!rule) {
		fprintf(stderr,"Error creating connection\n");
		exit(1);
	}    
	socket = z_connection_get_socket(conn);
	if(!socket) {
		fprintf(stderr,"Error getting socket\n");
		exit(1);
	}
	z_socket_set_host(socket,"talk.google.com");
	z_socket_set_read_write_callback(socket,read_write_callback,NULL);
	if(z_context_add_connection(ctx,conn)) {
		fprintf(stderr,"Error adding connection\n");
		exit(1);
	}
	z_object_unref((ZObject*)conn);

	while(z_context_has_connections(ctx)) {
		z_context_iterate(ctx);
	}
	z_object_unref((ZObject*)ctx);
	z_xmpp_destroy();
	return 0;
}
