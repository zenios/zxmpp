#include <stdio.h>
#include <stdlib.h>
#include <zcontext.h>
#include <zconnection.h>
#include <zjid.h>
#include <zobject.h>
#include <zxmpp.h>
#include <string.h>

static void status_callback(ZConnection *connection,int status,void *data) {
        switch(status) {
            case Z_STATUS_WRONG_CREDENTIALS:
            {
                    printf("Wrong credentials\n");
            }
            break;
            case Z_STATUS_AUTHENTICATION_FAILURE:
            {
                printf("Authentication failure\n");
            }
            break;
        }

}

static void read_write_callback(ZSocket *sock,int type,const char *text,size_t data_size,void *data) {
	char *tmp;

	tmp = malloc(data_size+1);
	memcpy(tmp,text,data_size);
	tmp[data_size] = '\0';
	if(type == Z_SOCKET_READ) {
		printf("Reading [%s]\n",tmp);
	}else{
		printf("Writting [%s]\n",tmp);
	}
	free(tmp);
}

int main (int argc, char *argv[])
{
	ZContext *ctx;
	ZConnection *conn;
	ZJid *jid = NULL;
	ZSocket *socket;

	if(z_xmpp_init()) {
		fprintf(stderr,"Error initializing library\n");
		exit(1);
	}
	ctx = z_context_new();
	if(!ctx) {
		fprintf(stderr,"Error creating context\n");
		exit(1);
	}

	jid = z_jid_new("test.component.zenios@zenios");
	if(!jid) {
		fprintf(stderr,"Error creating jid\n");
		exit(1);
	}

	conn = z_connection_new(jid,"blahblah",1);
	if(!conn) {
		fprintf(stderr,"Error creating connection\n");
		exit(1);
	}
	z_connection_set_status_callback(conn,status_callback,NULL);
	z_object_unref((ZObject*)jid);

	socket = z_connection_get_socket(conn);
	if(!socket) {
		fprintf(stderr,"Error getting socket\n");
		exit(1);
	}
	z_socket_set_read_write_callback(socket,read_write_callback,NULL);
	if(z_context_add_connection(ctx,conn)) {
		fprintf(stderr,"Error adding connection\n");
		exit(1);
	}
	z_object_unref((ZObject*)conn);

	while(z_context_has_connections(ctx)) {
		z_context_iterate(ctx);
	}
	z_object_unref((ZObject*)ctx);
	z_xmpp_destroy();
	return 0;
}
