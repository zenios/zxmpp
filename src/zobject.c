/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "zobject_c.h"
#include "zstdlib_c.h"
#include "zstring.h"
#include "zlist.h"
#include "zatomic.h"


struct ZObjectPrivate {
    size_t obj_size;
    char *type;
    ZAtomic refcount;
};

struct ZObjectCallback {
    ZCallback callback;
    struct ZObjectCallback *next;
};


ZObject *z_object_new(size_t obj_size, const char *type) {
    ZObject *obj;

    if (obj_size < sizeof(ZObject)) {
        return NULL;
    }

    obj = z_malloc(obj_size);
    if(!obj) {
        return NULL;
    }


    z_memset(obj, 0, obj_size);
    obj->private_data = z_malloc(sizeof(ZObjectPrivate));
    if(!obj->private_data) {
        z_free(obj);
        return NULL;
    }
    z_memset(obj->private_data,0,sizeof(ZObjectPrivate));
    obj->private_data->obj_size = obj_size;
    obj->private_data->refcount = 1;

    if (type) {
        obj->private_data->type = z_strdup(type);
        if(!obj->private_data->type) {
            z_free(obj->private_data);
            z_free(obj);
            return NULL;
        }

    }

    return obj;
}

void z_object_ref(ZObject *obj) {

    if (!obj) {
      return;
    }

    z_atomic_inc(&obj->private_data->refcount);
}

void z_object_unref(ZObject *obj) {
    int ref_count;
    ZObjectCallback *curr;

    if (!obj) {
       return;
    }

    ref_count = z_atomic_dec(&obj->private_data->refcount);

    if (ref_count == 0) {
        if (obj->free_callbacks) {
            foreach_in_list(curr, obj->free_callbacks) {
                curr->callback(obj);
            }

            while(obj->free_callbacks) {
                curr = obj->free_callbacks;
                obj->free_callbacks = curr->next;
                z_free(curr);
            }
        }

        if (obj->private_data->type) {
            z_free(obj->private_data->type);
        }
        z_free(obj->private_data);
        z_free(obj);
    }
}

void z_object_add_free_callback(ZObject *obj, ZCallback free_callback) {
    ZObjectCallback *callback;

    if (!obj || !free_callback) {
        return;
    }

    callback = z_malloc(sizeof(ZObjectCallback));
    if (!callback) {
        return;
    }
    callback->callback = free_callback;
    callback->next = obj->free_callbacks;
    obj->free_callbacks = callback;
}

size_t z_object_get_size(ZObject *obj) {
    if (!obj)  {
        return 0;
    }

    return obj->private_data->obj_size;
}

const char *z_object_get_type(ZObject *obj) {
    if (!obj) {
        return NULL;
    }

    return obj->private_data->type;
}

