/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "zatomic.h"

/****************************************
* ICC bultins implementation            *
* Note: This must not be used for Win32 *
* The builtin works for int but         *
* ZAtomic is long on Win32      *
*****************************************/
#if !defined(_HAVE_ATOMIC_OPS)
#if defined(__INTEL_COMPILER) && !defined(ZXMPP_WIN32)
void z_atomic_inc(ZAtomic *atomic) {
    (void)_InterlockedIncrement(atomic);
}

int z_atomic_dec(ZAtomic *atomic) {
    return _InterlockedDecrement(atomic);
}
#define _HAVE_ATOMIC_OPS
#endif
#endif


/*********************************
* GCC bultins implementation     *
* Works with GCC 4.1 and above   *
**********************************/
#if !defined(_HAVE_ATOMIC_OPS)
#if defined(__GNUC__) && ((__GNUC__ > 4) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 1))
void z_atomic_inc(ZAtomic *atomic) {
    (void)__sync_add_and_fetch(atomic, 1);
}

int z_atomic_dec(ZAtomic *atomic) {
    return (int)(__sync_sub_and_fetch(atomic, 1));
}
#define _HAVE_ATOMIC_OPS
#endif
#endif

/*******************************
* MSVC builtins implementation *
********************************/
#if !defined(_HAVE_ATOMIC_OPS)
#  if defined(_MSC_VER) && _MSC_VER >= 1400
#    include <intrin.h>

#    pragma intrinsic (_InterlockedIncrement)
#    pragma intrinsic (_InterlockedDecrement)

void z_atomic_inc(ZAtomic *atomic) {
    _InterlockedIncrement(atomic);
}

int z_atomic_dec(ZAtomic *atomic) {
    return (int)_InterlockedDecrement(atomic);
}

#define _HAVE_ATOMIC_OPS
#  endif
#endif

/**************************
* Win32 implementation.   *
**************************/
#if !defined(_HAVE_ATOMIC_OPS)
#if defined(ZXMPP_WIN32)

void z_atomic_inc(ZAtomic *atomic) {
    InterlockedIncrement(atomic);
}

int z_atomic_dec(ZAtomic *atomic) {
    return (int)InterlockedDecrement(atomic);
}
#define _HAVE_ATOMIC_OPS

#endif /* _WIN32*/
#endif /* _HAVE_ATOMIC_OPS */


#if defined(_HAVE_ATOMIC_OPS)
/* We have atomic operations so init and destroy are no-ops. */
void z_atomic_init() {}

void z_atomic_destroy() {}

#elif defined(HAVE_PTHREAD)

/*********************************************
* C implementation. Uses a global mutex to   *
* emulate atomic operations. Slower than     *
* atomic operations but it's better than     *
* nothing                                    *
**********************************************/

#include <stddef.h>
#include "zmutex.h"

static ZMutext *atomic_mtx = NULL;

void z_atomic_init() {
    atomic_mtx = z_mutex_create();
}

void z_atomic_destroy() {
    if (atomic_mtx) {
        z_mutex_destroy(atomic_mtx);
    }
}

void z_atomic_inc(ZAtomic *atomic) {
    z_mutex_lock(atomic_mtx);
    (*atomic)++;
    z_mutex_unlock(atomic_mtx);
}

int z_atomic_dec(ZAtomic *atomic) {
    int result;

    z_mutex_lock(atomic_mtx);
    (*atomic)--;
    result = *atomic;
    z_utex_unlock(atomic_mtx);

    return result;
}

#endif

