/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "zauth_processor_c.h"
#include "zauth_c.h"
#include "zobject_c.h"
#include "zstring.h"
#include "zstdlib.h"
#include "zlist.h"
#include <stdio.h>


typedef struct ZAuthProcessorMechanism {
    ZObject object;
    char *name;
    ZAuthMechanism *mechanism;
    struct ZAuthProcessorMechanism *next;
}ZAuthProcessorMechanism;

struct ZAuthProcessor {
    ZObject object;
    ZAuthProcessorMechanism *mechanisms;
    ZAuthProcessorMechanism *curr;
    ZAuthProcessorCallback callback;
    void *data;
};


static void z_auth_mechanism_free(void *data) {
    ZAuthProcessorMechanism *mechanism = (ZAuthProcessorMechanism*)data;
    z_free(mechanism->name);
    z_auth_mechanism_destroy(mechanism->mechanism);
}

static void z_auth_processor_mechanism_item_free(ZAuthProcessorMechanism *mechanism) {
    z_object_unref((ZObject*)mechanism);
}

static void z_auth_processor_free(void *data) {
    ZAuthProcessor *processor = (ZAuthProcessor*)data;
    ZAuthProcessorMechanism *curr = NULL;

    z_list_free_func(curr,processor->mechanisms,z_auth_processor_mechanism_item_free);


}

ZAuthProcessor *z_auth_processor_new(ZAuthProcessorCallback callback,void *data) {
    ZAuthProcessor *processor;

    if(!callback) {
        return NULL;
    }
    processor = Z_OBJECT_NEW(ZAuthProcessor);
    if(!processor) {
        return NULL;
    }
    z_object_add_free_callback((ZObject*)processor,z_auth_processor_free);

    processor->callback = callback;
    processor->data = data;

    return processor;
}

static int sort_list(ZAuthProcessorMechanism *mechanism1,ZAuthProcessorMechanism *mechanism2) {
    return z_auth_mechanism_get_priority(mechanism1->name) < z_auth_mechanism_get_priority(mechanism2->name);
}

int z_auth_processor_add_mechanism(ZAuthProcessor *processor,const char *name) {
    ZAuthProcessorMechanism *mechanism_item;

    if(!processor || !name) {
        return 1;
    }
    if(!z_auth_mechanism_is_supported(name)) {
        return 1;
    }

    mechanism_item = Z_OBJECT_NEW(ZAuthProcessorMechanism);
    if(!mechanism_item) {
        return 1;
    }
    z_object_add_free_callback((ZObject*)mechanism_item,z_auth_mechanism_free);
    mechanism_item->name = z_strdup(name);
    if(!mechanism_item->name) {
        z_object_unref((ZObject*)mechanism_item);
        return 1;
    }

    if(!processor->mechanisms) {
        processor->mechanisms = mechanism_item;
    }else{
        ZAuthProcessorMechanism *curr;
        curr = processor->mechanisms;
        while (curr->next) curr = curr->next;
        curr->next = mechanism_item;
    }

    z_list_sort(processor->mechanisms,sort_list);
    return 0;
}

int z_auth_processor_proceed(ZAuthProcessor *processor,ZConnection *connection) {
    if(!processor || !connection) {
        return 1;
    }
    if(!processor->curr) {
        processor->curr = processor->mechanisms;
    }else{
        if(processor->curr->mechanism) {
            z_auth_mechanism_destroy(processor->curr->mechanism);
            processor->curr->mechanism = NULL;
        }
        processor->curr = processor->curr->next;
    }

    if(!processor->curr) {
        return 1;
    }

    processor->curr->mechanism = z_auth_mechanism_create(processor->curr->name);
    if(!processor->curr->mechanism) {
        return 1;
    }

    return z_auth_mechanism_proceed(processor->curr->mechanism,processor,connection);
}

int z_auth_processor_set_status(ZAuthProcessor *processor,int status) {
    if(!processor) {
        return 1;
    }

    return processor->callback(processor,status,processor->data);
}



