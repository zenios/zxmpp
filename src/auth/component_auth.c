/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include <stdio.h>
#include "auth/component_auth.h"
#include "zhelper.h"
#include "zstanza.h"
#include "zstdlib_c.h"
#include "zstring.h"
#include "zbase64.h"
#include "zobject.h"
#include "zconstants.h"
#include "zsha.h"


struct ZAuthMechanismPrivate{
    ZAuthProcessor *auth_processor;
    ZConnection *connection;
};


int z_auth_mechanism_component_handle_error(ZXMPP_UNUSED ZStanzaFilter *stanza_filter,ZXMPP_UNUSED ZStanza *stanza,void *data) {
    ZAuthMechanismPrivate *auth_mechanism_plain = (ZAuthMechanismPrivate*)data;
    if(z_auth_processor_set_status(auth_mechanism_plain->auth_processor,Z_AUTH_PROCESSOR_FAILURE)) {
        return -1;
    }
    return 0;
}

int z_auth_mechanism_component_handle_success(ZXMPP_UNUSED ZStanzaFilter *stanza_filter,ZXMPP_UNUSED ZStanza *stanza,void *data) {
    ZAuthMechanismPrivate *auth_mechanism_plain = (ZAuthMechanismPrivate*)data;
    if(z_auth_processor_set_status(auth_mechanism_plain->auth_processor,Z_AUTH_PROCESSOR_SUCCESS)) {
        return -1;
    }
    return 0;
}

static int z_auth_mechanism_component_proceed(ZAuthMechanismPrivate *auth_mechanism,ZAuthProcessor *processor,ZConnection *connection) {
    ZStanzaFilter *filter;
    ZStanza *auth_stanza;
    ZStanzaFilterRule *rule;

    auth_mechanism->auth_processor = processor;
    auth_mechanism->connection = connection;

    filter = z_connection_get_filter(connection);
    rule = z_stanza_filter_add_rule(filter,z_auth_mechanism_component_handle_success,auth_mechanism,Z_FILTER_RULE_NAME,"handshake",Z_FILTER_RULE_DONE);
    if(!rule) {
        return 1;
    }
    z_stanza_filter_rule_set_single(rule);

    rule = z_stanza_filter_add_rule(filter,z_auth_mechanism_component_handle_error,auth_mechanism,Z_FILTER_RULE_NAME,"stream:error",Z_FILTER_RULE_DONE);
    if(!rule) {
        return 1;
    }
    z_stanza_filter_rule_set_single(rule);


    auth_stanza =  z_helper_create_component_auth_stanza(z_connection_get_stream_id(connection),z_connection_get_password(connection));
    if(!auth_stanza) {
        return 1;
    }

    if(z_connection_send_stanza(connection,auth_stanza)) {
        z_object_unref((ZObject*)auth_stanza);
        return 1;
    }
    z_object_unref((ZObject*)auth_stanza);

    return 0;
}


void z_auth_mechanism_component_destroy(ZAuthMechanismPrivate *auth_mechanism) {
    z_free(auth_mechanism);
}

ZAuthMechanismPrivate *z_auth_mechanism_component_create(void) {
    ZAuthMechanismPrivate *auth_mechanism = z_malloc(sizeof(ZAuthMechanismPrivate));
    if(!auth_mechanism) {
        return NULL;
    }
    z_memset(auth_mechanism,0,sizeof(ZAuthMechanismPrivate));
    return auth_mechanism;
}

struct ZAuthMechanismType z_auth_mechanism_component = {
    ZXMPP_COMPONENT_AUTH_NAME,
    0,
    {
        z_auth_mechanism_component_create,
        z_auth_mechanism_component_destroy,
        z_auth_mechanism_component_proceed
    }
};


