/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include <stdio.h>
#include "auth/anonymous.h"
#include "zhelper.h"
#include "zstanza.h"
#include "zstdlib_c.h"
#include "zstring.h"
#include "zbase64.h"
#include "zobject.h"
#include "zconstants.h"

#define MECHANISM_NAME "ANONYMOUS"


struct ZAuthMechanismPrivate{
    ZAuthProcessor *auth_processor;
    ZConnection *connection;
};

int z_auth_mechanism_anonymous_handle_response(ZXMPP_UNUSED ZStanzaFilter *stanza_filter,ZStanza *stanza,void *data) {
    ZAuthMechanismPrivate *auth_mechanism_plain = (ZAuthMechanismPrivate*)data;
    if(z_strcmp(z_stanza_get_name(stanza),"success") == 0) {
        if(z_auth_processor_set_status(auth_mechanism_plain->auth_processor,Z_AUTH_PROCESSOR_SUCCESS)) {
            return -1;
        }
    }else {
        if(z_auth_processor_set_status(auth_mechanism_plain->auth_processor,Z_AUTH_PROCESSOR_FAILURE)) {
            return -1;
        }
    }
    return 0;
}

static int z_auth_mechanism_anonymous_proceed(ZAuthMechanismPrivate *auth_mechanism,ZAuthProcessor *processor,ZConnection *connection) {
    ZStanzaFilter *filter;
    ZStanza *auth_stanza;
    ZStanzaFilterRule *rule;

    auth_mechanism->auth_processor = processor;
    auth_mechanism->connection = connection;

    filter = z_connection_get_filter(connection);
    rule = z_stanza_filter_add_rule(filter,z_auth_mechanism_anonymous_handle_response,auth_mechanism,Z_FILTER_RULE_NS,ZXMPP_NS_SASL,Z_FILTER_RULE_DONE);
    if(!rule) {
        return 1;
    }
    z_stanza_filter_rule_set_single(rule);


    auth_stanza =  z_helper_create_auth_stanza(MECHANISM_NAME);
    if(!auth_stanza) {
        return 1;
    }

    if(z_connection_send_stanza(connection,auth_stanza)) {
        z_object_unref((ZObject*)auth_stanza);
        return 1;
    }
    z_object_unref((ZObject*)auth_stanza);

    return 0;
}


void z_auth_mechanism_anonymous_destroy(ZAuthMechanismPrivate *auth_mechanism) {
    z_free(auth_mechanism);
}

ZAuthMechanismPrivate *z_auth_mechanism_anonymous_create(void) {
    ZAuthMechanismPrivate *auth_mechanism = z_malloc(sizeof(ZAuthMechanismPrivate));
    if(!auth_mechanism) {
        return NULL;
    }
    z_memset(auth_mechanism,0,sizeof(ZAuthMechanismPrivate));
    return auth_mechanism;
}

struct ZAuthMechanismType z_auth_mechanism_anonymous = {
    MECHANISM_NAME,
    0,
    {
        z_auth_mechanism_anonymous_create,
        z_auth_mechanism_anonymous_destroy,
        z_auth_mechanism_anonymous_proceed
    }
};

