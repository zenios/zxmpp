/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include <stdio.h>
#include <string.h>
#if !defined(HAVE_ARC4RANDOM)
# include <stdlib.h>
#endif
#include "auth/digest_md5.h"
#include "zhelper.h"
#include "zstanza.h"
#include "zstdlib_c.h"
#include "zstring.h"
#include "zbase64.h"
#include "zobject.h"
#include "zconstants.h"
#include "zmd5.h"

#define MECHANISM_NAME "DIGEST-MD5"
#define CNONCESIZE (32)


struct ZAuthMechanismPrivate{
    ZAuthProcessor *auth_processor;
    ZConnection *connection;
    ZStanzaFilterRule *challenge_rule;
};

int z_auth_mechanism_digest_md5_handle_response(ZXMPP_UNUSED ZStanzaFilter *stanza_filter,ZStanza *stanza,void *data) {
    ZAuthMechanismPrivate *auth_mechanism = (ZAuthMechanismPrivate*)data;
    if(z_strcmp(z_stanza_get_name(stanza),"success") == 0) {
        z_stanza_filter_remove_rule(auth_mechanism->challenge_rule);
        if(z_auth_processor_set_status(auth_mechanism->auth_processor,Z_AUTH_PROCESSOR_SUCCESS)) {
            return -1;
        }
    }else if(z_strcmp(z_stanza_get_name(stanza),"challenge") == 0) {
        ZStanza *final_stanza = z_stanza_new("response");
        if(!final_stanza) {
            z_auth_processor_set_status(auth_mechanism->auth_processor,Z_AUTH_PROCESSOR_FAILURE);
            return -1;
        }
        z_stanza_add_attribute(final_stanza,"xmlns",ZXMPP_NS_SASL);
        if(z_connection_send_stanza(auth_mechanism->connection,final_stanza)) {
            return -1;
        }
        z_object_unref((ZObject*)final_stanza);
    }else if(z_strcmp(z_stanza_get_name(stanza),"failure") == 0) {
        ZStanza *child_stanza = z_stanza_get_children(stanza);
        if(!child_stanza || (z_strcmp(z_stanza_get_name(child_stanza),"not-authorized") != 0)) {
            if(z_auth_processor_set_status(auth_mechanism->auth_processor,Z_AUTH_PROCESSOR_FAILURE)) {
                return -1;
            }
        }else{
            if(z_auth_processor_set_status(auth_mechanism->auth_processor,Z_AUTH_PROCESSOR_WRONG_CREDENTIALS)) {
                return -1;
            }
        }
    }else{
        if(z_auth_processor_set_status(auth_mechanism->auth_processor,Z_AUTH_PROCESSOR_FAILURE)) {
            return -1;
        }

    }
    return 0;
}

static ZStanza *handle_response(ZAuthMechanismPrivate *auth_mechanism,const char *decoded_challenge) {
    ZStanza *response;
    char *nonce = NULL, *nonce_eos = NULL;
	const char *realm = NULL;
	char *realm_eos = NULL;
	unsigned char a1_h[16], a1[33], a2[33], response_value[33];
	size_t responsedatastrlen = 0;
	char *responsedatastr = NULL, *responsedatab64 = NULL;
	const char *username;
	const char *server;
	const char *password;
	ZMd5 *md5;
	int i = 0;
	char cnonce[CNONCESIZE];


	server = z_jid_get(z_connection_get_jid(auth_mechanism->connection),Z_JID_NODE);
	username = z_jid_get(z_connection_get_jid(auth_mechanism->connection),Z_JID_USERNAME);
	password = z_connection_get_password(auth_mechanism->connection);

	/* Find the nonce (mandatory) and realm (optional) attributes */
	if ((nonce = strstr(decoded_challenge, "nonce=\"")) != NULL) {
		nonce += 7;
		nonce_eos = strchr(nonce, '\"');
	} else {
		return NULL;
	}
	if ((realm = strstr(decoded_challenge, "realm=\"")) != NULL) {
		realm += 7;
		realm_eos = strchr(realm, '\"');
	} else {
		realm = server;
	}
	if (nonce_eos != NULL) {
		*nonce_eos = '\0';
	}
	if (realm_eos != NULL) {
		*realm_eos = '\0';
	}
    for (i = 0; i < 4; ++i) {
#if defined(HAVE_ARC4RANDOM)
        sprintf( cnonce + i*8, "%08x", arc4random());
#else
        sprintf( cnonce + i*8, "%08x", rand());
#endif
    }
	cnonce[CNONCESIZE-1] = '\0';

	md5 = z_md5_new();
	if(!md5) {
	    return NULL;
	}
	z_md5_hash(md5, (const unsigned char *)username, z_strlen(username), 0);
	z_md5_hash(md5, (const unsigned char *)":", 1, 0);
	z_md5_hash(md5, (const unsigned char *)realm, z_strlen(realm), 0);
	z_md5_hash(md5, (const unsigned char *)":", 1, 0);
	z_md5_hash(md5, (const unsigned char *)password, z_strlen(password), 1);
	z_md5_digest(md5, a1_h);
	z_md5_reset(md5);
	z_md5_hash(md5, (const unsigned char *)a1_h, 16, 0);
	z_md5_hash(md5, (const unsigned char *)":", 1, 0);
	z_md5_hash(md5, (const unsigned char *)nonce, z_strlen(nonce), 0);
	z_md5_hash(md5, (const unsigned char *)":", 1, 0);
	z_md5_hash(md5, (const unsigned char *)cnonce, z_strlen(cnonce), 1);
	z_md5_print(md5,(char*)a1);
	z_md5_reset(md5);
	z_md5_hash(md5, (const unsigned char *)"AUTHENTICATE:xmpp/", 18, 0);
	z_md5_hash(md5, (const unsigned char *)server, z_strlen(server), 1);
	z_md5_print(md5,(char*)a2);
	z_md5_reset(md5);
	z_md5_hash(md5, (const unsigned char *)a1, 32, 0);
	z_md5_hash(md5, (const unsigned char *)":", 1, 0);
	z_md5_hash(md5, (const unsigned char *)nonce, z_strlen(nonce), 0);
	z_md5_hash(md5, (const unsigned char *)":00000001:", 10, 0);
	z_md5_hash(md5, (const unsigned char *)cnonce, z_strlen(cnonce), 0);
	z_md5_hash(md5, (const unsigned char *)":auth:", 6, 0);
	z_md5_hash(md5, (const unsigned char *)a2, 32, 1);
	z_md5_print(md5,(char*)response_value);

	z_object_unref((ZObject*)md5);

	responsedatastrlen =z_strlen(username) + z_strlen(realm)+ z_strlen(nonce) + z_strlen(server)+ 4*8 + 136;
    responsedatastr = z_malloc(responsedatastrlen);
	if (!responsedatastr) {
		return NULL;
	}
	snprintf(
			responsedatastr,
			responsedatastrlen,
			"username=\"%s\",realm=\"%s\",nonce=\"%s\",cnonce=\"%s\","
				"nc=00000001,digest-uri=\"xmpp/%s\",qop=auth,response=%s,"
				"charset=utf-8",
			username,
			realm,
			nonce,
			cnonce,
			server,
			response_value);

    responsedatab64 = z_base64_encode(responsedatastr, 0);
    z_free(responsedatastr);
	if (!responsedatab64) {
		return NULL;
	}

	response = z_stanza_new("response");
	if(!response) {
	    z_free(responsedatab64);
	    return NULL;
	}
	z_stanza_add_attribute(response,"xmlns",ZXMPP_NS_SASL);
	z_stanza_append_text(response,responsedatab64,0);
	z_free(responsedatab64);
    return response;

}

int z_auth_mechanism_digest_md5_handle_challenge(ZXMPP_UNUSED ZStanzaFilter *stanza_filter,ZStanza *stanza,void *data) {
    ZAuthMechanismPrivate *auth_mechanism = (ZAuthMechanismPrivate*)data;
    if(z_strcmp(z_stanza_get_name(stanza),"challenge") == 0) {
        const char *challenge = z_stanza_get_text(stanza);
        char *decoded_challenge = z_base64_decode(challenge);
        ZStanza *response = handle_response(auth_mechanism,decoded_challenge);
        z_free(decoded_challenge);
        if(!response) {
            if(z_auth_processor_set_status(auth_mechanism->auth_processor,Z_AUTH_PROCESSOR_FAILURE)) {
                return -1;
            }
            return 0;
        }

        auth_mechanism->challenge_rule = z_stanza_filter_add_rule(stanza_filter,z_auth_mechanism_digest_md5_handle_response,auth_mechanism,Z_FILTER_RULE_NS,ZXMPP_NS_SASL,Z_FILTER_RULE_DONE);
        if(!auth_mechanism->challenge_rule) {
            return -1;
        }
        if(z_connection_send_stanza(auth_mechanism->connection,response)) {
            return -1;
        }
        z_object_unref((ZObject*)response);
    }else{
        if(z_auth_processor_set_status(auth_mechanism->auth_processor,Z_AUTH_PROCESSOR_FAILURE)) {
            return -1;
        }
    }
    return 0;
}

static int z_auth_mechanism_digest_md5_proceed(ZAuthMechanismPrivate *auth_mechanism,ZAuthProcessor *processor,ZConnection *connection) {
    ZStanzaFilter *filter;
    ZStanza *auth_stanza;
    ZStanzaFilterRule *rule;

    auth_mechanism->auth_processor = processor;
    auth_mechanism->connection = connection;

    filter = z_connection_get_filter(connection);

    rule = z_stanza_filter_add_rule(filter,z_auth_mechanism_digest_md5_handle_challenge,auth_mechanism,Z_FILTER_RULE_NS,ZXMPP_NS_SASL,Z_FILTER_RULE_DONE);
    if(!rule) {
        return 1;
    }
    z_stanza_filter_rule_set_single(rule);

    auth_stanza =  z_helper_create_auth_stanza(MECHANISM_NAME);
    if(!auth_stanza) {
        return 1;
    }
    if(z_connection_send_stanza(connection,auth_stanza)) {
        z_object_unref((ZObject*)auth_stanza);
        return 1;
    }
    z_object_unref((ZObject*)auth_stanza);

    return 0;
}

void z_auth_mechanism_digest_md5_destroy(ZAuthMechanismPrivate *auth_mechanism) {
    z_stanza_filter_remove_rule(auth_mechanism->challenge_rule);
    z_free(auth_mechanism);
}


ZAuthMechanismPrivate *z_auth_mechanism_digest_md5_create(void) {
    ZAuthMechanismPrivate *auth_mechanism = z_malloc(sizeof(ZAuthMechanismPrivate));
    if(!auth_mechanism) {
        return NULL;
    }
    z_memset(auth_mechanism,0,sizeof(ZAuthMechanismPrivate));
    return auth_mechanism;
}

struct ZAuthMechanismType z_auth_mechanism_digest_md5 = {
    MECHANISM_NAME,
    2,
    {
        z_auth_mechanism_digest_md5_create,
        z_auth_mechanism_digest_md5_destroy,
        z_auth_mechanism_digest_md5_proceed,
    }
};

