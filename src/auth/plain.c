/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include <stdio.h>
#include "auth/plain.h"
#include "zhelper.h"
#include "zstanza.h"
#include "zstdlib_c.h"
#include "zstring.h"
#include "zbase64.h"
#include "zobject.h"
#include "zconstants.h"

#define MECHANISM_NAME "PLAIN"


struct ZAuthMechanismPrivate{
    ZAuthProcessor *auth_processor;
    ZConnection *connection;
};


int z_auth_mechanism_plain_handle_response(ZXMPP_UNUSED ZStanzaFilter *stanza_filter,ZStanza *stanza,void *data) {
    ZAuthMechanismPrivate *auth_mechanism_plain = (ZAuthMechanismPrivate*)data;
    if(z_strcmp(z_stanza_get_name(stanza),"success") == 0) {
        if(z_auth_processor_set_status(auth_mechanism_plain->auth_processor,Z_AUTH_PROCESSOR_SUCCESS)) {
            return -1;
        }
    }else if(z_strcmp(z_stanza_get_name(stanza),"failure") == 0) {
        ZStanza *child_stanza = z_stanza_get_children(stanza);
        if(!child_stanza || (z_strcmp(z_stanza_get_name(child_stanza),"not-authorized") != 0)) {
            if(z_auth_processor_set_status(auth_mechanism_plain->auth_processor,Z_AUTH_PROCESSOR_FAILURE)) {
                return -1;
            }
            return 0;
        }
        if(z_auth_processor_set_status(auth_mechanism_plain->auth_processor,Z_AUTH_PROCESSOR_WRONG_CREDENTIALS)) {
            return -1;
        }
    }else{
        if(z_auth_processor_set_status(auth_mechanism_plain->auth_processor,Z_AUTH_PROCESSOR_FAILURE)) {
            return -1;
        }
    }
    return 0;
}


static char *create_base64_auth_stanza(const char *username,const char *password) {
	size_t authdatalen = 0;
	char *authdatastr = NULL;
	char *authdatab64 = NULL;

	authdatalen = z_strlen(username) + z_strlen(password) + 3;

	authdatastr = z_malloc(authdatalen);
	if (!authdatastr) {
		return NULL;
	}
	snprintf(authdatastr,authdatalen,"%c%s%c%s",'\0', username, '\0', password);
	authdatab64 = z_base64_encode(authdatastr, authdatalen-1);
	z_free(authdatastr);
	if (!authdatab64) {
        return NULL;
	}
    return authdatab64;
}

static int z_auth_mechanism_plain_proceed(ZAuthMechanismPrivate *auth_mechanism,ZAuthProcessor *processor,ZConnection *connection) {
    ZStanzaFilter *filter;
    ZStanza *auth_stanza;
    char *id_stanza;
    const char *auth_id;
    ZStanzaFilterRule *rule;

    auth_mechanism->auth_processor = processor;
    auth_mechanism->connection = connection;

    filter = z_connection_get_filter(connection);
    rule = z_stanza_filter_add_rule(filter,z_auth_mechanism_plain_handle_response,auth_mechanism,Z_FILTER_RULE_NS,ZXMPP_NS_SASL,Z_FILTER_RULE_DONE);
    if(!rule) {
        return 1;
    }
    z_stanza_filter_rule_set_single(rule);


    auth_stanza =  z_helper_create_auth_stanza(MECHANISM_NAME);
    if(!auth_stanza) {
        return 1;
    }

    auth_id = z_jid_get(z_connection_get_jid(auth_mechanism->connection),Z_JID_USERNAME);
    if(!auth_id) {
        z_object_unref((ZObject*)auth_stanza);
        return 1;
    }

    id_stanza = create_base64_auth_stanza(auth_id,z_connection_get_password(connection));
    if(!id_stanza) {
        z_object_unref((ZObject*)auth_stanza);
        return 1;
    }

    z_stanza_append_text(auth_stanza,id_stanza,0);
    z_free(id_stanza);

    if(z_connection_send_stanza(connection,auth_stanza)) {
        z_object_unref((ZObject*)auth_stanza);
        return 1;
    }
    z_object_unref((ZObject*)auth_stanza);

    return 0;
}


void z_auth_mechanism_plain_destroy(ZAuthMechanismPrivate *auth_mechanism) {
    z_free(auth_mechanism);
}

ZAuthMechanismPrivate *z_auth_mechanism_plain_create(void) {
    ZAuthMechanismPrivate *auth_mechanism = z_malloc(sizeof(ZAuthMechanismPrivate));
    if(!auth_mechanism) {
        return NULL;
    }
    z_memset(auth_mechanism,0,sizeof(ZAuthMechanismPrivate));
    return auth_mechanism;
}

struct ZAuthMechanismType z_auth_mechanism_plain = {
    MECHANISM_NAME,
    1,
    {
        z_auth_mechanism_plain_create,
        z_auth_mechanism_plain_destroy,
        z_auth_mechanism_plain_proceed,
    }
};
