/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "zmutex.h"
#include "zstdlib_c.h"
#include "zstring.h"

#if defined(ZXMPP_WIN32)
# include <windows.h>
#endif


#if defined(HAVE_PTHREAD)
# include <pthread.h>
#endif



struct ZMutex {
#if defined(ZXMPP_WIN32)
    CRITICAL_SECTION critical_section;
#elif defined(HAVE_PTHREAD)
    pthread_mutex_t pthread_mutex;
#endif
};

ZMutex *z_mutex_create() {
    ZMutex *mutex = z_malloc(sizeof(ZMutex));
    if(!mutex) {
        return NULL;
    }
    z_memset(mutex,0,sizeof(ZMutex));


#if defined(ZXMPP_WIN32)
    InitializeCriticalSection(&mutex->critical_section);
#elif defined(HAVE_PTHREAD)
    if(pthread_mutex_init(&mutex->pthread_mutex, 0)) {
        z_free(mutex);
        return NULL;
    }
#endif

    return mutex;
}

void z_mutex_destroy(ZMutex *mutex) {
    if(!mutex) {
        return;
    }
#if defined(ZXMPP_WIN32)
    DeleteCriticalSection(&mutex->critical_section);
#elif defined(HAVE_PTHREAD)
    pthread_mutex_destroy(&mutex->pthread_mutex);
#endif

    z_free(mutex);
}

int z_mutex_lock(ZMutex *mutex) {
    int ret = 1;

    if(!mutex) {
        return ret;
    }
#if defined(ZXMPP_WIN32)
    EnterCriticalSection(&mutex->critical_section);
    ret = 0;
#elif defined(HAVE_PTHREAD)
    ret = pthread_mutex_lock(&mutex->pthread_mutex);
#endif
    return ret;
}

int z_mutex_unlock(ZMutex *mutex) {
    int ret = 1;

    if(!mutex) {
        return ret;
    }
#if defined(ZXMPP_WIN32)
    LeaveCriticalSection(&mutex->critical_section);
    ret = 0;
#elif defined(HAVE_PTHREAD)
    ret = pthread_mutex_unlock(&mutex->pthread_mutex);
#endif
    return ret;
}


int z_mutex_trylock(ZMutex *mutex) {
    int ret = 1;

    if(!mutex) {
        return ret;
    }
#if defined(ZXMPP_WIN32)
    ret =  TryEnterCriticalSection( &mutex->critical_section ) ? 0 : 1;
#elif defined(HAVE_PTHREAD)
    ret =  pthread_mutex_unlock(&mutex->pthread_mutex);
#endif
    return ret;
}








