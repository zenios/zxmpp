/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include <string.h>
#include <stdio.h>
#include "zsha.h"
#include "zstdlib_c.h"
#include "zstring.h"
#include "zobject_c.h"

/*Taken from gloox.Thanks :)*/

struct ZSha {
    ZObject parent_object;
    unsigned int sha_hash[5];
    unsigned int length_low;
    unsigned int length_height;
    unsigned char message_block[64];
    int message_block_index;
    int finished;
    int corrupted;

};

static unsigned int shift(int bits, unsigned word) {
    return ( ( word << bits ) & 0xFFFFFFFF) | ( ( word & 0xFFFFFFFF ) >> ( 32-bits ) );
}


static void process(ZSha *sha) {
    const unsigned int K[] = { 0x5A827999,0x6ED9EBA1,0x8F1BBCDC,0xCA62C1D6 };
    int t;
    unsigned int temp;
    unsigned int W[80];
    unsigned int A, B, C, D, E;

    for( t = 0; t < 16; t++ ) {
        W[t] =  ((unsigned int) sha->message_block[t * 4]) << 24;
        W[t] |= ((unsigned int) sha->message_block[t * 4 + 1]) << 16;
        W[t] |= ((unsigned int) sha->message_block[t * 4 + 2]) << 8;
        W[t] |= ((unsigned int) sha->message_block[t * 4 + 3]);
    }

    for( t = 16; t < 80; ++t ) {
        W[t] = shift( 1, W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16] );
    }

    A = sha->sha_hash[0];
    B = sha->sha_hash[1];
    C = sha->sha_hash[2];
    D = sha->sha_hash[3];
    E = sha->sha_hash[4];

    for( t = 0; t < 20; ++t ) {
        temp = shift( 5, A ) + ( ( B & C ) | ( ( ~B ) & D ) ) + E + W[t] + K[0];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = shift( 30, B );
        B = A;
        A = temp;
    }

    for( t = 20; t < 40; ++t ) {
        temp = shift( 5, A ) + ( B ^ C ^ D ) + E + W[t] + K[1];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = shift( 30, B );
        B = A;
        A = temp;
    }

    for( t = 40; t < 60; ++t ) {
        temp = shift( 5, A ) + ( ( B & C ) | ( B & D ) | ( C & D ) ) + E + W[t] + K[2];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = shift( 30, B );
        B = A;
        A = temp;
    }

    for( t = 60; t < 80; ++t ) {
        temp = shift( 5, A ) + ( B ^ C ^ D ) + E + W[t] + K[3];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = shift( 30, B );
        B = A;
        A = temp;
    }

    sha->sha_hash[0] = ( sha->sha_hash[0] + A ) & 0xFFFFFFFF;
    sha->sha_hash[1] = ( sha->sha_hash[1] + B ) & 0xFFFFFFFF;
    sha->sha_hash[2] = ( sha->sha_hash[2] + C ) & 0xFFFFFFFF;
    sha->sha_hash[3] = ( sha->sha_hash[3] + D ) & 0xFFFFFFFF;
    sha->sha_hash[4] = ( sha->sha_hash[4] + E ) & 0xFFFFFFFF;

    sha->message_block_index = 0;
}

static void pad(ZSha *sha) {
    sha->message_block[sha->message_block_index++] = 0x80;

    if( sha->message_block_index > 56 ) {
        while( sha->message_block_index < 64 ) {
            sha->message_block[sha->message_block_index++] = 0;
        }
        process(sha);
    }

    while( sha->message_block_index < 56 ) {
        sha->message_block[sha->message_block_index++] = 0;
    }

    sha->message_block[56] = (unsigned char) ( ( sha->length_height >> 24 ) & 0xFF );
    sha->message_block[57] = (unsigned char) ( ( sha->length_height >> 16 ) & 0xFF );
    sha->message_block[58] = (unsigned char) ( ( sha->length_height >> 8 ) & 0xFF );
    sha->message_block[59] = (unsigned char) ( ( sha->length_height ) & 0xFF );
    sha->message_block[60] = (unsigned char) ( ( sha->length_low >> 24 ) & 0xFF );
    sha->message_block[61] = (unsigned char) ( ( sha->length_low >> 16 ) & 0xFF );
    sha->message_block[62] = (unsigned char) ( ( sha->length_low >> 8 ) & 0xFF );
    sha->message_block[63] = (unsigned char) ( ( sha->length_low ) & 0xFF );

    process(sha);
}


ZSha *z_sha_new() {
    ZSha *sha_object = Z_OBJECT_NEW(ZSha);
    if(!sha_object) {
        return NULL;
    }
    z_sha_reset(sha_object);

    return sha_object;
}


int z_sha_reset(ZSha *sha) {
    if(!sha) {
        return 1;
    }
    sha->length_low = 0;
    sha->length_height = 0;
    sha->message_block_index = 0;

    sha->sha_hash[0] = 0x67452301;
    sha->sha_hash[1] = 0xEFCDAB89;
    sha->sha_hash[2] = 0x98BADCFE;
    sha->sha_hash[3] = 0x10325476;
    sha->sha_hash[4] = 0xC3D2E1F0;

    sha->finished = 0;
    sha->corrupted = 0;
	return 0;
}

int z_sha_feed(ZSha *sha,const unsigned char* data, unsigned length) {
    if(!sha || !data || !length) {
        return 1;
    }
    if( sha->finished || sha->corrupted ) {
        sha->corrupted = 1;
        return 1;
    }
    while(length-- && !sha->corrupted ) {
        sha->message_block[sha->message_block_index++] = ( *data & 0xFF );

        sha->length_low += 8;
        sha->length_low &= 0xFFFFFFFF;
        if(sha->length_low == 0 ) {
            sha->length_height++;
            sha->length_height &= 0xFFFFFFFF;
            if(sha->length_height == 0 ) {
                sha->corrupted = 1;
            }
        }

        if(sha->message_block_index == 64) {
            process(sha);
        }

        ++data;
    }

    return 0;
}

void z_sha_finalize(ZSha *sha) {
    if( !sha->finished) {
        pad(sha);
        sha->finished = 1;
    }
}

unsigned char *z_sha_to_hex(ZSha *sha) {
    char *buf;
    int i;
    if(!sha || sha->corrupted) {
        return NULL;
    }
    z_sha_finalize(sha);

    buf = z_malloc(41);
    if(!buf) {
        return NULL;
    }
    for(i = 0; i < 20; ++i ) {
      sprintf( buf + i * 2, "%02x", (unsigned char)( sha->sha_hash[i >> 2] >> ( ( 3 - ( i & 3 ) ) << 3 ) ) );
    }

    return (unsigned char*)buf;
}

unsigned char *z_sha_to_binary(ZSha *sha) {
    unsigned char *digest;
    int i;
    if(!sha) {
        return NULL;
    }
    if(!sha->finished) {
        z_sha_finalize(sha);
    }

    digest = z_malloc(sizeof(unsigned char) * 20);
    for(i = 0; i < 20; ++i ) {
        digest[i] = (unsigned char)(sha->sha_hash[i >> 2] >> ( ( 3 - ( i & 3 ) ) << 3 ) );
    }

    return digest;
}
