/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "common.h"

#if defined(HAVE_XML2_PARSER)
#include "zstdlib_c.h"
#include "zstring.h"
#include "parsers/xml2.h"
#include <libxml/parser.h>
#include <libxml/tree.h>

struct ZParserPrivate {
    xmlSAXHandler handlers;
    xmlParserCtxtPtr parser;
    ZStanzaParser *stanza_parser;
};

static void start_element(void *userData, const xmlChar *name, const xmlChar **attrs) {
    ZParserPrivate *prs = (ZParserPrivate*)userData;

    if(z_stanza_parser_start_element(prs->stanza_parser,(const char *)name,(const char**)attrs)) {
        xmlStopParser(prs->parser);
    }
}

static void end_element(void *userData, const xmlChar *name) {
    ZParserPrivate *prs = (ZParserPrivate*)userData;

    if(z_stanza_parser_end_element(prs->stanza_parser,(const char*)name)) {
        xmlStopParser(prs->parser);
    }
}

static void text_tag(void *UserData, const xmlChar *data, int len) {
    ZParserPrivate *prs = (ZParserPrivate*)UserData;

    if(z_stanza_parser_handle_characters(prs->stanza_parser,(const char *)data,len)) {
        xmlStopParser(prs->parser);
    }
}

static int z_xml2_parser_library_init(void) {
	xmlInitParser();
	return 0;
}

static void z_xml2_parser_library_destroy(void) {
    xmlCleanupParser();
}


static ZParserPrivate *z_xml2_parser_create (ZStanzaParser *stanza_parser)
{
	ZParserPrivate *prs;

	prs = z_malloc(sizeof(ZParserPrivate));
	if(!prs) {
        return NULL;
	}
	z_memset(prs,0,sizeof(ZParserPrivate));

	prs->stanza_parser = stanza_parser;
	prs->handlers.startElement = start_element;
	prs->handlers.endElement = end_element;
	prs->handlers.characters = text_tag;
	prs->parser = xmlCreatePushParserCtxt(&prs->handlers,prs,NULL,0,NULL);
	if(!prs->parser) {
        z_free(prs);
        return NULL;
	}
	return prs;
}

static int z_xml2_parser_parse (ZParserPrivate *prs, const char *data, size_t len, int finish)
{
	return xmlParseChunk(prs->parser,data,len,finish);
}

static void z_xml2_parser_reset (ZParserPrivate *prs) {
    xmlCtxtReset(prs->parser);
    z_stanza_parser_reset(prs->stanza_parser);
}

static void z_xml2_parser_destroy (ZParserPrivate *prs) {
    xmlFreeParserCtxt(prs->parser);
    z_free(prs);
}

struct ZParserType z_xml2_parser = {
    "libxml2",
    {
        z_xml2_parser_library_init,
        z_xml2_parser_library_destroy,
        z_xml2_parser_create,
        z_xml2_parser_parse,
        z_xml2_parser_reset,
        z_xml2_parser_destroy,
    }
};
#endif

