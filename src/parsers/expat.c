/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "common.h"

#if defined(HAVE_EXPAT_PARSER)
#include <stdio.h>
#include "parsers/expat.h"
#include "zstdlib_c.h"
#include "zstring.h"
#include <expat.h>

struct ZParserPrivate {
    XML_Parser parser;
    ZStanzaParser *stanza_parser;
    void *user_data;
};

static void startElement(void *userData, const char *name, const char **attrs) {
    ZParserPrivate *prs = (ZParserPrivate*)userData;

    if(z_stanza_parser_start_element(prs->stanza_parser,name,attrs)) {
        XML_StopParser(prs->parser,0);
    }
}

static void endElement(void *userData, const char *name) {
    ZParserPrivate *prs = (ZParserPrivate*)userData;

    if(z_stanza_parser_end_element(prs->stanza_parser,name)) {
        XML_StopParser(prs->parser,0);
    }
}

static void texttag(void *UserData, const XML_Char *data, int len) {
    ZParserPrivate *prs = (ZParserPrivate*)UserData;

    if(z_stanza_parser_handle_characters(prs->stanza_parser,data,len)) {
        XML_StopParser(prs->parser,0);
    }
}

static void z_expat_parser_set_handlers(ZParserPrivate *parser) {
    XML_SetUserData(parser->parser, parser);
    XML_SetElementHandler(parser->parser, startElement, endElement);
    XML_SetCharacterDataHandler(parser->parser,texttag);
}

static ZParserPrivate *z_expat_parser_create(ZStanzaParser *stanza_parser) {
    ZParserPrivate *parser = z_malloc(sizeof(ZParserPrivate));
    if(!parser) {
        return NULL;
    }
    z_memset(parser,0,sizeof(ZParserPrivate));

    parser->stanza_parser = stanza_parser;
    parser->parser = XML_ParserCreate("UTF-8");
    if(!parser->parser) {
        z_free(parser);
        return NULL;
    }

    z_expat_parser_set_handlers(parser);

    return parser;
}

static int z_expat_parser_parse(ZParserPrivate *parser, const char *data, size_t len, int finish) {

    /*Expat returns 1 if everything is ok*/
    return !XML_Parse(parser->parser,data,len,finish);
}

static void z_expat_parser_reset(ZParserPrivate *parser) {
    XML_ParserReset(parser->parser,"UTF-8");

    /*Reassign handlers*/
    z_expat_parser_set_handlers(parser);
    z_stanza_parser_reset(parser->stanza_parser);
}

static void z_expat_parser_destroy(ZParserPrivate *parser) {
    XML_ParserFree(parser->parser);
    z_free(parser);
}

struct ZParserType z_expat_parser = {
    "expat",
    {
        NULL,
        NULL,
        z_expat_parser_create,
        z_expat_parser_parse,
        z_expat_parser_reset,
        z_expat_parser_destroy
    }
};

#endif
