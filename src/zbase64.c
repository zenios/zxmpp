/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include <string.h>
#include "zbase64.h"
#include "zstdlib_c.h"
#include "zstring.h"

static const char base64_charset[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";


char *z_base64_decode(const char *buf) {
	char *res, *save;
	char val;
    const char *foo;
	const char *end;
	int idx;
	size_t len;

	if (!buf) {
		return NULL;
	}

	len = z_strlen(buf) * 6 / 8 + 1;

	save = res = z_malloc(len);
	if (!save) {
		return NULL;
	}
	z_memset(res, 0, len);

	idx = 0;
	end = buf + z_strlen(buf);

	while (*buf && buf < end) {
		if (!(foo = strchr(base64_charset, *buf))) {
			foo = base64_charset;
		}
		val = (int)(foo - base64_charset);
		buf++;
		switch (idx) {
			case 0:
				*res |= val << 2;
				break;
			case 1:
				*res++ |= val >> 4;
				*res |= val << 4;
				break;
			case 2:
				*res++ |= val >> 2;
				*res |= val << 6;
				break;
			case 3:
				*res++ |= val;
				break;
		}
		idx++;
		idx %= 4;
	}
	*res = 0;

	return save;
}

char *z_base64_encode(const char *buf, size_t len) {
	char *res, *save;
	size_t k, t;

	len = (len > 0) ? (len) : (z_strlen(buf));
	save = res = z_malloc((len*8) / 6 + 4);
	if (!save) {
	    return NULL;
	}

	for (k = 0; k < len/3; ++k) {
		*res++ = base64_charset[*buf >> 2];
		t = ((*buf & 0x03) << 4);
		buf++;
		*res++ = base64_charset[t | (*buf >> 4)];
		t = ((*buf & 0x0F) << 2);
		buf++;
		*res++ = base64_charset[t | (*buf >> 6)];
		*res++ = base64_charset[*buf++ & 0x3F];
	}

	switch (len % 3) {
		case 2:
			*res++ = base64_charset[*buf >> 2];
			t =  ((*buf & 0x03) << 4);
			buf++;
			*res++ = base64_charset[t | (*buf >> 4)];
			*res++ = base64_charset[((*buf++ & 0x0F) << 2)];
			*res++ = '=';
			break;
		case 1:
			*res++ = base64_charset[*buf >> 2];
			*res++ = base64_charset[(*buf++ & 0x03) << 4];
			*res++ = '=';
			*res++ = '=';
			break;
	}
	*res = 0;
	return save;
}
