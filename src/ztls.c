/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "ztls_c.h"
#include "zobject_c.h"
#include "ztls_type.h"


#if defined(HAVE_GNU_TLS)
#include "tls/gnutls.h"
#endif

#if defined(HAVE_OPENSSL)
#include "tls/openssl.h"
#endif

struct ZTls {
    ZObject object;
    const ZTlsType *tls;
    ZTlsPrivate *tls_private;
};

static const ZTlsType *tls_types[] = {
#if defined(HAVE_OPENSSL)
    &z_openssl,
#endif
#if defined(HAVE_GNU_TLS)
    &z_gnu_tls,
#endif
    NULL
};

static void z_tls_free(void *data) {
    ZTls *tls = (ZTls*)data;
    if(tls->tls_private && tls->tls->tls_ops.tls_private_destroy) {
        tls->tls->tls_ops.tls_private_destroy(tls->tls_private);
    }
}

int z_tls_init() {
    int i;
    for(i=0;tls_types[i];i++) {
        if(!tls_types[i]->tls_ops.tls_private_create) {
            return 1;
        }
        if(tls_types[i]->tls_ops.tls_private_lib_init) {
            if(tls_types[i]->tls_ops.tls_private_lib_init()) {
                return 1;
            }
        }
    }
    return 0;
}

void z_tls_destroy() {
    int i;
    for(i=0;tls_types[i];i++) {
        if(tls_types[i]->tls_ops.tls_private_lib_destroy) {
            tls_types[i]->tls_ops.tls_private_lib_destroy();
        }
    }
}


ZTls *z_tls_new(sock_t sock) {
    ZTls *tls = Z_OBJECT_NEW(ZTls);
    if(!tls) {
        return NULL;
    }
    z_object_add_free_callback((ZObject*)tls,z_tls_free);
    tls->tls = tls_types[0];
    if(!tls->tls) {
        return NULL;
    }
    tls->tls_private = tls->tls->tls_ops.tls_private_create(sock);
    if(!tls->tls_private) {
        z_object_unref((ZObject*)tls);
        return NULL;
    }
    return tls;
}

int z_tls_handshake(ZTls *tls) {
    if(!tls || !tls->tls->tls_ops.tls_private_handshake) {
        return 1;
    }

    return tls->tls->tls_ops.tls_private_handshake(tls->tls_private);
}

int z_tls_write(ZTls *tls,void *data,size_t len) {
    if(!tls || !tls->tls->tls_ops.tls_private_write) {
        return 0;
    }

    return tls->tls->tls_ops.tls_private_write(tls->tls_private,data,len);
}

int z_tls_read(ZTls *tls,void *buff,size_t len) {
    if(!tls || !tls->tls->tls_ops.tls_private_read) {
        return 0;
    }

    return tls->tls->tls_ops.tls_private_read(tls->tls_private,buff,len);
}
