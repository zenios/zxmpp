/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#include "zhelper.h"
#include "zconstants.h"
#include "zstdlib_c.h"
#include "zobject.h"
#include "zsha.h"
#include "zstring.h"

char *z_helper_create_opening_stream(ZJid *jid,int component) {
    char *value;
    if(!jid) {
        return NULL;
    }
    value = z_malloc(1024);
    if(!value) {
        return NULL;
    }
	sprintf(value,
			"<?xml version='1.0'?>"
            "<stream:stream "
            "xmlns:stream='http://etherx.jabber.org/streams' "
            "xmlns='%s' to='%s' version='1.0'>",
			component ? ZXMPP_NS_COMPONENT : ZXMPP_NS_CLIENT,
			component ? z_jid_get(jid,Z_JID_USERNAME) : z_jid_get(jid,Z_JID_NODE));

    return value;
}


ZStanza *z_helper_create_auth_stanza(const char *mechanism) {
    ZStanza *stanza;
    if(!mechanism) {
        return NULL;
    }

    stanza = z_stanza_new("auth");
	z_stanza_add_attribute(stanza, "xmlns", ZXMPP_NS_SASL);
	z_stanza_add_attribute(stanza, "mechanism", mechanism);

	return stanza;
}

ZStanza *z_helper_create_bind_stanza(const char *id,const char *resource) {
    ZStanza *stanza,*bind_stanza;

    if(!id) {
        return NULL;
    }

    stanza = z_stanza_new("iq");
    if(!stanza) {
        return NULL;
    }
    z_stanza_add_attribute(stanza, "type", "set");

    z_stanza_add_attribute(stanza, "id", id);
    bind_stanza = z_stanza_new("bind");
    if(!bind_stanza) {
        z_object_unref((ZObject*)stanza);
        return NULL;
    }
    z_stanza_add_attribute(bind_stanza, "xmlns", ZXMPP_NS_BIND);
    if(resource) {
        ZStanza *resource_stanza;

        resource_stanza = z_stanza_new("resource");
        if(!resource_stanza) {
            z_object_unref((ZObject*)stanza);
            z_object_unref((ZObject*)bind_stanza);
            return NULL;
        }
        z_stanza_append_text(resource_stanza,resource,0);

        z_stanza_add_child(bind_stanza, resource_stanza);
        z_object_unref((ZObject*)resource_stanza);
    }

    z_stanza_add_child(stanza,bind_stanza);
    z_object_unref((ZObject*)bind_stanza);

    return stanza;
}

ZStanza *z_helper_create_session_stanza(const char *id) {
    ZStanza *stanza,*session;
    if(!id) {
        return NULL;
    }

    stanza = z_stanza_new("iq");
    if(!stanza) {
        return NULL;
    }

    z_stanza_add_attribute(stanza, "type", "set");

    z_stanza_add_attribute(stanza, "id", id);
    session = z_stanza_new("session");
    if(!session) {
        z_object_unref((ZObject*)stanza);
        return NULL;
    }
    z_stanza_add_attribute(session, "xmlns", ZXMPP_NS_SESSION);

    z_stanza_add_child(stanza,session);
    z_object_unref((ZObject*)session);

    return stanza;
}

ZStanza *z_helper_create_compression_stanza(const char *mechanism) {
    ZStanza *stanza;
    ZStanza *child;
    if(!mechanism) {
        return NULL;
    }

    stanza = z_stanza_new("compress");
	z_stanza_add_attribute(stanza, "xmlns", ZXMPP_NS_COMPRESSION);

	child = z_stanza_new("method");
	z_stanza_append_text(child,mechanism,0);

	z_stanza_add_child(stanza,child);
	z_object_unref((ZObject*)child);

	return stanza;

}

ZStanza *z_helper_create_component_auth_stanza(const char *stream_id,const char *password) {
    ZStanza *stanza;
    ZSha *sha;
    unsigned char *sha_value;
    if(!stream_id || !password) {
        return NULL;
    }

    sha = z_sha_new();
    if(!sha) {
        return NULL;
    }

    stanza = z_stanza_new("handshake");
    if(!stanza) {
        z_object_unref((ZObject*)sha);
        return NULL;
    }
    if(z_sha_feed(sha,(const unsigned char*)stream_id,z_strlen(stream_id))) {
        z_object_unref((ZObject*)sha);
        z_object_unref((ZObject*)stanza);
        return NULL;
    }

    if(z_sha_feed(sha,(const unsigned char*)password,z_strlen(password))) {
        z_object_unref((ZObject*)sha);
        z_object_unref((ZObject*)stanza);
        return NULL;
    }

    z_sha_finalize(sha);

    sha_value = z_sha_to_hex(sha);
    if(!sha_value) {
        z_object_unref((ZObject*)sha);
        z_object_unref((ZObject*)stanza);
        return NULL;
    }

    if(z_stanza_append_text(stanza,(char*)sha_value,z_strlen((char*)sha_value))) {
        z_free(sha_value);
        z_object_unref((ZObject*)sha);
        z_object_unref((ZObject*)stanza);
        return NULL;
    }

    z_free(sha_value);
    z_object_unref((ZObject*)sha);
    return stanza;

}

