/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "zstring.h"
#include "zstdlib_c.h"
#include <string.h>
#include <strings.h>

void *z_memset(void *data,int filler,size_t alloc_size) {
    if(!data) {
        return NULL;
    }

    return memset(data,filler,alloc_size);
}

size_t z_strlen(const char *str)
{
    const char *c;
    size_t len;

    if (!str) {
        return 0;
    }

    len = 0;
    for (c = str; *c; c++) {
        len++;
    }

    return len;
}

char *z_strdup(const char *str)
{
    char *dst;
    size_t str_len;

    if (!str) {
        return NULL;
    }

    str_len = z_strlen(str);

    dst = z_malloc(str_len + 1);
    if (!dst) {
        return NULL;
    }

    z_memcpy(dst, str, str_len);
    *(dst + str_len) = '\0';

    return dst;
}

void *z_memcpy(void *dest, const void *src, size_t n)
{
#if defined(__GNUC__) && ((__GNUC__ > 4) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 1))
    return __builtin_memcpy(dest, src, n);
#elif defined(ZXMPP_WIN32)
    if (n > 0) {
        CopyMemory(dest, src, n);
    }
    return dest;
#else
    return memcpy(dest, src, n);
#endif
}

int z_strcmp(const char *str1, const char *str2)
{
    if(!str1 || !str2) {
        return 1;
    }
#if defined(__GNUC__) && ((__GNUC__ > 4) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 1))
    return __builtin_strcmp(str1, str2);
#else
    return strcmp(str1, str2);
#endif
}

int z_strcasecmp(const char *str1, const char *str2)
{
    if(!str1 || !str2) {
        return 1;
    }
#if defined(ZXMPP_WIN32)
    return stricmp(str1, str2);
#else
    return strcasecmp(str1, str2);
#endif
}

