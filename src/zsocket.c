/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "zsocket_c.h"
#include "ztls_c.h"
#include "zobject_c.h"
#include "zstring.h"
#include "zlist.h"
#include "zstdlib_c.h"
#include <stdio.h>

#if !defined(ZXMPP_WIN32)
# include <sys/types.h>
# include <sys/socket.h>
# include <sys/select.h>
# include <netinet/in.h>
# include <string.h>
# include <stdio.h>
# include <netdb.h>
# include <string.h>
# include <unistd.h>
#else
# include <ws2tcpip.h>
#endif

#define BUFFER_SIZE (4096)

typedef struct ZSocketSendQueue {
    char *data;
    size_t len;
    size_t written;
    struct ZSocketSendQueue *next;
}ZSocketSendQueue;

typedef struct ZSocketState {
    int state;
    ZSocketStateCallback callback;
    void *data;
}ZSocketState;

typedef struct ZSocketRead {
    void *buffer;
    ZSocketReadCallback callback;
    void *data;
}ZSocketRead;

struct ZSocket {
    ZObject object;
    sock_t sock;
    int timeout;
    int port;
    char *host;
    ZSocketSendQueue *send_queue;
    ZSocketState state;
    ZSocketRead read;
    ZCompressionMechanism *compression_mechanism;
    ZTls *tls;
    ZSocketReadWriteCallback read_write_callback;
    void *read_write_data;
};


static int z_socket_change_state(ZSocket *sock,int state) {
    int err = 0;
    if(sock->state.state == state) {
        return 0;
    }

    err =  sock->state.callback(sock,state,sock->state.data);
    if(!err) {
        sock->state.state = state;
    }

    return err;
}


static int z_socket_close(ZSocket *sock) {
    int ret = 0;
    if(!sock) {
        return 1;
    }
#if defined(ZXMPP_WIN32)
    ret = closesocket(sock->sock);
#else
    ret = close(sock->sock);
#endif
    return ret;
}

static void z_socket_send_queue_item_free(ZSocketSendQueue *item) {
    if(item->data) {
        z_free(item->data);
    }
    z_free(item);
}

static void z_socket_free(void *data) {
    ZSocket *sock = (ZSocket*)data;
    ZSocketSendQueue *curr;

    if(sock->host) {
        z_free(sock->host);
    }
    if(sock->read.buffer) {
        z_free(sock->read.buffer);
    }
    if(sock->tls) {
        z_object_unref((ZObject*)sock->tls);
    }
    if(sock->compression_mechanism) {
        z_compression_mechanism_destroy(sock->compression_mechanism);
    }
    z_socket_close(sock);
    z_list_free_func(curr,sock->send_queue,z_socket_send_queue_item_free);
}

static int z_socket_connect(ZSocket *sock) {
#if defined (HAVE_GETADDRINFO)
	struct addrinfo hints;
	char portstr[6];
	struct addrinfo *addrinfo = NULL, *addrinfo_itr = NULL;
#else
	struct hostent *hostinfo = NULL;
	size_t itr = 0;
	struct sockaddr_in servaddr;
#endif
    if(!sock) {
        return 1;
    }


#if defined(HAVE_GETADDRINFO)
	hints.ai_flags = AI_CANONNAME;
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = 0;
	hints.ai_addrlen = 0;
	hints.ai_canonname = NULL;
	hints.ai_addr = NULL;
	hints.ai_next = NULL;
	snprintf(portstr, 6, "%u", sock->port);

	if (getaddrinfo(sock->host, portstr, &hints, &addrinfo) != 0) {
		return 1;
	}

	addrinfo_itr = addrinfo;
	while (addrinfo_itr != NULL) {
		sock->sock = socket(addrinfo_itr->ai_family,addrinfo_itr->ai_socktype,addrinfo_itr->ai_protocol);
		if (connect(sock->sock, addrinfo_itr->ai_addr, addrinfo->ai_addrlen)== 0) {
			break;
		}else {
			z_socket_close(sock);
            return 1;
		}

		addrinfo_itr = addrinfo_itr->ai_next;
	}
	freeaddrinfo(addrinfo);
#else
    if ((hostinfo = (struct hostent *)gethostbyname(sock->host)) == NULL) {
		return 1;
	}
	while (hostinfo->h_addr_list[itr] != NULL) {
	    sock->sock = socket(hostinfo->h_addrtype, SOCK_STREAM, 0);
		servaddr.sin_family = hostinfo->h_addrtype;
		servaddr.sin_addr = *((struct in_addr *)hostinfo->h_addr_list[itr]);
		servaddr.sin_port = htons(sock->port);
		if (connect(sock->sock, (struct sockaddr *)&servaddr, sizeof(servaddr))== 0) {
			break;
		}else {
			z_socket_close(sock);
            return 1;
		}
		itr++;
	}
#endif

    return z_socket_change_state(sock,Z_SOCKET_CONNECTING);
}

static int z_socket_write(ZSocket *sock,void *data,size_t len) {
    int ret = 0;
    void *output = NULL;
    size_t output_len = 0;
    size_t left = 0;

    /*Support compression*/
    if(sock->compression_mechanism) {
        z_compression_mechanism_compress(sock->compression_mechanism,data,len,&output,&output_len,&left);
    }else{
        output = data;
        output_len = len;
    }
    do {
        if(sock->tls) {
            ret = z_tls_write(sock->tls,((char*)(output) + ret),output_len);
        }else{
            ret = send(sock->sock, ((char*)(output) + ret), output_len, 0);
        }
        if(ret < 0) {
            break;
        }
        output_len -= ret;
    }while(output_len != 0);

    return (len - left);
}

static int z_socket_read(ZSocket *sock,void *buff,size_t len,char **output) {
    int ret = 0;
    size_t new_size = 0;

    if(sock->tls) {
        ret = z_tls_read(sock->tls,buff,len);
    }else{
        ret = recv(sock->sock,buff,len, 0);
    }

    if(ret > 0) {
        if(sock->compression_mechanism) {
            z_compression_mechanism_decompress(sock->compression_mechanism,buff,ret,(void*)output,&new_size,NULL);
        }else{
            *output = buff;
            new_size = ret;
        }
        return (int)new_size;
    }else{
        return -1;
    }
}


ZSocket *z_socket_new(ZSocketStateCallback state_callback,void *state_callback_data,ZSocketReadCallback read_callback,void *read_callback_data) {
    ZSocket *sock = NULL;
    if(!state_callback || !read_callback) {
        return NULL;
    }

    sock = Z_OBJECT_NEW(ZSocket);
    if(!sock) {
        return NULL;
    }
    z_object_add_free_callback((ZObject*)sock,z_socket_free);
    sock->read.buffer = z_malloc(BUFFER_SIZE);
    if(!sock->read.buffer) {
        z_object_unref((ZObject*)sock);
        return NULL;
    }
    memset(sock->read.buffer,0,BUFFER_SIZE);
    sock->timeout = 1;
    sock->state.callback = state_callback;
    sock->state.data = state_callback_data,
    sock->read.callback = read_callback;
    sock->read.data = read_callback_data;
    return sock;
}

int z_socket_iterate(ZSocket *sock) {
    fd_set rfds, wfds;
    int ret = 0;
    long usec;
    struct timeval tv;

    if(!sock) {
        return 1;
    }

    if(sock->state.state == Z_SOCKET_INITIALIZED) {
        if(z_socket_connect(sock)) {
            return 1;
        }
    }


    if(sock->state.state == Z_SOCKET_CONNECTED) {
        int to_write = 0;
        ZSocketSendQueue *curr,*tmp;

        curr = sock->send_queue;
        while(curr) {
            to_write = curr->len - curr->written;
            if(sock->read_write_callback) {
                sock->read_write_callback(sock,Z_SOCKET_WRITE,&curr->data[curr->written],to_write,sock->read_write_data);
            }
            ret = z_socket_write(sock,&curr->data[curr->written],to_write);


            if(ret < 0) {
                return 1;
            } else if(ret < to_write) {
                curr->written += ret;
                return 0;
            }
            tmp = curr;
            curr = curr->next;
            z_socket_send_queue_item_free(tmp);
            if(!curr) {
                sock->send_queue = NULL;
            }
        }
    }

    usec = sock->timeout * 1000;
    tv.tv_sec = usec / 1000000;
    tv.tv_usec = usec % 1000000;

    FD_ZERO(&rfds);
    FD_ZERO(&wfds);
    if(sock->state.state == Z_SOCKET_CONNECTED) {
        FD_SET(sock->sock, &rfds);
    }else if(sock->state.state == Z_SOCKET_CONNECTING) {
        FD_SET(sock->sock, &wfds);
    }

    ret = select(FD_SETSIZE, &rfds,  &wfds, NULL, &tv);
    if (ret < 0) {
        return 1;
    }

    if(ret == 0) {
        return 0;
    }

    /*Socket is ready for reading*/
    if(FD_ISSET(sock->sock,&rfds)) {
        char *output = NULL;
        ret = z_socket_read(sock,sock->read.buffer,BUFFER_SIZE-1,&output);
        if(ret > 0) {
            if(sock->read_write_callback) {
                sock->read_write_callback(sock,Z_SOCKET_READ,output,ret,sock->read_write_data);
            }
            if(sock->read.callback) {
                if(sock->read.callback(sock,output,ret,sock->read.data)) {
                    return 1;
                }
            }

        }else{
            return 1;
        }
    }

    /*Socket is ready for writting*/
    if(FD_ISSET(sock->sock,&wfds)) {
        if(sock->state.state == Z_SOCKET_CONNECTING) {
            if(z_socket_change_state(sock,Z_SOCKET_CONNECTED)) {
                return 1;
            }
        }
    }

    return 0;
}

int z_socket_send(ZSocket *sock,const char *data,size_t len) {
    ZSocketSendQueue *curr;
    ZSocketSendQueue *item;
    if(!sock || !data) {
        return 1;
    }

    item = z_malloc(sizeof(ZSocketSendQueue));
    if(!item) {
        return 1;
    }
    memset(item,0,sizeof(ZSocketSendQueue));

    if(!len) {
        item->len = z_strlen(data);
    }else{
        item->len = len;
    }
    item->data = z_strdup(data);
    if(!item->data) {
        z_socket_send_queue_item_free(item);
    }

    if(!sock->send_queue) {
        sock->send_queue = item;
    }else{
        curr = sock->send_queue;
        while (curr->next) curr = curr->next;
        curr->next = item;
    }

    return 0;
}

int z_socket_initialize_tls(ZSocket *sock) {
    if(!sock || sock->tls) {
        return 1;
    }

    sock->tls = z_tls_new(sock->sock);
    if(!sock->tls) {
        return 1;
    }

    return z_tls_handshake(sock->tls);
}

int z_socket_set_compression_mechanism(ZSocket *sock,ZCompressionMechanism *compression_mechanism) {
    if(!sock || !compression_mechanism) {
        return 1;
    }

    sock->compression_mechanism = compression_mechanism;

    return 0;
}


int z_socket_set_host(ZSocket *sock,const char *host) {
    if(!sock) {
        return 1;
    }

    if(sock->host) {
        z_free(sock->host);
    }

    sock->host = z_strdup(host);
    if(!sock->host) {
        return 1;
    }

    return 0;
}

int z_socket_set_port(ZSocket *sock,int port) {
    if(!sock || port < 0) {
        return 1;
    }

    sock->port = port;

    return 0;
}

int z_socket_set_read_write_callback(ZSocket *sock,ZSocketReadWriteCallback callback,void *data) {
    if(!sock || !callback) {
        return 1;
    }

    sock->read_write_callback = callback;
    sock->read_write_data = data;

    return 0;
}

int z_socket_get_status(ZSocket *sock) {
    if(!sock) {
        return -1;
    }

    return sock->state.state;

}


