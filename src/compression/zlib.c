/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "common.h"


#if defined(HAVE_ZLIB_COMPRESSION)
#include "compression/zlib.h"
#include "zstring.h"
#include "zstdlib_c.h"
#include "zobject_c.h"
#include <zlib.h>
#include <stdio.h>

#define CHUNK_SIZE 1024


struct ZCompressionMechanismPrivate{
    ZObject parent_object;
    z_stream compress_stream;
    z_stream decompress_stream;
	char *output;
};



static void z_compressions_mechanism_zlib_free(void *data) {
    ZCompressionMechanismPrivate *mechanism = (ZCompressionMechanismPrivate*)data;

    deflateEnd(&mechanism->compress_stream);
    inflateEnd(&mechanism->decompress_stream);

    if(mechanism->output) {
        z_free(mechanism->output);
    }
}


static ZCompressionMechanismPrivate *z_compression_mechanism_zlib_create(void) {
    ZCompressionMechanismPrivate *mechanism;
    int err;


    mechanism = Z_OBJECT_NEW(ZCompressionMechanismPrivate);
    if(!mechanism) {
        return NULL;
    }
    z_object_add_free_callback((ZObject*)mechanism,z_compressions_mechanism_zlib_free);

    mechanism->compress_stream.zalloc = Z_NULL;
    mechanism->compress_stream.zfree = Z_NULL;
    mechanism->compress_stream.opaque = Z_NULL;
    err = deflateInit(&mechanism->compress_stream, Z_BEST_COMPRESSION);
    if (err != Z_OK) {
        z_object_unref((ZObject*)mechanism);
        return NULL;
    }

    mechanism->decompress_stream.zalloc = Z_NULL;
    mechanism->decompress_stream.zfree = Z_NULL;
    mechanism->decompress_stream.opaque = Z_NULL;
    err = inflateInit(&mechanism->decompress_stream);
    if (err != Z_OK) {
        z_object_unref((ZObject*)mechanism);
        return NULL;
    }

    return mechanism;
}

static void z_compression_mechanism_zlib_destroy(ZCompressionMechanismPrivate *mechanism) {
    z_object_unref((ZObject*)mechanism);
}

static int z_compression_mechanism_zlib_compress(ZCompressionMechanismPrivate *compression_mechanism,void *source,size_t source_len,void **dest,size_t *dest_len,size_t *left) {
    int err;
	size_t output_position = 0;

    compression_mechanism->compress_stream.next_in = (Bytef*)source;
    compression_mechanism->compress_stream.avail_in = source_len;


    do {
        compression_mechanism->output = z_realloc(compression_mechanism->output,output_position + CHUNK_SIZE);
        compression_mechanism->compress_stream.avail_out = CHUNK_SIZE;
        compression_mechanism->compress_stream.next_out = (Bytef*)(compression_mechanism->output + output_position);

        err = deflate(&compression_mechanism->compress_stream,Z_NO_FLUSH);
        if (err == Z_STREAM_ERROR) {
			return 1;
		}
        output_position += CHUNK_SIZE;
    }while(compression_mechanism->compress_stream.avail_out == 0);

	output_position -= compression_mechanism->compress_stream.avail_out;
    do {
        compression_mechanism->output = z_realloc(compression_mechanism->output,output_position + CHUNK_SIZE);
        compression_mechanism->compress_stream.avail_out = CHUNK_SIZE;
        compression_mechanism->compress_stream.next_out = (Bytef*)(compression_mechanism->output + output_position);

        err = deflate(&compression_mechanism->compress_stream,Z_SYNC_FLUSH);
        if (err == Z_STREAM_ERROR) {
            return 1;
        }
        output_position += CHUNK_SIZE;
    }while (compression_mechanism->compress_stream.avail_out == 0);
    output_position -= compression_mechanism->compress_stream.avail_out;

    compression_mechanism->output = z_realloc(compression_mechanism->output,output_position);

    if(dest_len) {
        *dest_len = output_position;
    }
    if(dest) {
        *dest = compression_mechanism->output;
    }

    if(left) {
        *left = compression_mechanism->compress_stream.avail_in;
    }

    return 0;
}

static int z_compression_mechanism_zlib_decompress(ZCompressionMechanismPrivate *compression_mechanism,void *source,size_t source_len,void **dest,size_t *dest_len,size_t *left) {
    int err;
	size_t output_position = 0;

    compression_mechanism->decompress_stream.next_in = source;
    compression_mechanism->decompress_stream.avail_in = source_len;

	do {
		compression_mechanism->output = z_realloc(compression_mechanism->output,output_position + CHUNK_SIZE);
		compression_mechanism->decompress_stream.avail_out = CHUNK_SIZE;
		compression_mechanism->decompress_stream.next_out = (Bytef*) (compression_mechanism->output + output_position);
		err = inflate(&compression_mechanism->decompress_stream,Z_NO_FLUSH);
		if (err == Z_STREAM_ERROR) {
			return 1;
		}
		output_position += CHUNK_SIZE;
	}while (compression_mechanism->decompress_stream.avail_out == 0);

	output_position -= compression_mechanism->decompress_stream.avail_out;
    do {
        compression_mechanism->output = z_realloc(compression_mechanism->output,output_position + CHUNK_SIZE);
        compression_mechanism->decompress_stream.avail_out = CHUNK_SIZE;
        compression_mechanism->decompress_stream.next_out = (Bytef*) (compression_mechanism->output + output_position);
        err = inflate(&compression_mechanism->decompress_stream,Z_SYNC_FLUSH);
        if (err == Z_STREAM_ERROR) {
            return 1;
        }
        output_position += CHUNK_SIZE;
    }while (compression_mechanism->decompress_stream.avail_out == 0);
    output_position -= compression_mechanism->decompress_stream.avail_out;

    compression_mechanism->output = z_realloc(compression_mechanism->output,output_position);

    if(dest_len) {
        *dest_len = output_position;
    }
    if(dest) {
        *dest = compression_mechanism->output;
    }
    if(left) {
        *left = compression_mechanism->decompress_stream.avail_in;
    }


    return 0;
}

struct ZCompressionMechanismType z_compressions_zlib = {
    "zlib",
    1,
    {
        z_compression_mechanism_zlib_create,
        z_compression_mechanism_zlib_destroy,
        z_compression_mechanism_zlib_compress,
        z_compression_mechanism_zlib_decompress,
    }
};
#endif
