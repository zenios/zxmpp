/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "zjid.h"
#include "zstdlib_c.h"
#include "zstring.h"
#include "zobject_c.h"
#include <stdio.h>

#include <string.h>

struct ZJid {
    ZObject object;
    char *full;
    char *username;
    char *node;
    char *resource;
};



static void z_jid_free(void *data) {
    ZJid *jid = (ZJid*)data;

    if(jid->username) {
        z_free(jid->username);
    }

    if(jid->node) {
        z_free(jid->node);
    }

    if(jid->resource) {
        z_free(jid->resource);
    }

    if(jid->full) {
        z_free(jid->full);
    }

}


ZJid *z_jid_new(const char *jid) {
    ZJid *ret_jid;
    char *tmp;
    char *tmp1;
    const char *src;

    if(!jid) {
        return NULL;
    }

    ret_jid = Z_OBJECT_NEW(ZJid);
    if(!ret_jid) {
        return NULL;
    }

    z_object_add_free_callback((ZObject*)ret_jid,z_jid_free);

    src = jid;

    ret_jid->full = z_strdup(src);
    if(!ret_jid->full || !z_strlen(ret_jid->full)) {
        z_object_unref((ZObject*)ret_jid);
        return NULL;
    }

    tmp1 = strrchr(src,'/');
    if(tmp1) {
        ret_jid->resource = z_strdup(tmp1+1);
        if(!ret_jid->resource || !z_strlen(ret_jid->resource)) {
            z_object_unref((ZObject*)ret_jid);
            return NULL;
        }
    }

    tmp = strrchr(src,'@');
    if(tmp) {
        if(tmp - src == 0) {
            z_object_unref((ZObject*)ret_jid);
            return NULL;
        }
        ret_jid->username = z_malloc((tmp-src) + 1);
        if(!ret_jid->username ) {
            z_object_unref((ZObject*)ret_jid);
            return NULL;
        }
        z_memcpy(ret_jid->username,src,tmp-src);
        ret_jid->username[tmp-src] = '\0';
        src = ++tmp;
    }else{
        z_object_unref((ZObject*)ret_jid);
        return NULL;
    }
    if(tmp1) {
        if(tmp1-src == 0) {
            z_object_unref((ZObject*)ret_jid);
            return NULL;
        }
        ret_jid->node = z_malloc((tmp1-src) + 1);
        if(!ret_jid->node) {
            z_object_unref((ZObject*)ret_jid);
            return NULL;
        }
        z_memcpy(ret_jid->node,src,tmp1-src);
        ret_jid->node[tmp1-src] = '\0';


    }else{
        ret_jid->node = z_strdup(src);
    }

	if(!ret_jid->node || z_strlen(ret_jid->node) == 0) {
        z_object_unref((ZObject*)ret_jid);
        return NULL;
	}

	return ret_jid;
}


const char *z_jid_get(const ZJid *jid, int part) {
    if(!jid) {
        return NULL;
    }


    if(part & Z_JID_USERNAME) {
        return jid->username;
    }

    if(part & Z_JID_NODE) {
        return jid->node;
    }

    if(part & Z_JID_RESOURCE) {
        return jid->resource;
    }

    if(part & Z_JID_FULL) {
        return jid->full;
    }

    return NULL;
}

int z_jid_cmp (const ZJid *a,const ZJid *b, int parts) {
	int diff = 0;

	if (!a || !b) {
        return (Z_JID_RESOURCE | Z_JID_USERNAME | Z_JID_NODE);
	}

	if (parts & Z_JID_RESOURCE && !(!a->resource && !b->resource) && z_strcmp (a->resource, b->resource) != 0) {
		diff += Z_JID_RESOURCE;
	}
	if (parts & Z_JID_USERNAME && !(!a->username && !b->username) && z_strcasecmp (a->username, b->username) != 0) {
		diff += Z_JID_USERNAME;
	}
	if (parts & Z_JID_NODE && !(!a->node && !b->node) && z_strcmp (a->node, b->node) != 0) {
        diff += Z_JID_NODE;
	}
	return diff;

}
