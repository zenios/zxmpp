/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdint.h>
#include "zhashtable_c.h"
#include "zstdlib_c.h"
#include "zstring.h"
#include "zobject_c.h"

#define foreach_entry(c, e) for(c=e; c; c=c->next)

struct ZHashEntry {
    void *value;
    struct ZHashEntry *next;
    ZHashTableFreeFunc entry_free;
    char *key;
};

struct ZHashTableIter {
    ZObject object;
    ZHashTable *table;
    size_t entries_passed;
    unsigned int curr_bucket;
    struct ZHashEntry *prev;
    struct ZHashEntry *curr;
};


struct ZHashTable {
    ZObject object;
    unsigned int num_buckets;
    size_t num_entries;
    struct ZHashEntry **entries;
    ZHashTableFreeFunc value_free;
};

static uint32_t nearest_pow(uint32_t num) {
    uint32_t n = num > 0 ? num - 1 : 0;

    n |= n >> 1;
    n |= n >> 2;
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;
    n++;

    return n;
}

static inline unsigned int my_hash(const char *str) {
    unsigned int hash = 0;
    int c;

    while ((c = *str++) != 0)
        hash = c + (hash << 6) + (hash << 16) - hash;

    return hash;
}

static inline unsigned int index_of(unsigned int tablelength, unsigned int hashvalue) {
    return (hashvalue & (tablelength - 1u));
}

static struct ZHashEntry *entry_new(const char *key, void *val) {
    struct ZHashEntry *entry;

    entry = z_malloc(sizeof(struct ZHashEntry));

    if (!entry) {
        return NULL;
    }

    entry->value = val;
    entry->next = NULL;
    entry->key = z_strdup(key);
    if(!entry->key) {
        z_free(entry);
        return NULL;
    }
    return entry;
}

static void z_hashtable_iter_free(void *data) {
    ZHashTableIter *iter = (ZHashTableIter*)data;

    z_object_unref((ZObject*)iter->table);
}

static void z_hashtable_free(void *data) {
    ZHashTable *table = (ZHashTable*)data;
    unsigned int bucket;
    struct ZHashEntry *entry;
    struct ZHashEntry *next;

    if (!table) {
        return;
    }

    for (bucket = 0; bucket < table->num_buckets; bucket++) {
        for (entry = table->entries[bucket]; entry;) {
            next = entry->next;

            if (entry->value) {
                if(entry->entry_free) {
                    entry->entry_free(entry->value);
                } else if (table->value_free) {
                    table->value_free(entry->value);
                }
            }
            z_free(entry->key);
            z_free(entry);

            entry = next;
        }
    }

    z_free(table->entries);
}

static void hashtable_expand(ZHashTable *table)
{
    unsigned int next_size;
    struct ZHashEntry **entries;
    unsigned int bucket;
    struct ZHashEntry *curr;
    struct ZHashEntry *prev;
    struct ZHashEntry *next_curr;
    unsigned int new_bucket;

    next_size = sizeof(struct ZHashEntry*) * (table->num_buckets * 2);

    entries = z_realloc(table->entries, next_size);
    if (!entries) {
        return;
    }

    z_memset(entries + table->num_buckets, 0, table->num_buckets * sizeof(struct ZHashEntry*));

    table->entries = entries;
    table->num_buckets = table->num_buckets * 2;

    /* Reposition all entries */
    for (bucket = 0; bucket < table->num_buckets; bucket++) {
        if (table->entries[bucket]) {
            prev = NULL;
            for (curr = table->entries[bucket]; curr;) {
                new_bucket = index_of(table->num_buckets, my_hash(curr->key));
                next_curr = curr->next;

                /* Only move the entry if we need to */
                if (new_bucket != bucket) {

                    /* Update current bucket */
                    if (prev == NULL) {
                        table->entries[bucket] = curr->next;
                    } else {
                        prev->next = curr->next;
                    }

                    /* Move to the head of the new bucket */
                    curr->next = table->entries[new_bucket];
                    table->entries[new_bucket] = curr;

                } else {
                    prev = curr;
                }

                curr = next_curr;
            }
        }
    }
}

size_t z_hashtable_get_size(ZHashTable *table) {
    return table->num_entries;
}

void *z_hashtable_get(ZHashTable *table, const char *key) {
    struct ZHashEntry *entry;
    unsigned int entry_index;

    if (!table || !key) {
        return NULL;
    }

    entry_index = index_of(table->num_buckets, my_hash(key));

    foreach_entry(entry, table->entries[entry_index]) {
        if (!z_strcmp(key, entry->key)) {
            return entry->value;
        }
    }

    return NULL;
}

void z_hashtable_remove(ZHashTable *table, const char *key) {
    unsigned int hash_value;
    unsigned int bucket_index;
    struct ZHashEntry *entry;
    struct ZHashEntry *prev;

    if (!table || !key) {
        return;
    }

    hash_value = my_hash(key);
    bucket_index = index_of(table->num_buckets, hash_value);

    prev = NULL;

    for (entry = table->entries[bucket_index]; entry; entry = entry->next) {

        if (!z_strcmp(entry->key, key)) {
            if (entry->value) {
                if(entry->entry_free) {
                    entry->entry_free(entry->value);
                } else if (table->value_free) {
                    table->value_free(entry->value);
                }
            }

            if (prev) {
                prev->next = entry->next;
            } else {
                table->entries[bucket_index] = entry->next;
            }
            z_free(entry->key);
            z_free(entry);
            table->num_entries--;
            break;
        }

        prev = entry;

    }
}

int z_hashtable_add(ZHashTable *table,const char *key, void *val, ZHashTableFreeFunc entry_free_func) {
    unsigned int hash_value;
    unsigned int bucket_index;
    struct ZHashEntry *entry;
    struct ZHashEntry *prev;

    if (!table || !key) {
        return -1;
    }

    if (!val) {
        z_hashtable_remove(table, key);
        return 0;
    }

    hash_value = my_hash(key);
    bucket_index = index_of(table->num_buckets, hash_value);

    prev = NULL;

    for (entry = table->entries[bucket_index]; entry; entry = entry->next) {
        if (!z_strcmp(entry->key, key)) {
            if (entry->value) {
                if(entry->entry_free) {
                    entry->entry_free(entry->value);
                } else if (table->value_free) {
                    table->value_free(entry->value);
                }
            }
            entry->value = val;
            entry->entry_free = entry_free_func;
            break;
        }

        prev = entry;
    }

    /* Key not found: New entry */
    if (!entry) {
        entry = entry_new(key, val);
        if (entry) {
            entry->entry_free = entry_free_func;
            if (prev) {
                prev->next = entry;
            } else {
                table->entries[bucket_index] = entry;
            }

            table->num_entries++;

            /* Expand hash table on load factor 0.75 */
            if (table->num_entries > (table->num_buckets * 3) / 4) {
                hashtable_expand(table);
            }
        } else {
            return -1;
        }
    }

    return 0;
}

ZHashTable *z_hashtable_new(unsigned int num_elements, ZHashTableFreeFunc value_free_func) {
    unsigned int initial_size;
    ZHashTable *table;

    initial_size = nearest_pow((uint32_t)num_elements);

    /* Enforce minimum size */
    if (initial_size < 16) {
        initial_size = 16;
    }

    table = Z_OBJECT_NEW(ZHashTable);
    if (!table) {
        return NULL;
    }
    z_object_add_free_callback((ZObject*)table,z_hashtable_free);

    table->num_entries = 0;
    table->num_buckets = initial_size;
    table->value_free = value_free_func;

    table->entries = (struct ZHashEntry**)z_malloc(sizeof(struct ZHashEntry*) * initial_size);

    if (!table->entries) {
        z_object_unref((ZObject*)table);
        return NULL;
    }

    z_memset(table->entries, 0, sizeof(struct ZHashEntry*) * initial_size);

    return table;
}


ZHashTableIter *z_hashtable_iter_new(ZHashTable *table) {
    ZHashTableIter *iter;

    if (!table) {
        return NULL;
    }

    iter = Z_OBJECT_NEW(ZHashTableIter);
    if (!iter) {
        return NULL;
    }

    z_object_add_free_callback((ZObject*)iter,z_hashtable_iter_free);
    z_object_ref((ZObject*)table);
    iter->table = table;
    iter->entries_passed = 0;
    iter->curr_bucket = 0;
    iter->curr = NULL;
    iter->prev = NULL;

    return iter;
}

void z_hashtable_iter_reset(ZHashTableIter *iter) {
    if(!iter) {
        return;
    }
    iter->entries_passed = 0;
    iter->curr_bucket = 0;
    iter->curr = NULL;
    iter->prev = NULL;
}

int z_hashtable_iter_has_next(ZHashTableIter *iter) {
    if (!iter) {
        return 0;
    }

    return iter->entries_passed < iter->table->num_entries;
}

int z_hashtable_iter_next(ZHashTableIter *iter) {
    struct ZHashTable *table;

    if (!iter || iter->entries_passed >= iter->table->num_entries) {
        return 0;
    }

    table = iter->table;

    /* Special case for 0 bucket */
    if (iter->curr_bucket == 0 && iter->curr == NULL) {
        if (table->entries[0]) {
            iter->curr = table->entries[0];
            iter->entries_passed++;
            return 1;
        }
    }

    /* If we have more entries in the current bucket */
    if (iter->curr && iter->curr->next) {
        iter->prev = iter->curr;
        iter->curr = iter->curr->next;
        iter->entries_passed++;
        return 1;
    }

    iter->prev = NULL;
    iter->curr = NULL;

    /* Move to next bucket */
    if (iter->curr_bucket < table->num_buckets) {
        iter->curr_bucket++;
    }

    for (; iter->curr_bucket < table->num_buckets; iter->curr_bucket++) {
        if (table->entries[iter->curr_bucket]) {
            iter->curr = table->entries[iter->curr_bucket];
            iter->entries_passed++;
            return 1;
        }
    }

    return 0;
}

void z_hashtable_iter_remove(ZHashTableIter *iter) {
    struct ZHashTable *table;
    struct ZHashEntry *next;

    if (!iter || !iter->curr) {
        return;
    }

    table = iter->table;
    next = iter->curr->next;

    if (iter->prev) {
        iter->prev->next = next;
    } else {
        table->entries[iter->curr_bucket] = next;
    }

    /* Remove the entry */
    iter->table->num_entries--;
    iter->entries_passed--;

    if (iter->curr->value) {
        if(iter->curr->entry_free) {
            iter->curr->entry_free(iter->curr->value);
        } else if (iter->table->value_free) {
            iter->table->value_free(iter->curr->value);
        }
    }
    z_free(iter->curr);
    iter->curr = NULL;

    /* Move current entry to the next one */
    if (next) {
        iter->curr = next;
        iter->entries_passed++;
    } else {
        /* Move to next bucket */
        iter->prev = NULL;

        /* Move to next bucket */
        if (iter->curr_bucket < table->num_buckets) {
            iter->curr_bucket++;
        }

        for (; iter->curr_bucket < table->num_buckets; iter->curr_bucket++) {
            if (table->entries[iter->curr_bucket]) {
                iter->curr = table->entries[iter->curr_bucket];
                iter->entries_passed++;
                break;
            }
        }
    }

}

const char *z_hashtable_iter_get_key(ZHashTableIter *iter) {
    if (!iter || !iter->curr) {
        return NULL;
    }

    return iter->curr->key;
}

void *z_hashtable_iter_get_value(ZHashTableIter *iter) {
    if (!iter || !iter->curr) {
        return NULL;
    }

    return iter->curr->value;
}

