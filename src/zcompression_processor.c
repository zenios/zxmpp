/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "zcompression_processor.h"
#include "zcompression_c.h"
#include "zobject_c.h"
#include "zstring.h"
#include "zstdlib.h"
#include "zlist.h"
#include "zconstants.h"
#include "zhelper.h"
#include <stdio.h>


typedef struct ZCompressionProcessorMechanism {
    ZObject object;
    char *name;
    struct ZCompressionProcessorMechanism *next;
}ZCompressionProcessorMechanism;

struct ZCompressionProcessor {
    ZObject object;
    ZCompressionProcessorMechanism *mechanisms;
    ZCompressionProcessorMechanism *curr;
    ZCompressionProcessorCallback callback;
    void *data;
};


int z_compression_mechanism_handle_response(ZXMPP_UNUSED ZStanzaFilter *stanza_filter,ZStanza *stanza,void *data) {
    ZCompressionProcessor *processor = (ZCompressionProcessor*)data;
    const char *name = z_stanza_get_name(stanza);
    if(z_strcmp(name,"compressed") == 0) {
        if(processor->callback(processor,Z_COMPRESSION_PROCESSOR_SUCCESS,processor->data)){
            return -1;
        }
    }else{
        if(processor->callback(processor,Z_COMPRESSION_PROCESSOR_FAILURE,processor->data)) {
            return -1;
        }

    }
    return 0;
}

static void z_compressions_mechanism_free(void *data) {
    ZCompressionProcessorMechanism *mechanism = (ZCompressionProcessorMechanism*)data;

    z_free(mechanism->name);
}

static void z_compression_processor_mechanism_item_free(ZCompressionProcessorMechanism *mechanism) {
    z_object_unref((ZObject*)mechanism);
}

static void z_compression_processor_free(void *data) {
    ZCompressionProcessor *processor = (ZCompressionProcessor*)data;
    ZCompressionProcessorMechanism *curr = NULL;

    z_list_free_func(curr,processor->mechanisms,z_compression_processor_mechanism_item_free);
}

int z_compression_processor_proceed(ZCompressionProcessor *processor,ZConnection *connection) {
    ZStanza *stanza;
    ZStanzaFilter *filter;
    ZStanzaFilterRule *rule;
    if(!processor || !connection) {
        return 1;
    }


    if(!processor->curr) {
        processor->curr = processor->mechanisms;
    }else{
        processor->curr = processor->curr->next;
    }

    if(!processor->curr) {
        return 1;
    }
    filter = z_connection_get_filter(connection);
    rule = z_stanza_filter_add_rule(filter,z_compression_mechanism_handle_response,processor,Z_FILTER_RULE_NS,ZXMPP_NS_COMPRESSION,Z_FILTER_RULE_DONE);
    if(!rule) {
        return 1;
    }
    z_stanza_filter_rule_set_single(rule);

    stanza = z_helper_create_compression_stanza(processor->curr->name);
    if(!stanza) {
        return 1;
    }

    if(z_connection_send_stanza(connection,stanza)) {
        z_object_unref((ZObject*)stanza);
        return 1;
    }

    z_object_unref((ZObject*)stanza);


    return 0;
}


int z_compression_processor_add_mechanism(ZCompressionProcessor *processor,const char *name) {
    ZCompressionProcessorMechanism *mechanism_item;
    if(!processor || !name) {
        return 1;
    }

    if(!z_compression_mechanism_is_supported(name)) {
        return 1;
    }
    mechanism_item = Z_OBJECT_NEW(ZCompressionProcessorMechanism);
    if(!mechanism_item) {
        return 1;
    }
    z_object_add_free_callback((ZObject*)mechanism_item,z_compressions_mechanism_free);
    mechanism_item->name = z_strdup(name);

    if(!processor->mechanisms) {
        processor->mechanisms = mechanism_item;
    }else{
        ZCompressionProcessorMechanism *curr;
        curr = processor->mechanisms;
        while (curr->next) curr = curr->next;
        curr->next = mechanism_item;
    }
    return 0;
}

ZCompressionProcessor *z_compression_processor_new(ZCompressionProcessorCallback callback,void *data) {
    ZCompressionProcessor *processor;

    if(!callback) {
        return NULL;
    }

    processor = Z_OBJECT_NEW(ZCompressionProcessor);
    if(!processor) {
        return NULL;
    }
    z_object_add_free_callback((ZObject*)processor,z_compression_processor_free);

    processor->callback = callback;
    processor->data = data;

    return processor;
}

const char *z_compression_processor_get_active_compression(ZCompressionProcessor *processor) {
    return processor->curr->name;
}
