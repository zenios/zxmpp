/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "zstanza_filter.h"
#include "zstanza_filter_c.h"
#include "zobject_c.h"
#include "zstring.h"
#include "zstdlib.h"
#include "zlist.h"

#if defined(HAVE_PCRE)
# include <pcre.h>
#endif



struct ZStanzaFilterRule {
    ZObject object;
    struct ZStanzaFilterRule *next;
    struct ZStanzaFilterRule *prev;
    int score;
    int rules;
    int single;
    int removed;

    char *id;
    char *ns;
    char *from;
    char *name;
    char *type;
#if defined(HAVE_PCRE)
    pcre *id_re;
    pcre *ns_re;
    pcre *from_re;
    pcre *name_re;
    pcre *type_re;
#endif

    ZStanzaFilterCallback callback;
    void *data;
};

struct ZStanzaFilter {
    ZObject object;

    ZStanzaFilterRule *rules;
    ZStanzaFilterRule *tmp_rules;
};

static void z_stanza_filter_rule_free(void *data) {
    ZStanzaFilterRule *filter_rule = (ZStanzaFilterRule*)data;
    if(filter_rule->id) {
        z_free(filter_rule->id);
    }
    if(filter_rule->ns) {
        z_free(filter_rule->ns);
    }
    if(filter_rule->from) {
        z_free(filter_rule->from);
    }
    if(filter_rule->type) {
        z_free(filter_rule->type);
    }
    if(filter_rule->name) {
        z_free(filter_rule->name);
    }
#if defined(HAVE_PCRE)
    if(filter_rule->id_re) {
        pcre_free(filter_rule->id_re);
    }
    if(filter_rule->ns_re) {
        pcre_free(filter_rule->ns_re);
    }
    if(filter_rule->from_re) {
        pcre_free(filter_rule->from_re);
    }
    if(filter_rule->name_re) {
        pcre_free(filter_rule->name_re);
    }
    if(filter_rule->type_re) {
        pcre_free(filter_rule->type_re);
    }
#endif
}


static void z_filter_rule_free(ZStanzaFilterRule *filter_rule) {
    z_object_unref((ZObject*)filter_rule);
}

static void z_stanza_filter_free(void *data) {
    ZStanzaFilter *filter = (ZStanzaFilter*)data;
    ZStanzaFilterRule *curr;

    z_list_free_func(curr,filter->tmp_rules,z_filter_rule_free);
    z_list_free_func(curr,filter->rules,z_filter_rule_free);
}

#if defined(HAVE_PCRE)
static pcre *to_regex(const char *value) {
    const char *error;
    int error_offset;
	const char *regex_value;
	char *tmp;
	pcre *regex_filter;

	tmp = strstr(value,Z_STANZA_FILTER_REGEX_PREFIX);
    if(!tmp || tmp != value) {
        return NULL;
    }

    regex_value = value;
    regex_value += strlen(Z_STANZA_FILTER_REGEX_PREFIX);

    regex_filter = pcre_compile(regex_value,0,&error,&error_offset,NULL);
    if(!regex_filter || error || error_offset) {
        return NULL;
    }

    return regex_filter;
}
#endif


ZStanzaFilter *z_stanza_filter_new() {
    ZStanzaFilter *filter = Z_OBJECT_NEW(ZStanzaFilter);
    if(!filter) {
        return NULL;
    }

    z_object_add_free_callback((ZObject*)filter,z_stanza_filter_free);
    return filter;
}

static void process_tmp_rules(ZStanzaFilter *filter) {
    ZStanzaFilterRule *rule;

    while(filter->tmp_rules) {
        rule = filter->tmp_rules;
        filter->tmp_rules = filter->tmp_rules->next;
        rule->next = NULL;
        rule->prev = NULL;
        if (!filter->rules) {
            filter->rules = rule;
        }else{
            ZStanzaFilterRule *curr;
            curr = filter->rules;
            while (curr->next) curr = curr->next;
            curr->next = rule;
            rule->prev = curr;
        }
    }
    filter->tmp_rules = NULL;
}

static void process_removed_rules(ZStanzaFilter *filter) {
    ZStanzaFilterRule *curr, *curr_list;


    curr_list = filter->rules;
    while(curr_list) {
        curr = curr_list;
        curr_list = curr_list->next;
        if(curr->removed) {
            if(curr == filter->rules) {
                filter->rules = filter->rules->next;
            }else{
                if(curr->prev) {
                    curr->prev->next = curr->next;
                }

                if(curr->next) {
                    curr->next->prev = curr->prev;
                }
            }
            z_filter_rule_free(curr);
        }
    }
}


int z_stanza_filter_process_stanza(ZStanzaFilter *filter,ZStanza *stanza) {
    ZStanzaFilterRule compare_rule;
    ZStanzaFilterRule *rule,*max_rule;
    int fail, score, max_score, is_regex;
    if(!filter || !stanza) {
        return 1;
    }
    process_tmp_rules(filter);
    process_removed_rules(filter);
    if(!filter->rules) {
        return 0;
    }
    compare_rule.from = (char*)z_stanza_get_attribute(stanza,"from");
    compare_rule.id   = (char*)z_stanza_get_attribute(stanza,"id");
    compare_rule.ns   = (char*)z_stanza_get_attribute(stanza,"xmlns");
    /*Name space is the only attribute that if not found we are using the first we
      find from its childrens*/
    if(!compare_rule.ns) {
        ZStanza *child;
        for (child = z_stanza_get_children(stanza); child; child = z_stanza_get_next(child)) {
            compare_rule.ns   = (char*)z_stanza_get_attribute(child,"xmlns");
            if(compare_rule.ns) {
                break;
            }
        }
    }
    compare_rule.type = (char*)z_stanza_get_attribute(stanza,"type");
    compare_rule.name = (char*)z_stanza_get_name(stanza);

	rule = filter->rules;
	max_rule = NULL;
	max_score = 0;
	while (rule) {
		score = 0;
		fail = 0;
		is_regex = 0;

		if (rule->rules & Z_FILTER_RULE_TYPE) {
#if defined(HAVE_PCRE)
            if(rule->type_re) {
                is_regex = 1;
                if(pcre_exec(rule->type_re,NULL,compare_rule.type,z_strlen(compare_rule.type),0,0,NULL,0) >= 0) {
                    score += 1;
                }else{
                    fail = 1;
                }
            }
#endif
            if(!is_regex) {
                if (z_strcmp (rule->type, compare_rule.type) == 0) {
                    score += 1;
                }else{
                    fail = 1;
                }
            }
		}
		if (rule->rules & Z_FILTER_RULE_NS) {
#if defined(HAVE_PCRE)
            if(rule->ns_re) {
                is_regex = 1;
                if(pcre_exec(rule->ns_re,NULL,compare_rule.ns,z_strlen(compare_rule.ns),0,0,NULL,0) >= 0) {
                    score += 1;
                }else{
                    fail = 1;
                }
            }
#endif
            if(!is_regex) {
                if (z_strcmp (rule->ns, compare_rule.ns) == 0) {
                    score += 2;
                }else{
                    fail = 1;
                }
            }
		}
		if (rule->rules & Z_FILTER_RULE_FROM) {
#if defined(HAVE_PCRE)
            if(rule->from_re) {
                is_regex = 1;
                if(pcre_exec(rule->from_re,NULL,compare_rule.from,z_strlen(compare_rule.from),0,0,NULL,0) >= 0) {
                    score += 1;
                }else{
                    fail = 1;
                }
            }
#endif
            if(!is_regex) {
                if (z_strcmp (rule->from, compare_rule.from) == 0) {
                    score += 4;
                } else {
                    fail = 1;
                }
            }
		}
		if (rule->rules & Z_FILTER_RULE_ID) {
#if defined(HAVE_PCRE)
            if(rule->id_re) {
                is_regex = 1;
                if(pcre_exec(rule->id_re,NULL,compare_rule.id,z_strlen(compare_rule.id),0,0,NULL,0) >= 0) {
                    score += 1;
                }else{
                    fail = 1;
                }
            }
#endif
            if(!is_regex) {
                if (z_strcmp (rule->id, compare_rule.id) == 0) {
                    score += 1;
                }else{
                    fail = 1;
                }
            }
		}
		if (rule->rules & Z_FILTER_RULE_NAME) {
#if defined(HAVE_PCRE)
            if(rule->name_re) {
                is_regex = 1;
                if(pcre_exec(rule->name_re,NULL,compare_rule.name,z_strlen(compare_rule.name),0,0,NULL,0) >= 0) {
                    score += 1;
                }else{
                    fail = 1;
                }
            }
#endif
            if(!is_regex) {
                if (z_strcmp (rule->name, compare_rule.name) == 0) {
                    score += 16;
                }else{
                    fail = 1;
                }
            }
		}
		if (fail != 0) {
		    score = 0;
		}
		rule->score = score;
		if (score > max_score) {
			max_rule = rule;
			max_score = score;
		}
		rule = rule->next;
	}

	while (max_rule) {
	    int ret = max_rule->callback(filter,stanza,max_rule->data);
        /*Delete single rules*/
        if(max_rule->single) {
            if(max_rule == filter->rules) {
                filter->rules = filter->rules->next;
            }else{
                if(max_rule->prev) {
                    max_rule->prev->next = max_rule->next;
                }

                if(max_rule->next) {
                    max_rule->next->prev = max_rule->prev;
                }
            }
            z_filter_rule_free(max_rule);
            max_rule = NULL;
        }
        if(ret > 0) {
            return 0;
        }else if(ret < 0) {
            return 1;
        }else if(max_rule) {
            max_rule->score = 0;
            max_rule = NULL;
        }
		rule = filter->rules;
		while (rule) {
			if (rule->score > 0) {
				max_rule = rule;
			}
			rule = rule->next;
		}
	}

	return 0;
}

ZStanzaFilterRule *z_stanza_filter_add_rule(ZStanzaFilter *filter,ZStanzaFilterCallback callback,void *data, ...) {
    ZStanzaFilterRule *rule;
    ZStanzaFilterRule *curr;
	va_list ap;
	int type;
	char *value;
#if defined(HAVE_PCRE)
	int is_regex;
    pcre *regex_filter;
#endif

    if(!filter || !callback) {
        return NULL;
    }

    rule = Z_OBJECT_NEW(ZStanzaFilterRule);
    if(!rule) {
        return NULL;
    }
    z_object_add_free_callback((ZObject*)rule,z_stanza_filter_rule_free);
    rule->callback = callback;
    rule->data = data;

	va_start (ap, data);
	while (1) {
		type = va_arg (ap, int);
        value = va_arg (ap, char *);
		if (type == Z_FILTER_RULE_DONE) {
		    break;
		}
		rule->rules += type;
		if(!value) {
		    z_object_unref((ZObject*)rule);
            return NULL;
		}
#if defined(HAVE_PCRE)
        is_regex = 0;
        regex_filter = to_regex(value);
        if(regex_filter) {
            is_regex = 1;
        }
#endif
		switch (type) {
			case Z_FILTER_RULE_TYPE:
#if defined(HAVE_PCRE)
                if(is_regex) {
                    rule->type_re = regex_filter;
                    break;
                }
#endif
				rule->type = z_strdup(value);
				break;
			case Z_FILTER_RULE_ID:
#if defined(HAVE_PCRE)
                if(is_regex) {
                    rule->id_re = regex_filter;
                    break;
                }
#endif
				rule->id = z_strdup(value);
				break;
			case Z_FILTER_RULE_NS:
#if defined(HAVE_PCRE)
                if(is_regex) {
                    rule->ns_re = regex_filter;
                    break;
                }
#endif
				rule->ns = z_strdup(value);
				break;
			case Z_FILTER_RULE_FROM:
#if defined(HAVE_PCRE)
                if(is_regex) {
                    rule->from_re = regex_filter;
                    break;
                }
#endif
				rule->from = z_strdup(value);
				break;
            case Z_FILTER_RULE_NAME:
#if defined(HAVE_PCRE)
                if(is_regex) {
                    rule->name_re = regex_filter;
                    break;
                }
#endif
                rule->name = z_strdup(value);
                break;
		}
	}
	va_end(ap);


    /*Add the rule to a temp list that will be merged on the main list
     on the next iteration of the filter.This is done in order to be able
    to add rules while filter is processing the rules list*/
	if (!filter->tmp_rules) {
	    filter->tmp_rules = rule;
	}else{
        curr = filter->tmp_rules;
        while (curr->next) curr = curr->next;
        curr->next = rule;
        rule->prev = curr;
	}

    return rule;
}

int z_stanza_filter_remove_rule(ZStanzaFilterRule *rule) {
    if(!rule) {
        return 1;
    }

    /*Will be removed on the next iteration*/
    rule->removed = 1;
    return 0;
}

int z_stanza_filter_rule_set_single(ZStanzaFilterRule *rule) {
    if(!rule) {
        return 1;
    }

    rule->single = 1;
    return 0;
}
