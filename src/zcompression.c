/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "zcompression_c.h"
#include "zcompression_mechanism_type.h"
#include "zobject_c.h"
#include "zstdlib_c.h"
#include "zstring.h"
#include "zlist.h"
#include <stdio.h>

#if defined(HAVE_ZLIB_COMPRESSION)
#include "compression/zlib.h"
#endif

static const ZCompressionMechanismType *mechanism_type[] = {
#if defined(HAVE_ZLIB_COMPRESSION)
    &z_compressions_zlib,
#endif
    NULL
};

typedef struct ZCompressionMechanismItem {
    const ZCompressionMechanismType *mechanism;
    struct ZCompressionMechanismItem *next;
    struct ZCompressionMechanismItem *prev;
}ZCompressionMechanismItem;

struct ZCompressionMechanism {
    ZObject parent_object;
    const ZCompressionMechanismType *mechanism_type;
    ZCompressionMechanismPrivate *private_mechanism;
};

static ZCompressionMechanismItem *mechanisms = NULL;

int z_compression_init() {

    int i;
    for(i=0;mechanism_type[i];i++) {
        if(z_compression_register_mechanism(mechanism_type[i])) {
            return 1;
        }
    }

    return 0;
}


static void z_compression_mechanism_item_free(ZCompressionMechanismItem *mechanism) {
    z_free(mechanism);
}

void z_compression_destroy() {
    ZCompressionMechanismItem *curr;
    z_list_free_func(curr,mechanisms,z_compression_mechanism_item_free);
}

static void z_compression_mechanism_free(void *data) {
    ZCompressionMechanism *mechanism = (ZCompressionMechanism*)data;

    if(mechanism->mechanism_type->compression_mechanism_ops.destroy_callback) {
        mechanism->mechanism_type->compression_mechanism_ops.destroy_callback(mechanism->private_mechanism);
    }
}

int z_compression_register_mechanism(const ZCompressionMechanismType *mechanism) {
    ZCompressionMechanismItem *new_mechanism,*curr;
    if(!mechanism || !mechanism->compression_mechanism_ops.create_callback) {
        return 1;
    }


    /*Check if we allready have this type*/
    foreach_in_list(curr,mechanisms) {
        if(z_strcmp(curr->mechanism->compression_mechanism_name,mechanism->compression_mechanism_name) == 0) {
            curr->mechanism = mechanism;
            return 0;
        }
    }

    new_mechanism = z_malloc(sizeof(ZCompressionMechanismItem));
    if(!new_mechanism) {
        return 1;
    }
    z_memset(new_mechanism,0,sizeof(ZCompressionMechanismItem));
    new_mechanism->mechanism = mechanism;

    if(!mechanisms) {
        mechanisms = new_mechanism;
    }else{
        curr = mechanisms;
        while (curr->next) curr = curr->next;
        curr->next = new_mechanism;
        new_mechanism->prev = curr;
    }

    return 0;
}

int z_compression_unregister_mechanism(const ZCompressionMechanismType *mechanism) {
    ZCompressionMechanismItem *curr;
    int found = 0;
    if(!mechanism) {
        return 1;
    }

    foreach_in_list(curr,mechanisms) {
        if(curr->mechanism == mechanism) {
            found = 1;
            break;
        }
    }

    if(!found) {
        return 1;
    }

    if(mechanisms == curr) {
        mechanisms = mechanisms->next;
        z_compression_mechanism_item_free(curr);
        return 0;
    }

    if(curr->prev) {
        curr->prev->next = curr->next;
    }
    if(curr->next) {
        curr->next->prev = curr->prev;
    }
    z_compression_mechanism_item_free(curr);

    return 0;
}

ZCompressionMechanism *z_compression_mechanism_create(const char *name) {
    ZCompressionMechanismItem *curr;
    if(!name) {
        return NULL;
    }

    foreach_in_list(curr,mechanisms) {
        if(z_strcmp(curr->mechanism->compression_mechanism_name,name) == 0 && curr->mechanism->compression_mechanism_ops.create_callback) {
            ZCompressionMechanism *mechanism = Z_OBJECT_NEW(ZCompressionMechanism);
            if(!mechanism) {
                return NULL;
            }
            z_object_add_free_callback((ZObject*)mechanism,z_compression_mechanism_free);
            mechanism->mechanism_type = curr->mechanism;
            mechanism->private_mechanism = curr->mechanism->compression_mechanism_ops.create_callback();
            if(!mechanism->private_mechanism) {
                z_object_unref((ZObject*)mechanism);
                return NULL;
            }
            return mechanism;
        }
    }

    return NULL;
}

int z_compression_mechanism_get_priority(const char *name) {
    ZCompressionMechanismItem *curr;
    if(!name) {
        return -1;
    }

    foreach_in_list(curr,mechanisms) {
        if(z_strcmp(curr->mechanism->compression_mechanism_name,name) == 0) {
            return curr->mechanism->priority;
        }
    }

    return -1;
}

void z_compression_mechanism_destroy(ZCompressionMechanism *mechanism) {
    if(!mechanism) {
        return;
    }
    z_object_unref((ZObject*)mechanism);
}


int z_compression_mechanism_compress(ZCompressionMechanism *mechanism,void *source,size_t source_len,void **dest,size_t *dest_len,size_t *left) {
    if(!mechanism || !mechanism->mechanism_type->compression_mechanism_ops.compress_callback || !source || !dest) {
        return 1;
    }

    return mechanism->mechanism_type->compression_mechanism_ops.compress_callback(mechanism->private_mechanism,source,source_len,dest,dest_len,left);
}

int z_compression_mechanism_decompress(ZCompressionMechanism *mechanism,void *source,size_t source_len,void **dest,size_t *dest_len,size_t *left) {
    if(!mechanism || !mechanism->mechanism_type->compression_mechanism_ops.decompress_callback || !source || !dest) {
        return 1;
    }

    return mechanism->mechanism_type->compression_mechanism_ops.decompress_callback(mechanism->private_mechanism,source,source_len,dest,dest_len,left);
}

int z_compression_mechanism_is_supported(const char *name) {
    ZCompressionMechanismItem *curr;

    foreach_in_list(curr,mechanisms) {
        if(z_strcmp(curr->mechanism->compression_mechanism_name,name) == 0) {
            return 1;
        }
    }

    return 0;
}
