/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include <stdio.h>
#include "zstanza.h"
#include "zobject_c.h"
#include "zstdlib_c.h"
#include "zstring.h"
#include "zhashtable_c.h"
#include "zlist.h"


struct ZStanza {
    ZObject object;
    struct ZStanza *parent;
    struct ZStanza *children;
    struct ZStanza *next;
    struct ZStanza *prev;
    char *name;
    char *text;
    ZHashTable *attrs;
};

static void z_stanza_child_free(ZStanza *stanza) {
    z_object_unref((ZObject*)stanza);
}

static void z_stanza_free(void *data) {
    ZStanza *stanza = (ZStanza*)data;
    ZStanza *curr;

    /* release all children */
    z_list_free_func(curr,stanza->children,z_stanza_child_free);

    if(stanza->name) {
        z_free(stanza->name);
    }

    if(stanza->attrs) {
        z_object_unref((ZObject*)stanza->attrs);
    }
    if(stanza->text) {
        z_free(stanza->text);
    }
}

static inline void render_update(int *written, const int length,const int lastwrite,size_t *left, char **ptr)
{
    *written += lastwrite;

    if (*written > length) {
        *left = 0;
        *ptr = NULL;
    } else {
        *left -= lastwrite;
        *ptr = &(*ptr)[lastwrite];
    }
}

static int render_stanza_recursive(const ZStanza *stanza,char *buf,size_t buflen)
{
    size_t left;
    int ret, written;
    ZStanza *child;


    left = buflen;
    written = 0;

    if (!stanza->name) {
            return -1;
    }
    /* write begining of tag and attributes */
    ret = snprintf(buf, left, "<%s", stanza->name);
    if (ret < 0) {
        return -1;
    }
    render_update(&written, buflen, ret, &left, &buf);

    if (stanza->attrs && z_hashtable_get_size(stanza->attrs) > 0) {
        ZHashTableIter *iter = z_hashtable_iter_new(stanza->attrs);
        if(!iter) {
            return -1;
        }
        while (z_hashtable_iter_has_next(iter)) {
            z_hashtable_iter_next(iter);
            ret = snprintf(buf, left, " %s=\"%s\"", z_hashtable_iter_get_key(iter),(char*)z_hashtable_iter_get_value(iter));
            if (ret < 0) {
                return -1;
            }
            render_update(&written, buflen, ret, &left, &buf);
        }
        z_object_unref((ZObject*)iter);
    }

    if (!stanza->children && !stanza->text) {
        /* write end if singleton tag */
        ret = snprintf(buf, left, "/>");
        if (ret < 0) {
            return -1;
        }
        render_update(&written, buflen, ret, &left, &buf);
    } else {
        /* this stanza has child stanzas */

        /* write end of start tag */
        ret = snprintf(buf, left, ">");
        if (ret < 0) {
            return -1;
        }
        render_update(&written, buflen, ret, &left, &buf);

        if(stanza->text) {
            ret = snprintf(buf, left, "%s", stanza->text);
            if (ret < 0) {
                return -1;
            }
            render_update(&written, buflen, ret, &left, &buf);
        }
        if(stanza->children) {
            /* iterate and recurse over child stanzas */
            child = stanza->children;
            while (child) {
                ret = render_stanza_recursive(child, buf, left);
                if (ret < 0) {
                    return -1;
                }

                render_update(&written, buflen, ret, &left, &buf);

                child = child->next;
            }
        }

        /* write end tag */
        ret = snprintf(buf, left, "</%s>", stanza->name);
        if (ret < 0) {
            return -1;
        }

        render_update(&written, buflen, ret, &left, &buf);
    }

    return written;
}

ZStanza *z_stanza_new(const char *name) {
    ZStanza *stanza;
    if(!name) {
        return NULL;
    }
    stanza = Z_OBJECT_NEW(ZStanza);
    if(!stanza) {
        return NULL;
    }

    z_object_add_free_callback((ZObject*)stanza,z_stanza_free);

    stanza->name = z_strdup(name);
    if(!stanza->name) {
        z_object_unref((ZObject*)stanza);
    }



    return stanza;
}


int z_stanza_append_text(ZStanza *stanza,const char *text,size_t len) {
    int new_text_len = 0;
    int original_text_len = 0;
    if(!stanza || !text) {
        return 1;
    }
    if(!len) {
        new_text_len = z_strlen(text);
    }else{
        new_text_len = len;
    }

    if(stanza->text) {
        original_text_len = z_strlen(stanza->text);
    }
    stanza->text = z_realloc(stanza->text, new_text_len + original_text_len + 1);
    if(!stanza->text) {
        return 1;
    }
    if(z_memcpy(stanza->text + original_text_len,text,new_text_len) == NULL) {
        z_free(stanza->text);
        return 1;
    }
    stanza->text[new_text_len + original_text_len] = '\0';
    return 0;
}

const char *z_stanza_get_text(const ZStanza *stanza) {
    if(!stanza) {
        return NULL;
    }

    return stanza->text;
}

int z_stanza_add_attribute(ZStanza *stanza,const char *key,const char *value) {
    char *new_value;
    if(!stanza || !key || !value) {
        return 1;
    }

    if(!stanza->attrs) {
        stanza->attrs = z_hashtable_new(0,z_free);
        if(!stanza->attrs) {
            return 1;
        }
    }
    new_value = z_strdup(value);
    if(!new_value) {
        return 1;
    }
    z_hashtable_add(stanza->attrs,key,new_value,NULL);
    return 0;
}

ZHashTableIter *z_stanza_get_attributes(const ZStanza *stanza) {
    if(!stanza) {
        return NULL;
    }

    return z_hashtable_iter_new(stanza->attrs);
}

const char *z_stanza_get_attribute(const ZStanza *stanza,const char *name) {
    if(!stanza || !stanza->attrs) {
        return NULL;
    }

    return z_hashtable_get(stanza->attrs,name);
}

int z_stanza_add_child(ZStanza *parent,ZStanza *children) {
    ZStanza *curr;
    if(!parent || !children) {
        return 1;
    }

    z_object_ref((ZObject*)children);

    children->parent = parent;
    if (!parent->children) {
        parent->children = children;
    }else {
        curr = parent->children;
        while (curr->next) curr = curr->next;
        curr->next = children;
        children->prev = curr;
    }

    return 0;
}


char *z_stanza_to_string(const ZStanza *stanza)
{
    char *buffer, *tmp;
    size_t length;
    int ret;

    length = 1024;
    buffer = z_malloc(length);
    if (!buffer) {
        return NULL;
    }
    ret = render_stanza_recursive(stanza, buffer, length);
    if (ret < 0) {
        z_free(buffer);
        return NULL;
    }

    if (ret > (int)(length - 1)) {
        tmp = z_realloc(buffer, ret + 1);
        if (!tmp) {
            z_free(buffer);
            return NULL;
        }
        length = ret + 1;
        buffer = tmp;

        ret = render_stanza_recursive(stanza, buffer, length);
        if (ret > (int)(length - 1)) {
            z_free(buffer);
            return NULL;
        }
    }

    buffer[length - 1] = '\0';

    return buffer;
}

ZStanza *z_stanza_get_parent(const ZStanza *stanza) {
    if(!stanza) {
        return NULL;
    }

    return stanza->parent;
}

ZStanza *z_stanza_get_children(const ZStanza *stanza) {
    if(!stanza) {
        return NULL;
    }

    return stanza->children;
}

ZStanza *z_stanza_get_next(const ZStanza *stanza) {
    if(!stanza) {
        return NULL;
    }

    return stanza->next;
}

ZStanza *z_stanza_get_child_by(const ZStanza *stanza,int type,const char *value) {
    ZStanza *curr;
    if(!stanza || !value) {
        return NULL;
    }

    foreach_in_list(curr,stanza->children) {
        switch(type) {
            case Z_STANZA_CHILD_BY_NAME:
                if(z_strcmp(z_stanza_get_name(curr),value) == 0) {
                    return curr;
                }
                break;
            case Z_STANZA_CHILD_BY_NS:
            {
                const char *ns = z_stanza_get_attribute(curr,"xmlns");
                if(ns && (z_strcmp(ns,value) == 0)) {
                    return curr;
                }
            }
            break;
        }
    }
    return NULL;

}

const char *z_stanza_get_name(const ZStanza *stanza) {
    if(!stanza) {
        return NULL;
    }

    return stanza->name;
}

