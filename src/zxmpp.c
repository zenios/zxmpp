/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "zxmpp.h"
#include "zparser_c.h"
#include "zauth_c.h"
#include "ztls_c.h"
#include "zatomic.h"
#include <stdio.h>
#include <time.h>
#if !defined(HAVE_ARC4RANDOM)
#include <stdlib.h>
#endif

static int initialized;

int z_xmpp_init() {
    int ret;

    if(initialized) {
       return 0;
    }
#if !defined(HAVE_ARC4RANDOM)
    srand(time(0));
#endif
    z_atomic_init();
    ret = z_parsers_init();
    if(ret) {
        z_xmpp_destroy();
        return 1;
    }
    ret = z_tls_init();
    if(ret) {
        z_xmpp_destroy();
        return 1;
    }
    ret = z_auth_init();
    if(ret) {
        z_xmpp_destroy();
        return 1;
    }

    ret = z_compression_init();
    if(ret) {
        z_xmpp_destroy();
        return 1;
    }
    initialized++;
    return 0;
}


void z_xmpp_destroy() {

    initialized--;
    if(initialized) {
        return;
    }
    z_compression_destroy();
    z_auth_destroy();
    z_tls_destroy();
    z_parsers_destroy();
    z_atomic_destroy();
}
