/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#if !defined(HAVE_ARC4RANDOM)
# include <stdlib.h>
#endif
#include "zparser_c.h"
#include "zconnection_c.h"
#include "zstanza_filter_c.h"
#include "zstdlib_c.h"
#include "zstring.h"
#include "zconstants.h"
#include "zauth_processor_c.h"
#include "zcompression_processor.h"
#include "zcompression_c.h"
#include "zhelper.h"
#include "zlist.h"



static int z_connection_send_header(ZConnection *connection) {
    char *tmp  = z_helper_create_opening_stream(connection->jid,connection->is_component);
    if(!tmp) {
        return 1;
    }
    z_socket_send(connection->sock,tmp,0);
    z_free(tmp);
    return 0;
}

static void z_connection_send_status(ZConnection *connection, int status) {

    if(connection->status_callback) {
        connection->status_callback(connection,status,connection->status_callback_data);
    }
}


static void z_connection_free(void *data) {
    ZEvent event;
    ZConnection *connection = (ZConnection*)data;

    if(z_socket_get_status(connection->sock) == Z_SOCKET_CONNECTED) {
        z_connection_send_status(connection,Z_STATUS_SOCKET_CLOSED);
    }
    while(!z_event_queue_pop(connection->event_queue,&event)) {
        switch(event.type) {
            case Z_CONNECTION_READY:
            case Z_CONNECTION_START_BIND:
            case Z_CONNECTION_START_SESSION:
            {
                z_object_unref((ZObject*)event.data);
            }
            break;

        }
    }
    z_free(connection->stream_id);
    z_object_unref((ZObject*)connection->features_stanza);
    z_object_unref((ZObject*)connection->auth_processor);
    z_object_unref((ZObject*)connection->compression_processor);
    z_object_unref((ZObject*)connection->event_queue);
    z_object_unref((ZObject*)connection->stanza_filter);
    z_object_unref((ZObject*)connection->parser);
    z_object_unref((ZObject*)connection->stanza_parser);
    z_object_unref((ZObject*)connection->sock);
    z_object_unref((ZObject*)connection->jid);
    z_free(connection->password);
}

ZConnection *z_connection_new(ZJid *jid,const char *password,int component) {
    ZConnection *connection;
    ZStanzaFilterRule *rule;

    if(!jid || !password) {
        return NULL;
    }

    connection = Z_OBJECT_NEW(ZConnection);
    if(!connection) {
        return NULL;
    }
    z_object_add_free_callback((ZObject*)connection,z_connection_free);

#if defined(HAVE_ARC4RANDOM)
    connection->next_id = arc4random();
#else
    connection->next_id = rand();
#endif
    connection->is_component = component;
    connection->jid = jid;
    z_object_ref((ZObject*)connection->jid);

    connection->password = z_strdup(password);
    if(!connection->password) {
        z_object_unref((ZObject*)connection);
        return NULL;
    }

    connection->sock = z_socket_new(z_socket_state_callback,connection,z_socket_read_callback,connection);
    if(!connection->sock) {
        z_object_unref((ZObject*)connection);
        return NULL;
    }
    z_socket_set_host(connection->sock,z_jid_get(connection->jid,Z_JID_NODE));


    if(!connection->is_component) {
        z_socket_set_port(connection->sock,5222);
    }else{
        z_socket_set_port(connection->sock,5275);
    }


    connection->stanza_parser = z_stanza_parser_new(z_stanza_parser_callback,connection);
    if(!connection->stanza_parser) {
        z_object_unref((ZObject*)connection);
        return NULL;
    }

    connection->parser = z_parser_new(connection->stanza_parser);
    if(!connection->parser) {
        z_object_unref((ZObject*)connection);
        return NULL;
    }

    connection->stanza_filter = z_stanza_filter_new();
    if(!connection->stanza_filter) {
        z_object_unref((ZObject*)connection);
        return NULL;
    }

    rule = z_stanza_filter_add_rule(connection->stanza_filter,z_connection_handle_ping,connection,Z_FILTER_RULE_NS,ZXMPP_NS_PING,Z_FILTER_RULE_DONE);
    if(!rule) {
        z_object_unref((ZObject*)connection);
        return NULL;
    }
    rule = z_stanza_filter_add_rule(connection->stanza_filter,z_connection_handle_features,connection,Z_FILTER_RULE_NAME,"stream:features",Z_FILTER_RULE_DONE);
    if(!rule) {
        z_object_unref((ZObject*)connection);
        return NULL;
    }

    rule = z_stanza_filter_add_rule(connection->stanza_filter,z_connection_handle_tls,connection,Z_FILTER_RULE_NAME,"proceed",Z_FILTER_RULE_NS,ZXMPP_NS_TLS,Z_FILTER_RULE_DONE);
    if(!rule) {
        z_object_unref((ZObject*)connection);
        return NULL;

    }else{
        z_stanza_filter_rule_set_single(rule);
    }
    connection->event_queue = z_event_queue_new(0);
    if(!connection->event_queue) {
        z_object_unref((ZObject*)connection);
        return NULL;
    }


    return connection;
}


int z_connection_iterate(ZConnection *connection) {
    ZEvent event;
    if(!connection) {
        return 1;
    }
    while(!z_event_queue_pop(connection->event_queue,&event)) {
        switch(event.type) {
            case Z_CONNECTION_SOCKET_CONNECTED:
                z_connection_send_status(connection,Z_STATUS_SOCKET_OPENED);
                if(z_connection_send_header(connection)) {
                    z_connection_send_status(connection,Z_STATUS_UNKNOWN_ERROR);
                    return 1;
                }
            break;
            case Z_CONNECTION_START_TLS:
                {
                    z_connection_send_status(connection,Z_STATUS_START_TLS);
                }
            break;
            case Z_CONNECTION_START_TLS_NEGOTIATION:
                if(z_socket_initialize_tls(connection->sock)) {
                    z_connection_send_status(connection,Z_STATUS_TLS_FAILURE);
                    return 1;
                }
                z_parser_reset(connection->parser);
                if(z_connection_send_header(connection)) {
                    z_connection_send_status(connection,Z_STATUS_UNKNOWN_ERROR);
                    return 1;
                }
                z_connection_send_status(connection,Z_STATUS_TLS_SUCCESS);
            break;
            case Z_CONNECTION_AUTH_PROCESSOR_FAILURE:
                z_connection_send_status(connection,Z_STATUS_AUTHENTICATION_FAILURE);
            break;
            case Z_CONNECTION_STREAM_OPENED:
            case Z_CONNECTION_AUTH_PROCESSOR_PROCEED:
                {
                    if(event.type == Z_CONNECTION_STREAM_OPENED) {
                        z_connection_send_status(connection,Z_STATUS_STREAM_OPENED);
                    }
                    if(connection->auth_processor) {
                        z_connection_send_status(connection,Z_STATUS_AUTHENTICATING);
                        if(z_auth_processor_proceed(connection->auth_processor,connection)) {
                            z_object_unref((ZObject*)connection->auth_processor);
                            connection->auth_processor = NULL;
                            z_connection_send_status(connection,Z_STATUS_AUTHENTICATION_FAILURE);
                            if(z_connection_close(connection)) {
                                return 1;
                            }
                        }
                    }
                }
            break;
            case Z_CONNECTION_AUTH_PROCESSOR_AUTHENTICATED:
                {
                    z_object_unref((ZObject*)connection->auth_processor);
                    connection->auth_processor = NULL;


                    connection->connection_flags &= ~Z_CONNECTION_AUTH_TRIED;
                    z_connection_send_status(connection,Z_STATUS_AUTHENTICATED);
                    if(!connection->is_component) {
                        z_parser_reset(connection->parser);
                        if(z_connection_send_header(connection)) {
                            z_connection_send_status(connection,Z_STATUS_UNKNOWN_ERROR);
                            return 1;
                        }
                    }else{
                        z_connection_send_status(connection,Z_STATUS_READY);
                    }
                }
            break;
            case Z_CONNECTION_AUTH_PROCESSOR_WRONG_CREDENTIALS:
                {
                    z_object_unref((ZObject*)connection->auth_processor);
                    connection->auth_processor = NULL;
                    z_connection_send_status(connection,Z_STATUS_WRONG_CREDENTIALS);

                }
            break;
            case Z_CONNECTION_COMPRESSION_PROCESSOR_PROCEED:
                {
                    z_connection_send_status(connection,Z_STATUS_START_COMPRESSION);
                    if(z_compression_processor_proceed(connection->compression_processor,connection)) {
                        z_connection_send_status(connection,Z_STATUS_COMPRESSION_FAILURE);
                        z_object_unref((ZObject*)connection->compression_processor);
                        connection->compression_processor = NULL;
                        if(z_stanza_parser_callback(connection->stanza_parser,connection->features_stanza,Z_XMPP_STANZA_NORMAL,connection)) {
                            z_connection_send_status(connection,Z_STATUS_UNKNOWN_ERROR);
                        }
                        return 0;
                    }
                }
            break;
            case Z_CONNECTION_COMPRESSION_PROCESSOR_FAILURE:
                {
                    z_connection_send_status(connection,Z_STATUS_COMPRESSION_FAILURE);
                    z_object_unref((ZObject*)connection->compression_processor);
                    connection->compression_processor = NULL;
                    if(z_stanza_parser_callback(connection->stanza_parser,connection->features_stanza,Z_XMPP_STANZA_NORMAL,connection)) {
                        z_connection_send_status(connection,Z_STATUS_UNKNOWN_ERROR);
                    }
                    return 0;
                }
            break;
            case Z_CONNECTION_COMPRESSION_PROCESSOR_SUCCEDED:
                {
                    const char *name;
                    ZCompressionMechanism *mechanism;

                    name = z_compression_processor_get_active_compression(connection->compression_processor);
                    mechanism = z_compression_mechanism_create(name);
                    if(!mechanism) {
                        z_connection_send_status(connection,Z_STATUS_UNKNOWN_ERROR);
                        z_object_unref((ZObject*)connection->compression_processor);
                        connection->compression_processor = NULL;
                        return 1;
                    }
                    z_object_unref((ZObject*)connection->compression_processor);
                    connection->compression_processor = NULL;
                    z_socket_set_compression_mechanism(connection->sock,mechanism);
                    z_parser_reset(connection->parser);
                    if(z_connection_send_header(connection)) {
                        z_connection_send_status(connection,Z_STATUS_UNKNOWN_ERROR);
                        return 1;
                    }
                    z_connection_send_status(connection,Z_STATUS_COMPRESSION_SUCCESS);
                }
            break;
            case Z_CONNECTION_START_BIND:
                {
                    z_connection_send_status(connection,Z_STATUS_START_BIND);
                    if(z_connection_start_bind(connection,event.data)) {
                        z_connection_send_status(connection,Z_STATUS_BIND_FAILURE);
                        z_object_unref((ZObject*)event.data);
                        if(z_connection_close(connection)) {
                            return 1;
                        }
                    }
                }
                break;
            case Z_CONNECTION_START_SESSION:
                {
                    z_connection_send_status(connection,Z_STATUS_BIND_SUCCESS);
                    z_connection_send_status(connection,Z_STATUS_START_SESSION);
                    if(z_connection_start_session(connection,event.data)) {
                        z_connection_send_status(connection,Z_STATUS_SESSION_FAILURE);
                        z_object_unref((ZObject*)event.data);
                        if(z_connection_close(connection)) {
                            return 1;
                        }
                    }
                }
                break;
            case Z_CONNECTION_STREAM_REOPENED:
                z_connection_send_status(connection,Z_STATUS_STREAM_REOPENED);
                break;
            case Z_CONNECTION_STREAM_CLOSED:
                {
                    z_connection_send_status(connection,Z_STATUS_STREAM_CLOSED);
                    return 1;
                }
                break;
            case Z_CONNECTION_READY:
                {
                    ZBind *z_bind = (ZBind*)event.data;

                    z_object_unref((ZObject*)z_bind);
                    z_connection_send_status(connection,Z_STATUS_SESSION_SUCCESS);
                    z_connection_send_status(connection,Z_STATUS_READY);
                }
                break;
            case Z_CONNECTION_SOCKET_CLOSED:
                {
                    return 1;
                }
                break;
            case Z_CONNECTION_STANZA_FILTER_FAILED:
                {
                    z_connection_send_status(connection,Z_STATUS_STANZA_FILTER_FAILED);
                    if(z_connection_close(connection)) {
                        return 1;
                    }
                }
                break;
        }
    }

    if(z_socket_iterate(connection->sock)) {
        z_connection_send_status(connection,Z_STATUS_SOCKET_ERROR);
        return 1;
    }
    return 0;
}

ZStanzaFilter *z_connection_get_filter(const ZConnection *connection) {
    if(!connection) {
        return NULL;
    }

    return connection->stanza_filter;
}

const ZJid *z_connection_get_jid(const ZConnection *connection) {
    if(!connection) {
        return NULL;
    }

    return connection->jid;
}

const char *z_connection_get_password(const ZConnection *connection) {
    if(!connection) {
        return NULL;
    }

    return connection->password;
}


int z_connection_send_stanza(const ZConnection *connection,const ZStanza *stanza) {
    char *value;
    int ret = 1;
    if(!stanza) {
        return 1;
    }

    value = z_stanza_to_string(stanza);
    if(!value) {
        return 1;
    }

    ret = z_socket_send(connection->sock,value,0);
    z_free(value);
    return ret;
}

ZSocket *z_connection_get_socket(const ZConnection *connection) {
    if(!connection) {
        return NULL;
    }

    return connection->sock;
}

int z_connection_set_status_callback(ZConnection *connection,ZConnectionStatusCallback callback,void *data) {
    if(!connection || !callback) {
        return 1;
    }

    connection->status_callback = callback;
    connection->status_callback_data = data;
    return 0;
}

int z_connection_change_credentials(ZConnection *connection,ZJid *jid,const char *password) {
    if(!connection || (jid && !password)) {
        return 1;
    }

    if(jid) {
        if(connection->jid) {
            z_object_unref((ZObject*)connection->jid);
        }
        z_object_ref((ZObject*)connection->jid);
        connection->jid = jid;
    }

    if(password) {
        if(connection->password) {
            z_free(connection->password);
            connection->password = NULL;
        }
        connection->password = z_strdup(password);
        if(!connection->password) {
            return 1;
        }
    }

    if(connection->features_stanza && (connection->connection_flags & Z_CONNECTION_AUTH_TRIED)) {
        if(z_stanza_parser_callback(connection->stanza_parser,connection->features_stanza,Z_XMPP_STANZA_NORMAL,connection)) {
            return 1;
        }
    }

    return 0;
}

const char *z_connection_get_stream_id(ZConnection *connection) {
    if(!connection) {
        return NULL;
    }

    return connection->stream_id;
}

char *z_connection_get_next_id(ZConnection *connection) {
    char *buffer = NULL;
    int next_id = connection->next_id++;
    int buffer_size = 0;

    buffer_size = snprintf(buffer,0,Z_CONNECTION_ID_PREFIX"%x",next_id);

    if(buffer_size <= 0) {
        return NULL;
    }

    buffer = z_malloc(buffer_size + 1);
    sprintf(buffer,Z_CONNECTION_ID_PREFIX"%x",next_id);

    return buffer;
}

int z_connection_close(ZConnection *connection) {
    char *buffer = z_malloc(17);
    if(!buffer) {
        return 1;
    }
    sprintf(buffer,"</stream:stream>");
    if(z_socket_send(connection->sock,buffer,0)) {
        z_free(buffer);
        return 1;
    }
    z_free(buffer);

    return 0;

}
