/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#include "zconnection_c.h"
#include "zconstants.h"
#include "zstdlib_c.h"
#include "zauth_processor_c.h"
#include "zcompression_processor.h"
#include "zhelper.h"
#include "zstring.h"
#include "zstanza_filter_c.h"
#include "zparser_c.h"

static void z_bind_free(void *data) {
    ZBind *z_bind = (ZBind*)data;

    if(z_bind->bind_id) {
        z_free(z_bind->bind_id);
    }

    if(z_bind->session_id) {
        z_free(z_bind->session_id);
    }

}

int z_connection_handle_auth_processor(ZXMPP_UNUSED ZAuthProcessor *processor,int state,void *data) {
    ZConnection *connection = (ZConnection*)data;
    ZEvent event;

    switch(state) {
        case Z_AUTH_PROCESSOR_FAILURE:
        {
            event.type = Z_CONNECTION_AUTH_PROCESSOR_FAILURE;
        }
        break;
        case Z_AUTH_PROCESSOR_SUCCESS:
        {
            event.type = Z_CONNECTION_AUTH_PROCESSOR_AUTHENTICATED;
        }
        break;
        case Z_AUTH_PROCESSOR_WRONG_CREDENTIALS:
        {
            event.type = Z_CONNECTION_AUTH_PROCESSOR_WRONG_CREDENTIALS;
        }
    }

    if(z_event_queue_push(connection->event_queue,&event)) {
        return 1;
    }

    return 0;
}

int z_connection_handle_compression_processor(ZXMPP_UNUSED ZCompressionProcessor *processor,int state,void *data) {
    ZConnection *connection = (ZConnection*)data;
    ZEvent event;


    switch(state) {
        case Z_COMPRESSION_PROCESSOR_FAILURE:
        {
            event.type = Z_CONNECTION_COMPRESSION_PROCESSOR_FAILURE;
        }
        break;
        case Z_COMPRESSION_PROCESSOR_SUCCESS:
        {
            event.type = Z_CONNECTION_COMPRESSION_PROCESSOR_SUCCEDED;
        }
        break;
    }

    if(z_event_queue_push(connection->event_queue,&event)) {
        return 1;
    }

    return 0;
}

int z_connection_handle_session(ZXMPP_UNUSED ZStanzaFilter *stanza_filter,ZXMPP_UNUSED ZStanza *stanza,void *data) {
    ZBind *z_bind  = (ZBind*)data;
    ZEvent event;
    ZConnection *connection = z_bind->connection;

    event.type = Z_CONNECTION_READY;
    event.data = z_bind;

    if(z_event_queue_push(connection->event_queue,&event)) {
        z_object_unref((ZObject*)z_bind);
        return -1;
    }
    return 0;
}

int z_connection_start_session(ZConnection *connection,void *data) {
    ZBind *z_bind  = (ZBind*)data;
    ZStanzaFilterRule *rule;
    ZStanza *session_stanza;
    z_bind->session_id = z_connection_get_next_id(connection);

    if(!z_bind->session_id) {
        return 1;
    }
    session_stanza = z_helper_create_session_stanza(z_bind->session_id);
    if(!session_stanza) {
        return 1;
    }

    rule = z_stanza_filter_add_rule(connection->stanza_filter,z_connection_handle_session,z_bind,Z_FILTER_RULE_ID,z_bind->session_id,Z_FILTER_RULE_DONE);
    if(!rule) {
        return 1;
    }
    z_stanza_filter_rule_set_single(rule);
    z_connection_send_stanza(connection,session_stanza);
    z_object_unref((ZObject*)session_stanza);

    return 0;
}

int z_connection_handle_bind(ZStanzaFilter *stanza_filter, ZStanza *stanza,void *data) {
    ZBind *z_bind  = (ZBind*)data;
    const char *type;


    type = z_stanza_get_attribute(stanza,"type");
    if(z_strcmp(type,"error") == 0) {
        z_object_unref((ZObject*)z_bind);
        return -1;
    }else{
        if(z_bind->session_required) {
            ZEvent event;

            event.type = Z_CONNECTION_START_SESSION;
            event.data = z_bind;

            if(z_event_queue_push(z_bind->connection->event_queue,&event)) {
                z_object_unref((ZObject*)z_bind);
                return -1;
            }
        }else{
            return z_connection_handle_session(stanza_filter,stanza,data);
        }
    }

    return 0;

}

int z_connection_start_bind(ZConnection *connection,void *data) {
    ZBind *z_bind = (ZBind*)data;
    ZStanza *bind_stanza;
    ZStanzaFilterRule *rule;


    z_bind->bind_id = z_connection_get_next_id(connection);
    if(!z_bind->bind_id) {
        return 1;
    }
    bind_stanza = z_helper_create_bind_stanza(z_bind->bind_id,z_jid_get(connection->jid,Z_JID_RESOURCE));
    if(!bind_stanza) {
        return 1;
    }

    rule = z_stanza_filter_add_rule(connection->stanza_filter,z_connection_handle_bind,z_bind,Z_FILTER_RULE_ID,z_bind->bind_id,Z_FILTER_RULE_DONE);
    if(!rule) {
        return 1;
    }
    z_stanza_filter_rule_set_single(rule);
    z_connection_send_stanza(connection,bind_stanza);
    z_object_unref((ZObject*)bind_stanza);

    return 0;
}

int z_connection_handle_features(ZXMPP_UNUSED ZStanzaFilter *stanza_filter,ZStanza *stanza,void *data) {
    ZConnection *connection = (ZConnection*)data;
    ZStanza *child = NULL;

    z_object_ref((ZObject*)stanza);
    if(connection->features_stanza) {
        z_object_unref((ZObject*)connection->features_stanza);
        connection->features_stanza = NULL;
    }

    connection->features_stanza = stanza;

    child = z_stanza_get_child_by(stanza,Z_STANZA_CHILD_BY_NAME,"starttls");
    /*Start tls*/
    if(child && !(connection->connection_flags & Z_CONNECTION_TLS_TRIED)) {
        ZEvent event;
        child = z_stanza_new("starttls");
        if(!child) {
            return -1;
        }
        z_stanza_add_attribute(child,"xmlns",ZXMPP_NS_TLS);

        z_connection_send_stanza(connection,child);
        z_object_unref((ZObject*)child);
        connection->connection_flags |= Z_CONNECTION_TLS_TRIED;

        event.type = Z_CONNECTION_START_TLS;
        if(z_event_queue_push(connection->event_queue,&event)) {
            return -1;
        }

        return 0;
    }

    child = z_stanza_get_child_by(stanza,Z_STANZA_CHILD_BY_NAME,"compression");
    /*Start compression*/
    if(child && !(connection->connection_flags & Z_CONNECTION_COMPRESSION_TRIED)) {
        ZStanza *mechanism;
        ZEvent event;


        connection->compression_processor = z_compression_processor_new(z_connection_handle_compression_processor,connection);
        if(!connection->compression_processor) {
            return -1;
        }
        for (mechanism = z_stanza_get_children(child); mechanism; mechanism = z_stanza_get_next(mechanism)) {
            z_compression_processor_add_mechanism(connection->compression_processor,z_stanza_get_text(mechanism));
        }

        event.type = Z_CONNECTION_COMPRESSION_PROCESSOR_PROCEED;
        if(z_event_queue_push(connection->event_queue,&event)) {
            return -1;
        }

        connection->connection_flags |= Z_CONNECTION_COMPRESSION_TRIED;

        return 0;
    }

    child = z_stanza_get_child_by(stanza,Z_STANZA_CHILD_BY_NAME,"mechanisms");
    /*Start mechanisms*/
    if(child) {
        ZEvent event;
        ZStanza *mechanism;

        connection->auth_processor = z_auth_processor_new(z_connection_handle_auth_processor,connection);
        if(!connection->auth_processor) {
            return -1;
        }

        for (mechanism = z_stanza_get_children(child); mechanism; mechanism = z_stanza_get_next(mechanism)) {
            z_auth_processor_add_mechanism(connection->auth_processor,z_stanza_get_text(mechanism));
        }
        event.type = Z_CONNECTION_AUTH_PROCESSOR_PROCEED;
        if(z_event_queue_push(connection->event_queue,&event)) {
            return -1;
        }

        connection->connection_flags |= Z_CONNECTION_AUTH_TRIED;

        return 0;

    }

    child = z_stanza_get_child_by(stanza,Z_STANZA_CHILD_BY_NAME,"bind");
    /*Start Bind*/
    if(child) {
        ZEvent event;
        ZBind *z_bind;

        z_bind = Z_OBJECT_NEW(ZBind);
        if(!z_bind) {
            return -1;
        }
        z_object_add_free_callback((ZObject*)z_bind,z_bind_free);
        z_bind->connection = connection;
        child = z_stanza_get_child_by(stanza,Z_STANZA_CHILD_BY_NAME,"session");
        if(child) {
            z_bind->session_required = 1;
        }
        event.type = Z_CONNECTION_START_BIND;
        event.data = z_bind;
        if(z_event_queue_push(connection->event_queue,&event)) {
            z_object_unref((ZObject*)z_bind);
            return -1;
        }
        return 0;

    }
    return 0;
}

int z_connection_handle_tls(ZXMPP_UNUSED ZStanzaFilter *stanza_filter,ZXMPP_UNUSED ZStanza *stanza,void *data) {
    ZConnection *connection = (ZConnection*)data;
    ZEvent event;


    event.type = Z_CONNECTION_START_TLS_NEGOTIATION;
    if(z_event_queue_push(connection->event_queue,&event)) {
        return -1;
    }

    return 0;
}

int z_connection_handle_ping(ZXMPP_UNUSED ZStanzaFilter *stanza_filter,ZStanza *stanza,void *data) {
    ZConnection *connection = (ZConnection*)data;
    ZStanza *pong_stanza;

    pong_stanza = z_stanza_new("iq");
    z_stanza_add_attribute(pong_stanza,"type","result");
    z_stanza_add_attribute(pong_stanza,"id",z_stanza_get_attribute(stanza,"id"));
    z_stanza_add_attribute(pong_stanza,"to",z_stanza_get_attribute(stanza,"from"));
    z_stanza_add_attribute(pong_stanza,"from",z_stanza_get_attribute(stanza,"to"));

    if(z_connection_send_stanza(connection,pong_stanza)) {
        z_object_unref((ZObject*)pong_stanza);
        return -1;
    }

    z_object_unref((ZObject*)pong_stanza);

    return 0;
}

int z_socket_read_callback(ZXMPP_UNUSED ZSocket *sock,const char *buff,size_t len,void *data) {
    ZConnection *connection = (ZConnection*)data;


    return z_parser_parse(connection->parser,buff,len,0);
}

int z_stanza_parser_callback(ZXMPP_UNUSED ZStanzaParser *stanza_parser,ZStanza *stanza,int stanza_type,void *data) {
    ZConnection *connection = (ZConnection*)data;
    ZEvent event;


    if(stanza_type == Z_XMPP_STANZA_STREAM_START) {
        connection->stream_id = z_strdup(z_stanza_get_attribute(stanza,"id"));

        event.type = Z_CONNECTION_STREAM_OPENED;
        if(connection->is_component) {
            connection->auth_processor = z_auth_processor_new(z_connection_handle_auth_processor,connection);
            if(!connection->auth_processor) {
                return 1;
            }
            z_auth_processor_add_mechanism(connection->auth_processor,ZXMPP_COMPONENT_AUTH_NAME);
        }
        if(z_event_queue_push(connection->event_queue,&event)) {
            return 1;
        }
    }else if(stanza_type == Z_XMPP_STANZA_STREAM_END) {
        event.type = Z_CONNECTION_STREAM_CLOSED;
        if(z_event_queue_push(connection->event_queue,&event)) {
            return 1;
        }
    }else if(stanza_type == Z_XMPP_STANZA_STREAM_RESTART) {
        event.type = Z_CONNECTION_STREAM_REOPENED;
        if(z_event_queue_push(connection->event_queue,&event)) {
            return 1;
        }
    }

    if(z_stanza_filter_process_stanza(connection->stanza_filter,stanza)) {
        event.type = Z_CONNECTION_STANZA_FILTER_FAILED;
        if(z_event_queue_push(connection->event_queue,&event)) {
            return 1;
        }
    }

    return 0;
}

int z_socket_state_callback(ZXMPP_UNUSED ZSocket *sock,int state,void *data) {
    ZConnection *connection = (ZConnection*)data;
    ZEvent event;

    event.type = -1;
    if(state == Z_SOCKET_CONNECTED) {
        event.type = Z_CONNECTION_SOCKET_CONNECTED;
    }else if(state == Z_SOCKET_DISCONNECTED) {
        event.type = Z_CONNECTION_SOCKET_CLOSED;
    }

    if(z_event_queue_push(connection->event_queue,&event) && event.type >= -1) {
        return 1;
    }

    return 0;
}
