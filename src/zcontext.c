/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#include "zcontext.h"
#include "zstdlib.h"
#include "zstring.h"
#include "zobject.h"
#include "zlist.h"
#include "zconnection_c.h"
#include "zeventqueue.h"

#define EVENT_QUEUE_SIZE (16)

#define Z_CONTEXT_DEFAULT_NORMAL_CLOSE_ITERATIONS   (5)

#define Z_CONTEXT_ADD_CONNECTION                    (0)
#define Z_CONTEXT_REMOVE_CONNECTION                 (1)
#define Z_CONTEXT_REMOVE_CONNECTION_GRACEFULLY      (2)

typedef struct ZConnectionItem {
    ZObject object;
    ZConnection *connection;
    int remove_counter;
    struct ZConnectionItem *next;
    struct ZConnectionItem *prev;
}ZConnectionItem;


struct ZContext {
    ZObject object;
    ZConnectionItem *connections_list;
    ZEventQueue *event_queue;
    int is_closing;
    int is_iterating;
};

static void z_connection_item_free(void *data) {
    ZConnectionItem *item = (ZConnectionItem*)data;
    z_object_unref((ZObject*)item->connection);
}

static void z_context_free_item(ZConnectionItem *item) {
    z_object_unref((ZObject*)item);
}


static void z_context_free(void *data) {
    ZContext *context = (ZContext*)data;
    ZConnectionItem *curr;
    ZEvent event;

    context->is_closing = 1;

    while(!z_event_queue_pop(context->event_queue,&event)) {
        z_object_unref((ZObject*)event.data);
    }
    z_object_unref((ZObject*)context->event_queue);
    z_list_free_func(curr,context->connections_list,z_context_free_item);
}



static void z_context_add_connection_priv(ZContext *context,ZConnection *connection) {
    ZConnectionItem *curr;
    ZConnectionItem *new_connection;
    int found = 0;

    foreach_in_list(curr,context->connections_list) {
        if(curr->connection == connection) {
            found = 1;
            break;
        }
    }

    if(found) {
        return;
    }

    new_connection = Z_OBJECT_NEW(ZConnectionItem);
    if(!new_connection) {
        return;
    }
    z_object_add_free_callback((ZObject*)new_connection,z_connection_item_free);

    new_connection->remove_counter = -1;
    z_object_ref((ZObject*)connection);
    new_connection->connection = connection;

    if(!context->connections_list) {
        context->connections_list = new_connection;
    }else{
        curr = context->connections_list;
        while (curr->next) curr = curr->next;
        new_connection->prev = curr;
        curr->next = new_connection;
    }

    return;
}

static ZConnectionItem *z_context_find_connection(ZContext *context,ZConnection *connection) {
    ZConnectionItem *curr;
    int found = 0;

    foreach_in_list(curr,context->connections_list) {
        if(curr->connection == connection) {
            found = 1;
            break;
        }
    }

    if(!found) {
        return NULL;
    }

    return curr;
}


static void z_context_remove_connection_priv(ZContext *context,ZConnection *connection) {
    ZConnectionItem *curr;
    curr = z_context_find_connection(context,connection);
    if(!curr) {
        return;
    }

    if(context->connections_list == curr) {
        context->connections_list = curr->next;
        z_object_unref((ZObject*)curr);
        return;
    }


    if(curr->prev) {
        curr->prev->next = curr->next;
    }
    if(curr->next) {
        curr->next->prev = curr->prev;
    }
    z_object_unref((ZObject*)curr);

    return;
}



ZContext *z_context_new() {
    ZContext *context;

    context = Z_OBJECT_NEW(ZContext);
    if(!context) {
        return NULL;
    }
    z_object_add_free_callback((ZObject*)context,z_context_free);

    context->event_queue = z_event_queue_new(EVENT_QUEUE_SIZE);
    if(!context->event_queue) {
        z_object_unref((ZObject*)context->event_queue);
        return NULL;
    }
    return context;
}

int z_context_add_connection(ZContext *context,ZConnection *connection) {
    ZEvent event;
    if(!context || !connection) {
        return 1;
    }

    if(context->is_iterating) {
        z_object_ref((ZObject*)connection);
        event.data = connection;
        event.type = Z_CONTEXT_ADD_CONNECTION;

        if(z_event_queue_push(context->event_queue,&event)) {
            z_object_unref((ZObject*)connection);
            return 1;
        }
    }else{
        z_context_add_connection_priv(context,connection);
    }

    return 0;
}



int z_context_remove_connection(ZContext *context,ZConnection *connection) {
    ZEvent event;
    if(!context || !connection) {
        return 1;
    }


    /*Try to close the connection gracefully.If that doesnt work remove
      the connection in the next iteration*/
    if(z_connection_close(connection)) {
        if(context->is_iterating) {
            z_object_ref((ZObject*)connection);
            event.data = connection;
            event.type = Z_CONTEXT_REMOVE_CONNECTION;
            if(z_event_queue_push(context->event_queue,&event)) {
                z_object_unref((ZObject*)connection);
                return 1;
            }
        }else{
            z_context_remove_connection_priv(context,connection);
        }
    }else{
        z_object_ref((ZObject*)connection);
        event.data = connection;
        event.type = Z_CONTEXT_REMOVE_CONNECTION_GRACEFULLY;
        if(z_event_queue_push(context->event_queue,&event)) {
            z_object_unref((ZObject*)connection);
            return 1;
        }
    }
    return 0;
}


int z_context_iterate(ZContext *context) {
    ZConnectionItem *connection_item;
    ZEvent event;
    if(!context) {
        return 1;
    }

    context->is_iterating = 1;
    while(!z_event_queue_pop(context->event_queue,&event)) {
        ZConnection *connection = event.data;

        switch(event.type) {
            case Z_CONTEXT_ADD_CONNECTION:
            {
                z_context_add_connection_priv(context,connection);
            }
            break;
            case Z_CONTEXT_REMOVE_CONNECTION:
            {
                z_context_remove_connection_priv(context,connection);
            }
            break;
            case Z_CONTEXT_REMOVE_CONNECTION_GRACEFULLY:
            {
                ZConnectionItem *curr;
                /* If we dont receive a </stream:stream> in the next
                   Z_CONTEXT_DEFAULT_NORMAL_CLOSE_ITERATIONS then we will
                   remove it from the list wihout waiting any more.
                   Else it will be removed when </stream:stream> arrives*/
                curr = z_context_find_connection(context,connection);
                if(curr) {
                    curr->remove_counter = Z_CONTEXT_DEFAULT_NORMAL_CLOSE_ITERATIONS;
                }
            }
            break;
        }
        z_object_unref((ZObject*)connection);
    }
    foreach_in_list(connection_item,context->connections_list) {
        if(connection_item->remove_counter > 0) {
            connection_item->remove_counter--;
        }
        if(z_connection_iterate(connection_item->connection) || !connection_item->remove_counter) {
            z_object_ref((ZObject*)connection_item->connection);
            event.data = connection_item->connection;
            event.type = Z_CONTEXT_REMOVE_CONNECTION;
            if(z_event_queue_push(context->event_queue,&event)) {
                context->is_iterating = 0;
                return 1;
            }
        }
    }
    context->is_iterating = 0;
    return 0;
}

int z_context_has_connections(ZContext *context) {
    if(!context) {
       return 0;
    }

    return context->connections_list ? 1 : 0;
}




