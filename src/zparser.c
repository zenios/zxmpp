/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "zparser.h"
#include "zparser_c.h"
#include "zparser_type.h"
#include "zobject_c.h"
#include <stdio.h>


#if defined(HAVE_EXPAT_PARSER)
#include "parsers/expat.h"
#endif

#if defined(HAVE_XML2_PARSER)
#include "parsers/xml2.h"
#endif

struct ZParser {
    ZObject object;
    const ZParserType *parser;
    ZStanzaParser *stanza_parser;
    ZParserPrivate *private_parser;
};

static const ZParserType *parsers[] = {
#if defined(HAVE_EXPAT_PARSER)
    &z_expat_parser,
#endif
#if defined(HAVE_XML2_PARSER)
    &z_xml2_parser,
#endif
    NULL
};

static void z_parser_free(void *data) {
    ZParser *parser = (ZParser*)data;


    z_object_unref((ZObject*)parser->stanza_parser);
    if(parser->private_parser && parser->parser->parser_ops.parser_private_destroy) {
        parser->parser->parser_ops.parser_private_destroy(parser->private_parser);
    }
}

int z_parsers_init() {
    int i;
    for(i=0;parsers[i];i++) {
        if(!parsers[i]->parser_ops.parser_private_create) {
            return 1;
        }
        if(parsers[i]->parser_ops.parser_private_lib_init) {
            if(parsers[i]->parser_ops.parser_private_lib_init()) {
                return 1;
            }
        }
    }

    return 0;
}

void z_parsers_destroy() {
    int i;
    for(i=0;parsers[i];i++) {
        if(parsers[i]->parser_ops.parser_private_lib_destroy) {
            parsers[i]->parser_ops.parser_private_lib_destroy();
        }
    }
}

ZParser *z_parser_new(ZStanzaParser *stanza_parser) {
    ZParser *parser = NULL;
    if(!stanza_parser) {
        return NULL;
    }

    parser = Z_OBJECT_NEW(ZParser);
    if(!parser) {
        return NULL;
    }
    z_object_add_free_callback((ZObject*)parser,z_parser_free);
    parser->stanza_parser = stanza_parser;
    z_object_ref((ZObject*)stanza_parser);
    parser->parser = parsers[0];
    if(!parser->parser) {
        return NULL;
    }

    parser->private_parser = parser->parser->parser_ops.parser_private_create(stanza_parser);
    if(!parser->private_parser) {
        z_object_unref((ZObject*)parser);
        return NULL;
    }

    return parser;
}

int z_parser_parse (ZParser *parser, const char *data, size_t len, int finish) {
    if(!parser || !data || len <= 0) {
        return 1;
    }
    if(!parser->parser->parser_ops.parser_private_parse) {
        return 1;
    }

    return parser->parser->parser_ops.parser_private_parse(parser->private_parser,data,len,finish);
}

void z_parser_reset (ZParser *parser) {
    if(!parser) {
        return;
    }

    if(parser->parser->parser_ops.parser_private_reset) {
        parser->parser->parser_ops.parser_private_reset(parser->private_parser);
    }
}

