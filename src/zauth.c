/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "zauth_c.h"
#include "zobject_c.h"
#include "zstdlib_c.h"
#include "zstring.h"
#include "zlist.h"
#include <stdio.h>

/*Included types*/
#include "auth/plain.h"
#include "auth/digest_md5.h"
#include "auth/anonymous.h"
#include "auth/component_auth.h"

typedef struct ZAuthMechanismItem {
    const ZAuthMechanismType *mechanism;
    struct ZAuthMechanismItem *next;
    struct ZAuthMechanismItem *prev;
}ZAuthMechanismItem;


struct ZAuthMechanism {
    ZObject parent_object;
    const ZAuthMechanismType  *mechanism_type;
    ZAuthMechanismPrivate *private_mechanism;
};

static ZAuthMechanismItem *mechanisms = NULL;

static void z_auth_mechanism_item_free(ZAuthMechanismItem *mechanism) {
    z_free(mechanism);
}

static void z_auth_mechanism_free(void *data) {
    ZAuthMechanism *mechanism = (ZAuthMechanism*)data;

    if(mechanism->mechanism_type->auth_mechanism_ops.destroy_callback) {
        mechanism->mechanism_type->auth_mechanism_ops.destroy_callback(mechanism->private_mechanism);
    }
}

static const ZAuthMechanismType *mechanism_type[] = {
    &z_auth_mechanism_plain,
    &z_auth_mechanism_digest_md5,
    &z_auth_mechanism_anonymous,
    &z_auth_mechanism_component,
    NULL
};

int z_auth_init() {
    int i;
    for(i=0;mechanism_type[i];i++) {
        if(z_auth_register_mechanism(mechanism_type[i])) {
            return 1;
        }
    }

    return 0;
}

void z_auth_destroy() {
    ZAuthMechanismItem *curr;
    z_list_free_func(curr,mechanisms,z_auth_mechanism_item_free);
}

int z_auth_register_mechanism(const ZAuthMechanismType *mechanism) {
    ZAuthMechanismItem *new_mechanism,*curr;
    if(!mechanism || !mechanism->auth_mechanism_ops.create_callback) {
        return 1;
    }


    /*Check if we allready have this type*/
    foreach_in_list(curr,mechanisms) {
        if(z_strcmp(curr->mechanism->auth_mechanism_name,mechanism->auth_mechanism_name) == 0) {
            curr->mechanism = mechanism;
            return 0;
        }
    }

    new_mechanism = z_malloc(sizeof(ZAuthMechanismItem));
    if(!new_mechanism) {
        return 1;
    }
    z_memset(new_mechanism,0,sizeof(ZAuthMechanismItem));
    new_mechanism->mechanism = mechanism;

    if(!mechanisms) {
        mechanisms = new_mechanism;
    }else{
        curr = mechanisms;
        while (curr->next) curr = curr->next;
        curr->next = new_mechanism;
        new_mechanism->prev = curr;
    }

    return 0;
}

int z_auth_unregister_mechanism(const ZAuthMechanismType *mechanism) {
    ZAuthMechanismItem *curr;
    int found = 0;
    if(!mechanism || !mechanism->auth_mechanism_ops.create_callback) {
        return 1;
    }

    foreach_in_list(curr,mechanisms) {
        if(curr->mechanism == mechanism) {
            found = 1;
            break;
        }
    }

    if(!found) {
        return 1;
    }

    if(mechanisms == curr) {
        mechanisms = mechanisms->next;
        z_auth_mechanism_item_free(curr);
        return 0;
    }

    if(curr->prev) {
        curr->prev->next = curr->next;
    }
    if(curr->next) {
        curr->next->prev = curr->prev;
    }
    z_auth_mechanism_item_free(curr);

    return 0;
}

ZAuthMechanism *z_auth_mechanism_create(const char *name) {
    ZAuthMechanismItem *curr;
    if(!name) {
        return NULL;
    }

    foreach_in_list(curr,mechanisms) {
        if(z_strcmp(curr->mechanism->auth_mechanism_name,name) == 0 && curr->mechanism->auth_mechanism_ops.create_callback && curr->mechanism->auth_mechanism_ops.proceed_callback) {
            ZAuthMechanism *mechanism = Z_OBJECT_NEW(ZAuthMechanism);
            if(!mechanism) {
                return NULL;
            }
            z_object_add_free_callback((ZObject*)mechanism,z_auth_mechanism_free);
            mechanism->mechanism_type = curr->mechanism;
            mechanism->private_mechanism = curr->mechanism->auth_mechanism_ops.create_callback();
            if(!mechanism->private_mechanism) {
                z_object_unref((ZObject*)mechanism);
                return NULL;
            }
            return mechanism;
        }
    }

    return NULL;
}


void z_auth_mechanism_destroy(ZAuthMechanism *mechanism) {
    if(!mechanism) {
        return;
    }
    z_object_unref((ZObject*)mechanism);
}

int z_auth_mechanism_get_priority(const char *name) {
    ZAuthMechanismItem *curr;
    if(!name) {
        return -1;
    }

    foreach_in_list(curr,mechanisms) {
        if(z_strcmp(curr->mechanism->auth_mechanism_name,name) == 0) {
            return curr->mechanism->priority;
        }
    }

    return -1;
}

int z_auth_mechanism_proceed(ZAuthMechanism *mechanism,ZAuthProcessor *processor,ZConnection *connection) {
    if(!mechanism || !processor || !connection) {
        return 1;
    }

    return mechanism->mechanism_type->auth_mechanism_ops.proceed_callback(mechanism->private_mechanism,processor,connection);
}

int z_auth_mechanism_is_supported(const char *name) {
    ZAuthMechanismItem *curr;

    foreach_in_list(curr,mechanisms) {
        if(z_strcmp(curr->mechanism->auth_mechanism_name,name) == 0) {
            return 1;
        }
    }

    return 0;
}
