/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "common.h"


#if defined(HAVE_GNU_TLS)
#include <gnutls/gnutls.h>
#include <unistd.h>
#include "tls/gnutls.h"
#include "zstring.h"
#include "zstdlib_c.h"

struct ZTlsPrivate {
    int socket;
    gnutls_session_t session;
    gnutls_certificate_credentials_t cred;
};

static int z_gnu_tls_library_init(void) {
    return gnutls_global_init();
}

static void z_gnu_tls_library_destroy(void) {
    gnutls_global_deinit();
}

static void z_gnu_tls_destroy(ZTlsPrivate *tls) {
    gnutls_certificate_free_credentials(tls->cred);
    gnutls_deinit(tls->session);
    z_free(tls);
}

static ZTlsPrivate *z_gnu_tls_create(int socket) {
    ZTlsPrivate *tls = z_malloc(sizeof(ZTlsPrivate));
    if(!tls) {
        return NULL;
    }
    z_memset(tls,0,sizeof(ZTlsPrivate));

    tls->socket = socket;
    if(gnutls_init(&tls->session, GNUTLS_CLIENT)) {
        z_gnu_tls_destroy(tls);
        return NULL;
    }

    if(gnutls_certificate_allocate_credentials(&tls->cred)) {
        z_gnu_tls_destroy(tls);
        return NULL;
    }

    if(gnutls_set_default_priority(tls->session)) {
        z_gnu_tls_destroy(tls);
        return NULL;
    }

    if(gnutls_credentials_set(tls->session, GNUTLS_CRD_CERTIFICATE, tls->cred)) {
        z_gnu_tls_destroy(tls);
        return NULL;
    }
    gnutls_transport_set_ptr(tls->session,(gnutls_transport_ptr_t)(intptr_t)tls->socket);
    return tls;
}

static int z_gnu_tls_handshake(ZTlsPrivate *tls) {
    int ret;

    do {
        ret = gnutls_handshake(tls->session);
    } while (ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED);

    return ret;
}

static int z_gnu_tls_write(ZTlsPrivate *tls,void *data,size_t len) {
    ssize_t sendlen = 0;

    do {
        sendlen = gnutls_record_send(tls->session, data, len);
    } while (sendlen == GNUTLS_E_INTERRUPTED || sendlen == GNUTLS_E_AGAIN);

    return sendlen;
}

static int z_gnu_tls_read(ZTlsPrivate *tls,void *buff,size_t len) {
    ssize_t recvlen = 0;

    do {
        recvlen = gnutls_record_recv(tls->session, buff, len);
    } while (recvlen == GNUTLS_E_INTERRUPTED || recvlen == GNUTLS_E_AGAIN);

    return recvlen;
}



struct ZTlsType z_gnu_tls = {
    "gnu_tls",
    {
        z_gnu_tls_library_init,
        z_gnu_tls_library_destroy,
        z_gnu_tls_create,
        z_gnu_tls_destroy,
        z_gnu_tls_handshake,
        z_gnu_tls_write,
        z_gnu_tls_read,
    }
};
#endif
