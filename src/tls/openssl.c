/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "common.h"


#if defined(HAVE_OPENSSL)
#if defined(ZXMPP_WIN32)
# include <winsock2.h>
#endif
#include <openssl/ssl.h>
#include <openssl/rand.h>
#include <openssl/err.h>
#include "tls/openssl.h"
#include "zstring.h"
#include "zstdlib_c.h"

struct ZTlsPrivate {
    int socket;
    SSL_CTX *ssl_ctx;
    SSL *ssl;
};

static int z_openssl_library_init(void) {
    SSL_load_error_strings();
    SSL_library_init();

    return 0;
}

static void z_openssl_destroy(ZTlsPrivate *tls) {
    ERR_free_strings();
    SSL_shutdown(tls->ssl);
    SSL_free(tls->ssl);
    SSL_CTX_free(tls->ssl_ctx);
    z_free(tls);
}

static ZTlsPrivate *z_openssl_create(int sock) {
    int ret;
    ZTlsPrivate *tls = z_malloc(sizeof(ZTlsPrivate));
    if(!tls) {
        return NULL;
    }
    z_memset(tls,0,sizeof(ZTlsPrivate));

    tls->socket = sock;
    tls->ssl_ctx = SSL_CTX_new(SSLv23_client_method());
    if(!tls->ssl_ctx) {
        z_openssl_destroy(tls);
        return NULL;
    }

	SSL_CTX_set_client_cert_cb(tls->ssl_ctx, NULL);
	SSL_CTX_set_mode (tls->ssl_ctx, SSL_MODE_ENABLE_PARTIAL_WRITE);
	SSL_CTX_set_verify (tls->ssl_ctx, SSL_VERIFY_NONE, NULL);

	tls->ssl = SSL_new(tls->ssl_ctx);
	ret = SSL_set_fd(tls->ssl, tls->socket);
	if (ret <= 0) {
        z_openssl_destroy(tls);
        return NULL;
	}

    return tls;
}


static int z_openssl_handshake(ZTlsPrivate *tls) {
    int ret;


    do {
        ret = SSL_connect(tls->ssl);
        /* wait for something to happen on the sock before looping back */
        if (ret == -1) {
            fd_set fds;
            struct timeval tv;

            tv.tv_sec = 0;
            tv.tv_usec = 1000;

            FD_ZERO(&fds);
            FD_SET(tls->socket, &fds);

            select(tls->socket + 1, &fds, &fds, NULL, &tv);

        }
    } while (ret == -1);

    return !ret;
}

static int z_openssl_write(ZTlsPrivate *tls,void *data,size_t len) {
    return SSL_write(tls->ssl, data, len);

}

static int z_openssl_read(ZTlsPrivate *tls,void *buff,size_t len) {
    return SSL_read(tls->ssl, buff, len);
}


struct ZTlsType z_openssl = {
    "z_openssl",
    {
        z_openssl_library_init,
        NULL,
        z_openssl_create,
        z_openssl_destroy,
        z_openssl_handshake,
        z_openssl_write,
        z_openssl_read,
    }
};
#endif

