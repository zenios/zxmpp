/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#define DEFAULT_SIZE (50)

#include "zeventqueue.h"
#include "zmutex.h"
#include "zobject_c.h"
#include "zstdlib_c.h"
#include "zstring.h"

struct ZEventQueue {
    ZObject object;
    ZMutex *mtx;
    ZEvent *events;
    size_t events_size;
    size_t num_events;
    int head;
    int tail;
};


static void z_event_queue_free(void *data) {
    ZEventQueue *event_queue = (ZEventQueue*)data;

    if(event_queue->mtx) {
        z_mutex_destroy(event_queue->mtx);
    }

    if(event_queue->events) {
        z_free(event_queue->events);
    }

}

ZEventQueue *z_event_queue_new(size_t events_size)
{
    ZEventQueue *event_queue;
    if(!events_size) {
        events_size = DEFAULT_SIZE;
    }

    event_queue = Z_OBJECT_NEW(ZEventQueue);
    if (!event_queue) {
        return NULL;
    }

    z_object_add_free_callback((ZObject*)event_queue,z_event_queue_free);

    event_queue->events_size = events_size;
    event_queue->events = z_malloc(sizeof(ZEvent) * events_size);
    if (!event_queue->events) {
        z_object_unref((ZObject*)event_queue);
        return NULL;
    }

    event_queue->mtx = z_mutex_create();

    if (!event_queue->mtx) {
        z_object_unref((ZObject*)event_queue);
        return NULL;
    }

    return event_queue;
}

int z_event_queue_push(ZEventQueue *event_queue, ZEvent *event)
{
    z_mutex_lock(event_queue->mtx);
    if (event_queue->num_events == event_queue->events_size) {
        z_mutex_unlock(event_queue->mtx);
        return 1;
    }
    z_memcpy(&event_queue->events[event_queue->tail], event, sizeof(ZEvent));
    event_queue->num_events++;
    event_queue->tail = (int)((event_queue->tail + 1) % event_queue->events_size);
    z_mutex_unlock(event_queue->mtx);
    return 0;
}

int z_event_queue_has_events(ZEventQueue *event_queue)
{
    return event_queue != NULL && event_queue->num_events > 0;
}

int z_event_queue_pop(ZEventQueue *event_queue, ZEvent *event)
{
    z_mutex_lock(event_queue->mtx);
    if (!event_queue->num_events) {
        z_mutex_unlock(event_queue->mtx);
        return 1;
    }
    z_memcpy(event, &event_queue->events[event_queue->head], sizeof(ZEvent));
    event_queue->num_events--;
    event_queue->head = (int)((event_queue->head + 1) % event_queue->events_size);
    z_mutex_unlock(event_queue->mtx);
    return 0;
}

size_t z_event_queue_pop_many(ZEventQueue *event_queue, ZEvent *event, size_t events_size)
{
    size_t events_popped = 0;

    z_mutex_lock(event_queue->mtx);
    if (!event_queue->num_events) {
        z_mutex_unlock(event_queue->mtx);
        return 0;
    }

    if (events_size > event_queue->num_events) {
        events_size = event_queue->num_events;
    }

    while (events_size) {
        z_memcpy(&event[events_popped], &event_queue->events[event_queue->head], sizeof(ZEvent));
        event_queue->num_events--;
        event_queue->head = (int)((event_queue->head + 1) % event_queue->events_size);
        events_size--;
        events_popped++;
    }
    z_mutex_unlock(event_queue->mtx);
    return events_popped;
}
