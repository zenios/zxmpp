/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#include "zstanza_parser_c.h"
#include "zobject_c.h"
#include "zstdlib.h"
#include "zstring.h"

struct ZStanzaParser {
    ZObject object;
    ZStanza *stanza;
    int depth;
    ZStanzaParserCallback callback;
    void *data;
    int stream_started;
};


static void z_stanza_parser_free(void *data) {
    ZStanzaParser *stanza_parser = (ZStanzaParser*)data;

    if(stanza_parser->stanza) {
        z_object_unref((ZObject*)stanza_parser->stanza);
    }
}

ZStanzaParser *z_stanza_parser_new(ZStanzaParserCallback callback,void *data) {
    ZStanzaParser *stanza_parser;
    if(!callback) {
        return NULL;
    }
    stanza_parser = Z_OBJECT_NEW(ZStanzaParser);
    if(!stanza_parser) {
        return NULL;
    }
    z_object_add_free_callback((ZObject*)stanza_parser,z_stanza_parser_free);

    stanza_parser->callback = callback;
    stanza_parser->data = data;

    return stanza_parser;
}

static void z_stanza_parser_set_attributes(ZStanza *stanza, const char **attrs)
{
    int i;
    if (!attrs || !attrs[0]) {
        return;
    }

    for (i = 0; attrs[i]; i += 2) {
        z_stanza_add_attribute(stanza, attrs[i], attrs[i+1]);
    }
}


int z_stanza_parser_start_element(ZStanzaParser *stanza_parser,const char *name,const char **attrs) {
    ZStanza *child;

    if(!stanza_parser || !name) {
        return 1;
    }

    if (stanza_parser->depth != 0) {
        if (!stanza_parser->stanza) {
            /* starting a new toplevel stanza */
            stanza_parser->stanza = z_stanza_new(name);
            if (!stanza_parser->stanza) {
                return 1;
            }
            z_stanza_parser_set_attributes(stanza_parser->stanza, attrs);
        } else {
            /* starting a child of parser->stanza */
            child = z_stanza_new(name);
            if (!child) {
                return 1;
            }
            z_stanza_parser_set_attributes(child, attrs);

            /* add child to parent */
            z_stanza_add_child(stanza_parser->stanza, child);

            /* the child is owned by the toplevel stanza now */
            z_object_unref((ZObject*)child);

            /* make child the current stanza */
            stanza_parser->stanza = child;
        }
    }else{
        child = z_stanza_new(name);
        if (!child) {
            return 1;
        }
        z_stanza_parser_set_attributes(child, attrs);
        if(stanza_parser->callback(stanza_parser,child,stanza_parser->stream_started ? Z_XMPP_STANZA_STREAM_RESTART : Z_XMPP_STANZA_STREAM_START,stanza_parser->data)) {
            z_object_unref((ZObject*)child);
            return 1;
        }
        z_object_unref((ZObject*)child);
        stanza_parser->stream_started = 1;
    }
    stanza_parser->depth++;

    return 0;
}

int z_stanza_parser_end_element(ZStanzaParser *stanza_parser,const char *name) {


    if(!stanza_parser || !name) {
        return 1;
    }
    stanza_parser->depth--;
    if (stanza_parser->depth != 0) {
        ZStanza *parent;
        parent = z_stanza_get_parent(stanza_parser->stanza);
        if (parent) {
            /* we're finishing a child stanza, so set current to the parent */
            stanza_parser->stanza = parent;
        } else {
            if(stanza_parser->callback(stanza_parser,stanza_parser->stanza,Z_XMPP_STANZA_NORMAL,stanza_parser->data)) {
                return 1;
            }
            z_object_unref((ZObject*)stanza_parser->stanza);
            stanza_parser->stanza = NULL;
        }
    }else{
        ZStanza *stanza = z_stanza_new(name);
        if(!stanza) {
            return 1;
        }
        if(stanza_parser->callback(stanza_parser,stanza,Z_XMPP_STANZA_STREAM_END,stanza_parser->data)) {
            z_object_unref((ZObject*)stanza);
            return 1;
        }
        z_object_unref((ZObject*)stanza);
    }

    return 0;
}

int z_stanza_parser_handle_characters(ZStanzaParser *stanza_parser,const char *s,int len) {

    if(!stanza_parser || !s || !len) {
        return 1;
    }

    z_stanza_append_text(stanza_parser->stanza, s,len);

    return 0;
}

int z_stanza_parser_reset(ZStanzaParser *stanza_parser) {

    if(!stanza_parser) {
        return 1;
    }

    stanza_parser->depth = 0;
    if(stanza_parser->stanza) {
        z_object_unref((ZObject*)stanza_parser->stanza);
        stanza_parser->stanza = NULL;
    }


    return 0;
}
