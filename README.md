ZXMPP is a general library the provides support for connecting to xmpp servers.ZXMPP is written 
in C, but works with C++ natively.

This library is distributed under the GPL license, which can be found in the file "COPYING".

Tools:
In the tools folder you can find 2 examples of the library in action
1.zxmpp_client is a small client that connects to google talk servers.Once someone sends you a chat message
with body "exit" the client disconnects.
2.zxmpp_component is an application that connect to xmpp servers as a component.

Features:
Supports Windows,Linux,OSX. Only tested on linux and OSX ( It can be easily ported to any other platform )
Compression using zlib or any other provider you want ( You can register your own compression mechanism )
Encryption using either GnuTLS or OpenSSL
Anonymous,digest-md5,component and plain auth ( You can also register your own auth providers )
