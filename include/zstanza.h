/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_STANZA_H_
#define _ZXMPP_STANZA_H_

#include <zhashtable.h>

#ifdef __cplusplus
extern "C" {
#endif

#define Z_STANZA_UNKNOWN                (0)
#define Z_STANZA_TAG                    (1)
#define Z_STANZA_CDATA                  (2)


#define Z_STANZA_CHILD_BY_NAME          (0)
#define Z_STANZA_CHILD_BY_NS            (1)

struct ZStanza;
typedef struct ZStanza ZStanza;

ZXMPP_EXPORT ZStanza *z_stanza_new(const char *name);
ZXMPP_EXPORT int z_stanza_append_text(ZStanza *stanza,const char *text,size_t len);
ZXMPP_EXPORT const char *z_stanza_get_text(const ZStanza *stanza);
ZXMPP_EXPORT int z_stanza_add_attribute(ZStanza *stanza,const char *key,const char *value);
ZXMPP_EXPORT ZHashTableIter *z_stanza_get_attributes(const ZStanza *stanza);
ZXMPP_EXPORT const char *z_stanza_get_attribute(const ZStanza *stanza,const char *name);
ZXMPP_EXPORT int z_stanza_add_child(ZStanza *parent,ZStanza *children);
ZXMPP_EXPORT const char *z_stanza_get_name(const ZStanza *stanza);
ZXMPP_EXPORT char *z_stanza_to_string(const ZStanza *stanza);
ZXMPP_EXPORT ZStanza *z_stanza_get_parent(const ZStanza *stanza);
ZXMPP_EXPORT ZStanza *z_stanza_get_children(const ZStanza *stanza);
ZXMPP_EXPORT ZStanza *z_stanza_get_next(const ZStanza *stanza);
ZXMPP_EXPORT ZStanza *z_stanza_get_child_by(const ZStanza *stanza,int type,const char *value);


#ifdef __cplusplus
}
#endif

#endif







