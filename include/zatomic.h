/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_ATOMIC_H_
#define _ZXMPP_ATOMIC_H_

#include <common.h>

#ifdef __cplusplus
extern "C" {
#endif

#if defined(ZXMPP_WIN32)
#  if defined (__GNUC__)
typedef long __attribute__ ((__may_alias__)) ZAtomic;
#  else
typedef long ZAtomic;
#  endif
#else
typedef int ZAtomic;
#endif


ZXMPP_PRIVATE void z_atomic_init(void);
ZXMPP_PRIVATE void z_atomic_destroy(void);
ZXMPP_PRIVATE void z_atomic_inc(ZAtomic *atomic);
ZXMPP_PRIVATE int z_atomic_dec(ZAtomic *atomic);

#ifdef __cplusplus
}
#endif

#endif






