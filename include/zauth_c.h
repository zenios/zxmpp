/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _ZXMPP_AUTH_C_H_
#define _ZXMPP_AUTH_C_H_

#include <zauth_processor.h>
#include <zconnection.h>

#ifdef __cplusplus
extern "C" {
#endif

struct ZAuthMechanism;
typedef struct ZAuthMechanism ZAuthMechanism;

ZXMPP_PRIVATE int z_auth_init(void);
ZXMPP_PRIVATE void z_auth_destroy(void);
ZXMPP_PRIVATE ZAuthMechanism *z_auth_mechanism_create(const char *name);
ZXMPP_PRIVATE void z_auth_mechanism_destroy(ZAuthMechanism *mechanism);
ZXMPP_PRIVATE int z_auth_mechanism_is_supported(const char *name);
ZXMPP_PRIVATE int z_auth_mechanism_proceed(ZAuthMechanism *mechanism,ZAuthProcessor *processor,ZConnection *connection);
ZXMPP_PRIVATE int z_auth_mechanism_get_priority(const char *name);

#ifdef __cplusplus
}
#endif

#endif


