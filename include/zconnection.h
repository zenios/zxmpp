/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_CONNECTION_H_
#define _ZXMPP_CONNECTION_H_

#include <zjid.h>
#include <zstanza_filter.h>
#include <zsocket.h>

#ifdef __cplusplus
extern "C" {
#endif

#define Z_STATUS_SOCKET_OPENED          (0)
#define Z_STATUS_SOCKET_CLOSED          (1)
#define Z_STATUS_STREAM_OPENED          (2)
#define Z_STATUS_STREAM_CLOSED          (3)
#define Z_STATUS_STREAM_REOPENED        (4)
#define Z_STATUS_WRONG_CREDENTIALS      (5)
#define Z_STATUS_AUTHENTICATION_FAILURE (6)
#define Z_STATUS_AUTHENTICATED          (7)
#define Z_STATUS_AUTHENTICATING         (8)
#define Z_STATUS_READY                  (9)
#define Z_STATUS_START_BIND             (10)
#define Z_STATUS_BIND_SUCCESS           (11)
#define Z_STATUS_START_SESSION          (12)
#define Z_STATUS_SESSION_SUCCESS        (13)
#define Z_STATUS_START_COMPRESSION      (14)
#define Z_STATUS_COMPRESSION_SUCCESS    (15)
#define Z_STATUS_COMPRESSION_FAILURE    (16)
#define Z_STATUS_START_TLS              (17)
#define Z_STATUS_TLS_FAILURE            (18)
#define Z_STATUS_TLS_SUCCESS            (19)
#define Z_STATUS_BIND_FAILURE           (20)
#define Z_STATUS_SESSION_FAILURE        (21)
#define Z_STATUS_SOCKET_ERROR           (22)
#define Z_STATUS_STANZA_FILTER_FAILED   (23)
#define Z_STATUS_UNKNOWN_ERROR          (24)

struct ZConnection;
typedef struct ZConnection ZConnection;

typedef void (*ZConnectionStatusCallback) (ZConnection *connection, int status,void *data);

ZXMPP_EXPORT ZConnection *z_connection_new(ZJid *jid,const char *password,int component);
ZXMPP_EXPORT ZStanzaFilter *z_connection_get_filter(const ZConnection *connection);
ZXMPP_EXPORT const ZJid *z_connection_get_jid(const ZConnection *connection);
ZXMPP_EXPORT const char *z_connection_get_password(const ZConnection *connection);
ZXMPP_EXPORT ZSocket *z_connection_get_socket(const ZConnection *connection);
ZXMPP_EXPORT int z_connection_send_stanza(const ZConnection *connection,const ZStanza *stanza);
ZXMPP_EXPORT int z_connection_set_status_callback(ZConnection *connection,ZConnectionStatusCallback callback,void *data);
ZXMPP_EXPORT int z_connection_change_credentials(ZConnection *connection,ZJid *jid,const char *password);
ZXMPP_EXPORT const char *z_connection_get_stream_id(ZConnection *connection);
ZXMPP_EXPORT char *z_connection_get_next_id(ZConnection *connection);

#ifdef __cplusplus
}
#endif

#endif





