/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_STANZA_PARSER_C_H_
#define _ZXMPP_STANZA_PARSER_C_H_

#include <zstanza.h>
#include <zstanza_parser.h>

#ifdef __cplusplus
extern "C" {
#endif

#define Z_XMPP_STANZA_STREAM_START      (0)
#define Z_XMPP_STANZA_STREAM_END        (1)
#define Z_XMPP_STANZA_STREAM_RESTART    (2)
#define Z_XMPP_STANZA_NORMAL            (3)


typedef int (*ZStanzaParserCallback) (ZStanzaParser *stanza_parser,ZStanza *stanza,int stanza_type,void *data);

ZXMPP_PRIVATE ZStanzaParser *z_stanza_parser_new(ZStanzaParserCallback callback,void *data);

#ifdef __cplusplus
}
#endif

#endif







