/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_AUTH_MECHANISM_TYPE_H_
#define _ZXMPP_AUTH_MECHANISM_TYPE_H_

#include <zauth.h>
#include <zauth_processor.h>
#include <zconnection.h>

#ifdef __cplusplus
extern "C" {
#endif

struct ZAuthMechanismPrivate;
typedef struct ZAuthMechanismPrivate ZAuthMechanismPrivate;

typedef ZAuthMechanismPrivate* (*z_auth_mechanism_create_callback) (void);
typedef void (*z_auth_mechanism_destroy_callback) (ZAuthMechanismPrivate *auth_mechanism);
typedef int (*z_auth_mechanism_proceed_callback) (ZAuthMechanismPrivate *auth_mechanism,ZAuthProcessor *processor,ZConnection *connection);

typedef struct z_auth_mechanism_ops {
    z_auth_mechanism_create_callback    create_callback;
    z_auth_mechanism_destroy_callback   destroy_callback;
    z_auth_mechanism_proceed_callback   proceed_callback;
}z_auth_mechanism_ops;

struct ZAuthMechanismType {
    const char *auth_mechanism_name;
    int priority;
    z_auth_mechanism_ops auth_mechanism_ops;
};

#ifdef __cplusplus
}
#endif

#endif








