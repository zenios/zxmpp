/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_MD5_H_
#define _ZXMPP_MD5_H_

#include <common.h>

#ifdef __cplusplus
extern "C" {
#endif

struct ZMd5;
typedef struct ZMd5 ZMd5;

ZXMPP_PRIVATE ZMd5 *z_md5_new(void);
ZXMPP_PRIVATE int z_md5_reset(ZMd5 *md5);
ZXMPP_PRIVATE void z_md5_hash(ZMd5 *md5, const unsigned char *data, size_t slen, int finish);
ZXMPP_PRIVATE void z_md5_digest(ZMd5 *md5, unsigned char *digest);
ZXMPP_PRIVATE void z_md5_print(ZMd5 *md5, char *buf);

#ifdef __cplusplus
}
#endif

#endif










