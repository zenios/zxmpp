/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _ZXMPP_TLS_TYPE_H_
#define _ZXMPP_TLS_TYPE_H_

#include <ztls.h>

#ifdef __cplusplus
extern "C" {
#endif

struct ZTlsPrivate;
typedef struct ZTlsPrivate ZTlsPrivate;


typedef int             (*z_tls_private_lib_init)            (void);
typedef void            (*z_tls_private_lib_destroy)         (void);
typedef ZTlsPrivate*    (*z_tls_private_create)              (int socket);
typedef void            (*z_tls_private_destroy)             (ZTlsPrivate *tls);
typedef int             (*z_tls_private_handshake)           (ZTlsPrivate *tls);
typedef int             (*z_tls_private_write)               (ZTlsPrivate *tls,void *data,size_t len);
typedef int             (*z_tls_private_read)                (ZTlsPrivate *tls,void *buff,size_t len);

typedef struct z_tls_ops {
    z_tls_private_lib_init                tls_private_lib_init;
    z_tls_private_lib_destroy             tls_private_lib_destroy;
    z_tls_private_create                  tls_private_create;
    z_tls_private_destroy                 tls_private_destroy;
    z_tls_private_handshake               tls_private_handshake;
    z_tls_private_write                   tls_private_write;
    z_tls_private_read                    tls_private_read;
}z_tls_ops;

typedef struct ZTlsType {
    const char *tls_name;
    z_tls_ops tls_ops;
}ZTlsType;

#ifdef __cplusplus
}
#endif

#endif



