/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_CONSTANTS_H_
#define _ZXMPP_CONSTANTS_H_


#ifdef __cplusplus
extern "C" {
#endif

#define ZXMPP_NS_CLIENT             "jabber:client"
#define ZXMPP_NS_COMPONENT          "jabber:component:accept"
#define ZXMPP_NS_TLS                "urn:ietf:params:xml:ns:xmpp-tls"
#define ZXMPP_NS_SASL               "urn:ietf:params:xml:ns:xmpp-sasl"
#define ZXMPP_NS_BIND               "urn:ietf:params:xml:ns:xmpp-bind"
#define ZXMPP_NS_SESSION            "urn:ietf:params:xml:ns:xmpp-session"
#define ZXMPP_NS_PING               "urn:xmpp:ping"
#define ZXMPP_NS_STREAM             "http://etherx.jabber.org/streams"
#define ZXMPP_NS_COMPRESSION        "http://jabber.org/protocol/compress"
#define ZXMPP_COMPONENT_AUTH_NAME   "ZXMPP_COMPONENT_AUTH"


#ifdef __cplusplus
}
#endif

#endif







