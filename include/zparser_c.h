/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _ZXMPP_PARSER_C_H_
#define _ZXMPP_PARSER_C_H_

#include <zparser.h>
#include <zstanza_parser.h>

#ifdef __cplusplus
extern "C" {
#endif

ZXMPP_PRIVATE ZParser *z_parser_new (ZStanzaParser *stanza_parser);
ZXMPP_PRIVATE int z_parser_parse (ZParser *parser, const char *data, size_t len, int finish);
ZXMPP_PRIVATE void z_parser_reset (ZParser *parser);
ZXMPP_PRIVATE int z_parsers_init(void);
ZXMPP_PRIVATE void z_parsers_destroy(void);

#ifdef __cplusplus
}
#endif

#endif

