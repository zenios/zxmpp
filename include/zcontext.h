/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_CONTEXT_H_
#define _ZXMPP_CONTEXT_H_

#include <zconnection.h>

#ifdef __cplusplus
extern "C" {
#endif


struct ZContext;
typedef struct ZContext ZContext;

ZXMPP_EXPORT ZContext *z_context_new(void);
ZXMPP_EXPORT int z_context_add_connection(ZContext *context,ZConnection *connection);
ZXMPP_EXPORT int z_context_remove_connection(ZContext *context,ZConnection *connection);
ZXMPP_EXPORT int z_context_iterate(ZContext *context);
ZXMPP_EXPORT int z_context_has_connections(ZContext *context);

#ifdef __cplusplus
}
#endif

#endif




