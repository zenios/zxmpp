/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_AUTH_PROCESSOR_H_
#define _ZXMPP_AUTH_PROCESSOR_H_

#include <common.h>

#ifdef __cplusplus
extern "C" {
#endif

#define Z_AUTH_PROCESSOR_FAILURE            (0)
#define Z_AUTH_PROCESSOR_SUCCESS            (1)
#define Z_AUTH_PROCESSOR_WRONG_CREDENTIALS  (2)

struct ZAuthProcessor;
typedef struct ZAuthProcessor ZAuthProcessor;

ZXMPP_EXPORT int z_auth_processor_set_status(ZAuthProcessor *processor,int status);

#ifdef __cplusplus
}
#endif

#endif








