/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_CONNECTION_C_H_
#define _ZXMPP_CONNECTION_C_H_

#include <zconnection.h>
#include <zobject_c.h>
#include <zsocket_c.h>
#include <zparser.h>
#include <zstanza_parser_c.h>
#include <zstanza_filter.h>
#include <zjid.h>
#include <zeventqueue.h>
#include <zauth_processor.h>
#include <zcompression_processor.h>

#ifdef __cplusplus
extern "C" {
#endif

#define Z_CONNECTION_ID_PREFIX                          "zxmpp"

#define Z_CONNECTION_SOCKET_CONNECTED                   (1)
#define Z_CONNECTION_START_TLS                          (2)
#define Z_CONNECTION_AUTH_PROCESSOR_PROCEED             (3)
#define Z_CONNECTION_AUTH_PROCESSOR_AUTHENTICATED       (4)
#define Z_CONNECTION_START_BIND                         (5)
#define Z_CONNECTION_START_SESSION                      (6)
#define Z_CONNECTION_COMPRESSION_PROCESSOR_FAILURE      (7)
#define Z_CONNECTION_COMPRESSION_PROCESSOR_PROCEED      (8)
#define Z_CONNECTION_COMPRESSION_PROCESSOR_SUCCEDED     (9)
#define Z_CONNECTION_AUTH_PROCESSOR_WRONG_CREDENTIALS   (10)
#define Z_CONNECTION_STREAM_CLOSED                      (11)
#define Z_CONNECTION_AUTH_PROCESSOR_FAILURE             (12)
#define Z_CONNECTION_READY                              (13)
#define Z_CONNECTION_STREAM_OPENED                      (14)
#define Z_CONNECTION_SOCKET_CLOSED                      (15)
#define Z_CONNECTION_STANZA_FILTER_FAILED               (16)
#define Z_CONNECTION_START_TLS_NEGOTIATION              (17)
#define Z_CONNECTION_STREAM_REOPENED                    (18)

#define Z_CONNECTION_TLS_TRIED                          (1)
#define Z_CONNECTION_COMPRESSION_TRIED                  (2)
#define Z_CONNECTION_AUTH_TRIED                         (4)


typedef struct ZBind{
    ZObject object;
    ZConnection *connection;
    char *bind_id;
    char *session_id;
    int session_required;
}ZBind;

struct ZConnection {
    ZObject object;
    ZJid *jid;
    ZSocket *sock;
    ZParser *parser;
    ZStanzaParser *stanza_parser;
    ZStanzaFilter *stanza_filter;
    ZEventQueue *event_queue;
    ZStanza *features_stanza;
    ZCompressionProcessor *compression_processor;
    ZAuthProcessor *auth_processor;
    ZConnectionStatusCallback status_callback;
    void *status_callback_data;
    char *password;
    char *stream_id;
    int is_component;
    int connection_flags;
    int next_id;
};

ZXMPP_PRIVATE int z_connection_iterate(ZConnection *connection);
ZXMPP_PRIVATE int z_connection_handle_features(ZStanzaFilter *stanza_filter,ZStanza *stanza,void *data);
ZXMPP_PRIVATE int z_connection_handle_tls(ZStanzaFilter *stanza_filter,ZStanza *stanza,void *data);
ZXMPP_PRIVATE int z_connection_start_bind(ZConnection *connection,void *data);
ZXMPP_PRIVATE int z_connection_start_session(ZConnection *connection,void *data);
ZXMPP_PRIVATE int z_connection_handle_ping(ZStanzaFilter *stanza_filter,ZStanza *stanza,void *data);
ZXMPP_PRIVATE int z_socket_read_callback(ZSocket *sock,const char *buff,size_t len,void *data);
ZXMPP_PRIVATE int z_stanza_parser_callback(ZXMPP_UNUSED ZStanzaParser *stanza_parser,ZStanza *stanza,int stanza_type,void *data);
ZXMPP_PRIVATE int z_socket_state_callback(ZSocket *sock,int state,void *data);
ZXMPP_PRIVATE int z_connection_close(ZConnection *connection);

#ifdef __cplusplus
}
#endif

#endif





