/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _ZXMPP_EVENTQUEUE_H_
#define _ZXMPP_EVENTQUEUE_H_

#include <common.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ZEvent {
    int type;
    void *data;
}ZEvent;

struct ZEventQueue;
typedef struct ZEventQueue ZEventQueue;

ZXMPP_PRIVATE ZEventQueue *z_event_queue_new(size_t events_size);
ZXMPP_PRIVATE void z_event_queue_destroy(ZEventQueue *event_queue);
ZXMPP_PRIVATE int z_event_queue_push(ZEventQueue *event_queue, ZEvent *event);
ZXMPP_PRIVATE int z_event_queue_pop(ZEventQueue *event_queue, ZEvent *event);
ZXMPP_PRIVATE size_t z_event_queue_pop_many(ZEventQueue *event_queue, ZEvent *event, size_t events_size);
ZXMPP_PRIVATE int z_event_queue_has_events(ZEventQueue *event_queue);

#ifdef __cplusplus
}
#endif

#endif
