/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_SOCKET_C_H_
#define _ZXMPP_SOCKET_C_H_

#include <zsocket.h>
#include <zcompression_c.h>


#ifdef __cplusplus
extern "C" {
#endif

#if !defined (ZXMPP_WIN32)
    typedef int sock_t;
#else
    typedef SOCKET sock_t;
#endif


#define Z_SOCKET_INITIALIZED  (0)
#define Z_SOCKET_CONNECTING   (1)
#define Z_SOCKET_CONNECTED    (2)
#define Z_SOCKET_DISCONNECTED (3)


typedef int (*ZSocketStateCallback) (ZSocket *sock,int state, void *data);
typedef int (*ZSocketReadCallback) (ZSocket *socket,const char *buffer,size_t len, void *data);

ZXMPP_PRIVATE ZSocket *z_socket_new(ZSocketStateCallback state_callback,void *state_callback_data,ZSocketReadCallback read_callback,void *read_callback_data);
ZXMPP_PRIVATE int z_socket_iterate(ZSocket *sock);
ZXMPP_PRIVATE int z_socket_send(ZSocket *sock,const char *data,size_t len);
ZXMPP_PRIVATE int z_socket_initialize_tls(ZSocket *socket);
ZXMPP_PRIVATE int z_socket_set_compression_mechanism(ZSocket *socket,ZCompressionMechanism *compression_mechanism);
ZXMPP_PRIVATE int z_socket_get_status(ZSocket *socket);

#ifdef __cplusplus
}
#endif

#endif







