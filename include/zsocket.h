/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_SOCKET_H_
#define _ZXMPP_SOCKET_H_

#include <common.h>

#ifdef __cplusplus
extern "C" {
#endif

#define Z_SOCKET_READ   (0)
#define Z_SOCKET_WRITE  (1)


struct ZSocket;
typedef struct ZSocket ZSocket;

typedef void (*ZSocketReadWriteCallback) (ZSocket *sock,int type,const char *text,size_t data_size,void *data);

ZXMPP_EXPORT int z_socket_set_host(ZSocket *sock,const char *host);
ZXMPP_EXPORT int z_socket_set_port(ZSocket *sock,int port);
ZXMPP_EXPORT int z_socket_set_read_write_callback(ZSocket *sock,ZSocketReadWriteCallback callback,void *data);

#ifdef __cplusplus
}
#endif

#endif






