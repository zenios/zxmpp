/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#if defined(HAVE_CONFIG_H)
#include "config.h"
#endif

#include <stddef.h>

#if defined(_WIN32) || defined(_WIN64) || defined(_WIN32_WCE) || defined(__WIN32__)
#  ifndef ZXMPP_WIN32
#    define ZXMPP_WIN32
#  endif
#else
#  ifndef ZXMPP_POSIX
#    define ZXMPP_POSIX
#  endif
#endif

/* Export Macros */
#if defined(ZXMPP_STATIC)
#  define ZXMPP_EXPORT
#elif defined(ZXMPP_WIN32)
#  if defined(ZXMPP_SHARED)
#    define ZXMPP_EXPORT __declspec(dllexport)
#  else
#    define ZXMPP_EXPORT __declspec(dllimport)
#  endif
#elif defined(__GNUC__)
#    if __GNUC__ >= 4
#      define ZXMPP_EXPORT __attribute__ ((visibility("default")))
#    endif
#endif

/* Private symbols */
#if defined(__GNUC__) && !defined(ZXMPP_WIN32)
#  if __GNUC__ >= 4
#    define ZXMPP_PRIVATE __attribute__ ((visibility("hidden")))
#  endif
#else
#    define ZXMPP_PRIVATE
#endif

/*Deprecated*/
#if (__GNUC__ > 3 || (__GNUC__ == 3 && __GNUC_MINOR__ >= 1))
#    define ZXMPP_DEPRECATED  __attribute__((__deprecated__))
#elif defined(_MSC_VER)
#    define ZXMPP_DEPRECATED  __declspec(deprecated("Function is not supported"))
#endif

/*Unused*/
#if defined(__GNUC__)
#   define ZXMPP_UNUSED __attribute__ ((unused))
#else
#   define ZXMPP_UNUSED
#endif
