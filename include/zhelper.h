/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _ZXMPP_HELPER_H_
#define _ZXMPP_HELPER_H_

#include <common.h>

#ifdef __cplusplus
extern "C" {
#endif

#include "zstanza.h"
#include "zjid.h"

ZXMPP_PRIVATE char *z_helper_create_opening_stream(ZJid *jid,int component);
ZXMPP_PRIVATE ZStanza *z_helper_create_auth_stanza(const char *mechanism);
ZXMPP_PRIVATE ZStanza *z_helper_create_bind_stanza(const char *id,const char *resource);
ZXMPP_PRIVATE ZStanza *z_helper_create_session_stanza(const char *id);
ZXMPP_PRIVATE ZStanza *z_helper_create_compression_stanza(const char *mechanism);
ZXMPP_PRIVATE ZStanza *z_helper_create_component_auth_stanza(const char *stream_id,const char *password);

#ifdef __cplusplus
}
#endif

#endif



