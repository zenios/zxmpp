/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _ZXMPP_PARSER_TYPE_H_
#define _ZXMPP_PARSER_TYPE_H_

#include <zstanza_parser.h>

#ifdef __cplusplus
extern "C" {
#endif

struct ZParserPrivate;
typedef struct ZParserPrivate ZParserPrivate;

typedef int             (*z_parser_private_init)            (void);
typedef void            (*z_parser_private_destroy)         (void);
typedef ZParserPrivate* (*z_parser_private_create)          (ZStanzaParser *stanza_parser);
typedef int             (*z_parser_private_parse)           (ZParserPrivate *parser, const char *data, size_t len, int finish);
typedef void            (*z_parser_private_reset_destroy)   (ZParserPrivate *parser);

typedef struct z_parser_ops {
    z_parser_private_init             parser_private_lib_init;
    z_parser_private_destroy          parser_private_lib_destroy;
    z_parser_private_create           parser_private_create;
    z_parser_private_parse            parser_private_parse;
    z_parser_private_reset_destroy    parser_private_reset;
    z_parser_private_reset_destroy    parser_private_destroy;
}z_parser_ops;

typedef struct ZParserType {
    const char *parser_name;
    z_parser_ops parser_ops;
}ZParserType;

#ifdef __cplusplus
}
#endif

#endif


