/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_JID_H_
#define _ZXMPP_JID_H_

#include <common.h>

#ifdef __cplusplus
extern "C" {
#endif

#define Z_JID_USERNAME (1)
#define Z_JID_NODE     (2)
#define Z_JID_RESOURCE (4)
#define Z_JID_FULL     (Z_JID_USERNAME | Z_JID_NODE | Z_JID_RESOURCE)


struct ZJid;
typedef struct ZJid ZJid;

ZXMPP_EXPORT ZJid *z_jid_new(const char *jid);
ZXMPP_EXPORT const char *z_jid_get(const ZJid *jid, int part);
ZXMPP_EXPORT int z_jid_cmp (const ZJid *a,const ZJid *b, int parts);



#ifdef __cplusplus
}
#endif

#endif





