/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_COMPRESSION_MECHANISM_TYPE_H_
#define _ZXMPP_COMPRESSION_MECHANISM_TYPE_H_


#include <zcompression.h>

#ifdef __cplusplus
extern "C" {
#endif

struct ZCompressionMechanismPrivate;
typedef struct ZCompressionMechanismPrivate ZCompressionMechanismPrivate;

typedef ZCompressionMechanismPrivate* (*z_compression_mechanism_create_callback) (void);
typedef void (*z_compression_mechanism_destroy_callback) (ZCompressionMechanismPrivate *compression_mechanism);
typedef int (*z_compression_mechanism_compress_decompress_callback) (ZCompressionMechanismPrivate *compression_mechanism,void *source,size_t source_len,void **dest,size_t *dest_len,size_t *left);

typedef struct z_compression_mechanism_ops {
    z_compression_mechanism_create_callback                 create_callback;
    z_compression_mechanism_destroy_callback                destroy_callback;
    z_compression_mechanism_compress_decompress_callback    compress_callback;
    z_compression_mechanism_compress_decompress_callback    decompress_callback;
}z_compression_mechanism_ops;

struct ZCompressionMechanismType {
    const char *compression_mechanism_name;
    int priority;
    z_compression_mechanism_ops compression_mechanism_ops;
};

#ifdef __cplusplus
}
#endif

#endif









