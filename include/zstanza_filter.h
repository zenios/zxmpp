/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_STANZA_FILTER_H_
#define _ZXMPP_STANZA_FILTER_H_

#include <zstanza.h>

#ifdef __cplusplus
extern "C" {
#endif

#define Z_FILTER_RULE_DONE              (0)
#define Z_FILTER_RULE_NAME              (1)
#define Z_FILTER_RULE_ID                (2)
#define Z_FILTER_RULE_TYPE              (4)
#define Z_FILTER_RULE_FROM              (8)
#define Z_FILTER_RULE_NS                (16)

struct ZStanzaFilter;
typedef struct ZStanzaFilter ZStanzaFilter;

struct ZStanzaFilterRule;
typedef struct ZStanzaFilterRule ZStanzaFilterRule;

typedef int (*ZStanzaFilterCallback) (ZStanzaFilter *filter,ZStanza *stanza,void *data);

ZXMPP_EXPORT ZStanzaFilterRule *z_stanza_filter_add_rule(ZStanzaFilter *filter,ZStanzaFilterCallback callback,void *data, ...);
ZXMPP_EXPORT int z_stanza_filter_remove_rule(ZStanzaFilterRule *rule);
ZXMPP_EXPORT int z_stanza_filter_rule_set_single(ZStanzaFilterRule *rule);


#ifdef __cplusplus
}
#endif

#endif







