/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef _ZXMPP_STANZA_PARSER_H_
#define _ZXMPP_STANZA_PARSER_H_

#include <zstanza.h>

#ifdef __cplusplus
extern "C" {
#endif


struct ZStanzaParser;
typedef struct ZStanzaParser ZStanzaParser;

ZXMPP_EXPORT int z_stanza_parser_start_element(ZStanzaParser *stanza_parser,const char *name,const char **atts);
ZXMPP_EXPORT int z_stanza_parser_end_element(ZStanzaParser *stanza_parser,const char *name);
ZXMPP_EXPORT int z_stanza_parser_handle_characters(ZStanzaParser *stanza_parser,const char *s,int len);
ZXMPP_EXPORT int z_stanza_parser_reset(ZStanzaParser *stanza_parser);

#ifdef __cplusplus
}
#endif

#endif






