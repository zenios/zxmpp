/*
* ZXMPP
* Copyright (C) 2011-2013 Dimitris Zenios
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _ZXMPP_COMPRESSION_C_H_
#define _ZXMPP_COMPRESSION_C_H_

#include <common.h>

#ifdef __cplusplus
extern "C" {
#endif

struct ZCompressionMechanism;
typedef struct ZCompressionMechanism ZCompressionMechanism;

ZXMPP_PRIVATE int z_compression_init(void);
ZXMPP_PRIVATE void z_compression_destroy(void);
ZXMPP_PRIVATE ZCompressionMechanism *z_compression_mechanism_create(const char *name);
ZXMPP_PRIVATE void z_compression_mechanism_destroy(ZCompressionMechanism *mechanism);
ZXMPP_PRIVATE int z_compression_mechanism_is_supported(const char *name);
ZXMPP_PRIVATE int z_compression_mechanism_get_priorty(const char *name);
ZXMPP_PRIVATE int z_compression_mechanism_compress(ZCompressionMechanism *mechanism,void *source,size_t source_len,void **dest,size_t *dest_len,size_t *left);
ZXMPP_PRIVATE int z_compression_mechanism_decompress(ZCompressionMechanism *mechanism,void *source,size_t source_len,void **dest,size_t *dest_len, size_t *left);


#ifdef __cplusplus
}
#endif

#endif



